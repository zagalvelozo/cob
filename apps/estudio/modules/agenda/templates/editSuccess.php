<?php use_helper('Javascript', 'Object'); ?>
<?php $agenda = $form->getObject() ?>
<h2><?php echo $agenda->isNew() ? 'Ingresar' : 'Postergar' ?> Tarea</h2>

<?php if(isset($accion) && $accion == 'tareasvencidas'):?>
	<?php if(isset($count) && $count == 1):?>
		<?php echo form_remote_tag(array(
		    'update'   	=> 'vencidas',
		    'url'      	=> 'agenda/update'.(!$agenda->isNew() ? '?id='.$agenda->getId() : '').(isset($accion) ? '&accion='.$accion : ''),
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('fade','vencidas'),
			'confirm'  => "Se guardar&aacute;n los datos. Desea continuar?",
		))?>
	<?php else:?>
		<?php echo form_remote_tag(array(
		    'update'   	=> 'vencidas',
		    'url'      	=> 'agenda/update'.(!$agenda->isNew() ? '?id='.$agenda->getId() : '').(isset($accion) ? '&accion='.$accion : ''),
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading'),
			'confirm'  => "Se guardar&aacute;n los datos. Desea continuar?",
		))?>
	<?php endif;?>
	      <?php echo $form->renderGlobalErrors() ?>
	  <table>
	    <tfoot>
	      <tr>
	        <td class="tdcentrado" colspan="2">
	          <input type="submit" value="Guardar" class="guardar boton_con_imagen"/>
	        </td>
	      </tr>
	    </tfoot>
	    <tbody>
	      <tr>
	        <th><?php echo $form['descripcion']->renderLabel() ?></th>
	        <td>
	          <?php echo $form['descripcion']->renderError() ?>
	          <?php echo $form['descripcion'] ?>
	        </td>
	      </tr>
	      <tr>
	        <th><?php echo $form['fecha']->renderLabel() ?></th>
	        <td>
		       <div style="display:none;"> 
		          	<?php echo $form['fecha_actual'] ?> 
		       </div>
		       <div>
		          <?php echo $form['fecha']->renderError() ?>
		          <?php echo $form['fecha'] ?>
		        
		        <?php echo $form['realizada'] ?>  
		        
				<?php echo $form['perfil_id'] ?>
		        <?php echo $form['id'] ?>
		        </div>
	        </td>
	      </tr>
	    </tbody>
	  </table>
	  
	</form>
<?php else:?>
	<?php echo form_remote_tag(array(
	    'update'   	=> 'acciones_agenda',
	    'url'      	=> 'agenda/update'.(!$agenda->isNew() ? '?id='.$agenda->getId() : '').(isset($accion) ? '&accion='.$accion : ''),
		'loading'	=> visual_effect('appear','loading'),
		'complete'	=> visual_effect('fade','loading'),
		'confirm'  => "Se guardar&aacute;n los datos. Desea continuar?",
	))?>
	      <?php echo $form->renderGlobalErrors() ?>
	  <table>
	    <tfoot>
	      <tr>
	        <td class="tdcentrado" colspan="2">
	        <input type="submit" value="Guardar" class="guardar boton_con_imagen"/>
	          <?php echo button_to_remote('Cancelar', array(
				'update' => 'modulo',
				'url' => 'agenda/index',
				'loading' => visual_effect('appear','loading'),
				'complete'	=> visual_effect('fade','loading'),
				),array('class' => 'cancelar boton_con_imagen'))?>
	          
	        </td>
	      </tr>
	    </tfoot>
	    <tbody>
	      <tr>
	        <th><?php echo $form['descripcion']->renderLabel('Descripci&oacute;n *') ?></th>
	        <td>
	          <?php echo $form['descripcion']->renderError() ?>
	          <?php echo $form['descripcion'] ?>
	        </td>
	      </tr>
	      <tr>
	        <th><?php echo $form['fecha']->renderLabel('Fecha *') ?></th>
	        <td>
	        <div style="display:none;"> 
		          	<?php echo $form['fecha_actual'] ?> 
		       </div>
	          <?php echo $form['fecha']->renderError() ?>
	          <?php echo $form['fecha'] ?>
	        
	        <?php echo $form['realizada'] ?>  
	        
			<?php echo $form['perfil_id'] ?>
	        <?php echo $form['id'] ?>
	        </td>
	      </tr>
	    </tbody>
	  </table>
	</form>
<?php endif;?>
