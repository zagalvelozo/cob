<?php use_helper('Javascript') ?>
<h1>Agenda</h1>
<ul class="menu-modulo">
	<li>
		<?php echo link_to_remote('Ingresar Tarea', array(
    		'update' => 'acciones_agenda',
    		'url'    => 'agenda/create',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		))?></li>
	<li>
		<?php echo link_to_remote('Postergar Tarea', array(
    		'update' => 'acciones_agenda',
    		'url'    => 'agenda/list?accion=edit',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<li>
		<?php echo link_to_remote('Tareas Realizadas', array(
    		'update' => 'acciones_agenda',
    		'url'    => 'agenda/list?accion=realizadas',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<li>
		<?php echo link_to_remote('Tareas Vencidas', array(
    		'update' => 'acciones_agenda',
    		'url'    => 'agenda/list?accion=vencidas',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<li>
		<?php echo link_to_remote('Eliminar Tareas', array(
    		'update' => 'acciones_agenda',
    		'url'    => 'agenda/list?accion=delete',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
</ul>
<div id="acciones_agenda" style="clear: right">
<h2>Tareas a realizar por <?php echo ($sf_user->getPerfil()->getNombreCompleto() != '' ? $sf_user->getPerfil()->getNombreCompleto() : $sf_user->getUsername())?></h2>
<table>
  <thead>
    <tr>
    	<th class="tddestacado">Fecha - Hora</th>
      	<th class="tddestacado">Descripci&oacute;n</th>
      	<th class="tddestacado">Realizada</th>
    </tr>
  </thead>
  <tbody>
  <?php if(count($agendaList)>0):?>
    <?php foreach ($agendaList as $agenda): ?>
    <tr>
    	<td class="tdcentrado"><?php
    	echo Funciones::fecha2($agenda->getFecha('d-m-Y'));
				    	echo $agenda->getFecha('\ \a\ \l\a\s H:i').' hrs.';?></td>
      	<td><?php echo $agenda->getDescripcion() ?></td>
     	<td class="tdcentrado"><?php echo ($agenda->getRealizada() ? image_tag('accept.png',array('size'=>'16x16')) : image_tag('delete.png',array('size'=>'16x16'))) ?></td>
    </tr>
    <?php endforeach; ?>
    <?php else:?>
    <tr><td colspan="3" class="aviso">NO SE ENCONTRARON TAREAS</td></tr>
    <?php endif;?>
  </tbody>
</table>
</div>

