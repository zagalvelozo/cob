<?php

/**
 * perfil actions.
 *
 * @package    sgcj
 * @subpackage perfil
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class perfilActions extends sfActions
{
  public function executeIndex()
  {
    $this->perfilList = PerfilPeer::doSelect(new Criteria());
  }

  public function executeShow()
  {
    $this->perfil = $this->getUser()->getPerfil();
    $this->forward404Unless($this->perfil);
  }

  public function executeCreate()
  {
    $this->form = new PerfilForm();

    $this->setTemplate('edit');
  }

  public function executeEdit()
  {
    $this->form = new PerfilForm(PerfilPeer::retrieveByPk($this->getUser()->getPerfil()->getId()));
    
    $listaRegiones = RegionPeer::doSelect(new Criteria());
    
    $this->lista = array();
    
    foreach($listaRegiones as $region)
    {
    	$this->lista[$region->getId()] = $region->getNombre();
    }
    
    $this->aComuna = $this->getUser()->getPerfil()->getComuna();
    $this->aCiudad = ($this->aComuna ? $this->aComuna->getCiudad() : '');
    $this->aRegion = ($this->aCiudad ? $this->aCiudad->getRegion() : '');
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new PerfilForm(PerfilPeer::retrieveByPk($request->getParameter('id')));

    $datos = $request->getParameter('perfil');
    $datos['comuna_id'] = $request->getParameter('comuna');
    $this->form->bind($datos);
    if ($this->form->isValid())
    {
      $perfil = $this->form->save();

      $this->forward('perfil','show');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($perfil = PerfilPeer::retrieveByPk($request->getParameter('id')));

    $perfil->delete();

    $this->redirect('perfil/index');
  }
}
