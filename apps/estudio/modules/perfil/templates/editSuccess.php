<?php use_helper('Javascript', 'Object', 'DateForm') ?>
<?php $perfil = $form->getObject() ?>
<?php echo form_remote_tag(array(
    'update'   	=> 'acciones_perfil',
    'url'      	=> 'perfil/update'.(!$perfil->isNew() ? '?id='.$perfil->getId() : ''),
	'loading'	=> visual_effect('appear','loading'),
	'complete'	=> visual_effect('fade','loading'),
	'confirm'  => "Se guardar&aacute;n los datos. Desea continuar?",
))?>
<h2>Editar Datos</h2>
  <table>
    <tfoot>
      <tr>
        <td class="tdcentrado" colspan="4">
        <input type="submit" class="guardar boton_con_imagen" value="Guardar" />
          <?php echo button_to_remote('Cancelar', array(
			'update' => 'modulo',
			'url' => '@user_profile',
			'loading' => visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading'),
			),array('class' => 'cancelar boton_con_imagen'))?>
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['nombres']->renderLabel() ?></th>
        <td colspan="3">
          <?php echo $form['nombres']->renderError() ?>
          <?php echo $form['nombres'] ?>
          
          <?php echo $form['sf_guard_user_id']->renderError() ?>
          <?php echo $form['sf_guard_user_id'] ?>
          <?php echo $form['password1']->renderError() ?>
          <?php echo $form['password1'] ?>
          <?php echo $form['password2']->renderError() ?>
          <?php echo $form['password2'] ?>
          <?php echo $form['password3']->renderError() ?>
          <?php echo $form['password3'] ?>
          <?php echo $form['ultimo_cambio']->renderError() ?>
          <?php echo $form['ultimo_cambio'] ?>
          <?php echo $form['pregunta']->renderError() ?>
          <?php echo $form['pregunta'] ?>
          <?php echo $form['respuesta']->renderError() ?>
          <?php echo $form['respuesta'] ?>
          <?php echo $form['envio_mail']->renderError() ?>
          <?php echo $form['envio_mail'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['apellido_paterno']->renderLabel() ?></th>
        <td>
          <?php echo $form['apellido_paterno']->renderError() ?>
          <?php echo $form['apellido_paterno'] ?>
        </td>
        <th><?php echo $form['apellido_materno']->renderLabel() ?></th>
        <td>
          <?php echo $form['apellido_materno']->renderError() ?>
          <?php echo $form['apellido_materno'] ?>
          
          <?php echo $form['comuna_id']->renderError() ?>
          <?php echo $form['comuna_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['direccion']->renderLabel() ?></th>
        <td colspan="3">
          <?php echo $form['direccion']->renderError() ?>
          <?php echo $form['direccion'] ?>
        </td>
      </tr>
      <tr>
        <th>Regi&oacute;n</th>
        <td colspan="3">
			<?php 
				$selectedOption = '';
				
				if($aRegion instanceof Region)
				{
					$selectedOption = $aRegion->getId();
				}
				
				echo select_tag('region_id',
	  					options_for_select($lista,$selectedOption,array('include_custom' => '-- Seleccione Regi&oacute;n --')),
	  					array('onchange' => remote_function(array(
	      								'update' => "ciudades-segun-region",
	      								'with'   => "'region_id='+value",
	  									'script' => true,
	      								'url'    => '/administracion/creaselectciudad',
	      								'loading'	=> visual_effect('appear','loading'),
										'complete'	=> visual_effect('fade','loading')
	  									))
	  					));
			
			?>
        </td>
      </tr>
      <tr>
        <th>Provincia</th>
        <td>
			<div id="ciudades-segun-region">
			<?php 
				if($aCiudad instanceof Ciudad)
				{
					echo input_hidden_tag('acreedor_ciudad_id',$aCiudad->getId());
					echo input_tag('ciudadName',$aCiudad,array('readonly' => 'readonly'));
				}
				else
				{
					echo input_tag('ciudadName','',array('readonly' => 'readonly'));
				}
            ?>
			</div>
        </td>
        <th>Comuna</th>
        <td>
			<div id="comunas-segun-ciudades">
			<?php 
				if($aComuna instanceof Comuna)
				{
					echo input_hidden_tag('comuna',$aComuna->getId());
					echo input_tag('comunaName',$aComuna,array('readonly' => 'readonly'));
				}
				else
				{
					echo input_tag('comunaName','',array('readonly' => 'readonly'));
				}
            ?>
			</div>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['telefono']->renderLabel() ?></th>
        <td>
          <?php echo $form['telefono']->renderError() ?>
          <?php echo $form['telefono'] ?>
        </td>
        <th><?php echo $form['fax']->renderLabel() ?></th>
        <td>
          <?php echo $form['fax']->renderError() ?>
          <?php echo $form['fax'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['email']->renderLabel() ?></th>
        <td colspan="2">
          <?php echo $form['email']->renderError() ?>
          <?php echo $form['email'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
