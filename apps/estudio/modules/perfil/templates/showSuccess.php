<h2>Sus Datos</h2>
<table>
  <tbody>
    <tr>
      <th>Ultimo cambio de contrase&ntilde;a:</th>
      <td colspan="5"><?php echo Funciones::fecha2($perfil->getUltimoCambio('d-m-Y')) ?></td>
    </tr>
    <tr>
      <th>Nombre:</th>
      <td colspan="5"><?php echo $perfil->getNombreCompleto() ?></td>
    </tr>
    <tr>
      <th>Direcci&oacute;n:</th>
      <td colspan="5"><?php echo $perfil->getDireccion() ?></td>
    </tr>
    <tr>
      <th>Comuna:</th>
      <td><?php echo $perfil->getComuna() ?></td>
      <th>Provincia:</th>
      <td><?php echo ($perfil->getComuna() ? $perfil->getComuna()->getCiudad() : '')?></td>
      <th>Regi&oacute;n:</th>
      <td><?php echo ($perfil->getComuna() ? $perfil->getComuna()->getCiudad()->getRegion() : '') ?></td>
    </tr>
    <tr>
      <th>Telefono:</th>
      <td><?php echo $perfil->getTelefono() ?></td>
      <th>Fax:</th>
      <td colspan="3"><?php echo $perfil->getFax() ?></td>
    </tr>
    <tr>
      <th>Email:</th>
      <td colspan="5"><?php echo $perfil->getEmail() ?></td>
    </tr>
  </tbody>
</table>
