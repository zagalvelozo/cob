<?php use_helper('Javascript')?>
<?php if($accion != 'list'):?>
<h1>Gesti&oacute;n de Usuarios</h1>
<ul class="menu-modulo">
	<?php if($sf_user->hasCredential('administrador')): ?>
	<li>
		<?php echo link_to_remote('Ingresar Usuario', array(
			'script' => 'true',
    		'update' => 'acciones_usuario',
    		'url'    => 'usuario/create',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		))?></li>
		<li>
		<?php echo link_to_remote('Editar Usuario', array(
			'script' => 'true',
    		'update' => 'acciones_usuario',
    		'url'    => 'usuario/editarAutocompletar',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		))?></li>
	<li>
		<?php echo link_to_remote('Visualizar Usuario', array(
		'script' => 'true',
    		'update' => 'acciones_usuario',
    		'url'    => 'administracion/searchacreedor?accion=show',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<li>
		<?php echo link_to_remote('Lista de Usuario', array(
		'script' => 'true',
    		'update' => 'acciones_usuario',
    		'url'    => 'usuario/index?accion=list',
			'loading'	=> visual_effect('appear','loading'),
			'complete'	=> visual_effect('fade','loading').
							visual_effect('highlight','modulo',array('duration' => 0.5)),
		)) ?></li>
	<?php endif; ?>
</ul>
<?php endif;?>
<div id="acciones_usuario" style="clear: right">
<h2>Lista de Usuarios</h2>
	<table>
	  <thead>
	    <tr>
	      <th class="tddestacado">N Interno</th>
	      <th class="tddestacado">Usuario</th>
	      <th class="tddestacado">Fecha Creaci&oacute;n</th>
	      <th class="tddestacado">&Uacute;ltimo Ingreso</th>
	    </tr>
	  </thead>
	  <tbody>
	    <?php foreach ($sf_guard_userList as $sf_guard_user): ?>
	    <tr>
	      <td><a href="<?php echo url_for('usuario/show?id='.$sf_guard_user->getId()) ?>"><?php echo $sf_guard_user->getId() ?></a></td>
	      <td><?php echo $sf_guard_user->getUsername() ?></td>
	      <td><?php 
	      echo Funciones::fecha2($sf_guard_user->getCreatedAt('d-m-Y'));
	      echo $sf_guard_user->getCreatedAt('\ \a\ \l\a\s H:i \h\r\s\.')?></td>
	      <td><?php 
	      echo Funciones::fecha2($sf_guard_user->getLastLogin('d-m-Y'));
	      echo $sf_guard_user->getLastLogin('\ \a\ \l\a\s H:i \h\r\s\.') ?></td>
	     </tr>
	    <?php endforeach; ?>
	  </tbody>
	</table>
</div>

