<?php

/**
 * acreedor actions.
 *
 * @package    sgcj
 * @subpackage acreedor
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class acreedorActions extends sfActions
{
  public function executeIndex()
  {
    $this->acreedorList = AcreedorPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->acreedor = AcreedorPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->acreedor);
  }

  public function executeCreate()
  {
    $this->form = new AcreedorForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new AcreedorForm(AcreedorPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new AcreedorForm(AcreedorPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('acreedor'));
    if ($this->form->isValid())
    {
      $acreedor = $this->form->save();

      $this->redirect('acreedor/edit?id='.$acreedor->getId());
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($acreedor = AcreedorPeer::retrieveByPk($request->getParameter('id')));

    $acreedor->delete();

    $this->redirect('acreedor/index');
  }
}
