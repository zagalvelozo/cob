<?php

/**
 * agenda actions.
 *
 * @package    sgcj
 * @subpackage agenda
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class agendaActions extends sfActions
{
  public function executeIndex()
  {
    $this->agendaList = AgendaPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->agenda = AgendaPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->agenda);
  }

  public function executeCreate()
  {
    $this->form = new AgendaForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new AgendaForm(AgendaPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new AgendaForm(AgendaPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('agenda'));
    if ($this->form->isValid())
    {
      $agenda = $this->form->save();

      $this->redirect('agenda/edit?id='.$agenda->getId());
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($agenda = AgendaPeer::retrieveByPk($request->getParameter('id')));

    $agenda->delete();

    $this->redirect('agenda/index');
  }
}
