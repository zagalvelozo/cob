<?php $agenda = $form->getObject() ?>
<h1><?php echo $agenda->isNew() ? 'New' : 'Edit' ?> Agenda</h1>

<form action="<?php echo url_for('agenda/update'.(!$agenda->isNew() ? '?id='.$agenda->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('agenda/index') ?>">Cancel</a>
          <?php if (!$agenda->isNew()): ?>
            &nbsp;<?php echo link_to('Delete', 'agenda/delete?id='.$agenda->getId(), array('post' => true, 'confirm' => 'Are you sure?')) ?>
          <?php endif; ?>
          <input type="submit" value="Save" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['perfil_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['perfil_id']->renderError() ?>
          <?php echo $form['perfil_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['descripcion']->renderLabel() ?></th>
        <td>
          <?php echo $form['descripcion']->renderError() ?>
          <?php echo $form['descripcion'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['fecha']->renderLabel() ?></th>
        <td>
          <?php echo $form['fecha']->renderError() ?>
          <?php echo $form['fecha'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['realizada']->renderLabel() ?></th>
        <td>
          <?php echo $form['realizada']->renderError() ?>
          <?php echo $form['realizada'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
