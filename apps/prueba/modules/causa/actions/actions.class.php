<?php

/**
 * causa actions.
 *
 * @package    sgcj
 * @subpackage causa
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class causaActions extends sfActions
{
  public function executeIndex()
  {
    $this->causaList = CausaPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->causa = CausaPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->causa);
  }

  public function executeCreate()
  {
    $this->form = new CausaForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new CausaForm(CausaPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new CausaForm(CausaPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('causa'));
    if ($this->form->isValid())
    {
      $causa = $this->form->save();

      $this->redirect('causa/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($causa = CausaPeer::retrieveByPk($request->getParameter('id')));

    $causa->delete();

    $this->redirect('causa/index');
  }
}
