<h1>Lista de Causas (en un IF se debe mostrar los campos correspondientes si es que la causa es exhorto, sino, no mostrar los campos exhorto)</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Tipo de Causa</th>
      <th>Deudor</th>
      <th>Materia</th>
      <th>Contrato</th>
      <th>Rol</th>
      <th>Nombre</th>
      <th>Competencia</th>
      <th>Fecha de Inicio</th>
      <th>Fecha de T&eacute;rmino</th>
      <th>Herencia Exhorto</th>
      <th>Causa Exhorto</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($causaList as $causa): ?>
    <tr>
      <td><a href="<?php echo url_for('causa/show?id='.$causa->getId()) ?>"><?php echo $causa->getId() ?></a></td>
      <td><?php echo $causa->getTipoCausaId() ?></td>
      <td><?php echo $causa->getDeudorId() ?></td>
      <td><?php echo $causa->getMateriaId() ?></td>
      <td><?php echo $causa->getContratoId() ?></td>
      <td><?php echo $causa->getRol() ?></td>
      <td><?php echo $causa->getNombre() ?></td>
      <td><?php echo $causa->getCompetencia() ?></td>
      <td><?php echo $causa->getFechaInicio() ?></td>
      <td><?php echo $causa->getFechaTermino() ?></td>
      <td><?php echo $causa->getHerenciaExhorto() ?></td>
      <td><?php echo $causa->getCausaExhorto() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('causa/create') ?>">Crear</a>
