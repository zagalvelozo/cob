<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $causa->getId() ?></td>
    </tr>
    <tr>
      <th>Tipo de Causa:</th>
      <td><?php echo $causa->getTipoCausaId() ?></td>
    </tr>
    <tr>
      <th>Deudor:</th>
      <td><?php echo $causa->getDeudorId() ?></td>
    </tr>
    <tr>
      <th>Materia:</th>
      <td><?php echo $causa->getMateriaId() ?></td>
    </tr>
    <tr>
      <th>Contrato:</th>
      <td><?php echo $causa->getContratoId() ?></td>
    </tr>
    <tr>
      <th>Rol:</th>
      <td><?php echo $causa->getRol() ?></td>
    </tr>
    <tr>
      <th>Nombre:</th>
      <td><?php echo $causa->getNombre() ?></td>
    </tr>
    <tr>
      <th>Competencia:</th>
      <td><?php echo $causa->getCompetencia() ?></td>
    </tr>
    <tr>
      <th>Fecha de Inicio:</th>
      <td><?php echo $causa->getFechaInicio() ?></td>
    </tr>
    <tr>
      <th>Fecha de T&eacute;rmino:</th>
      <td><?php echo $causa->getFechaTermino() ?></td>
    </tr>
    <tr>
      <th>Herencia Exhorto:</th>
      <td><?php echo $causa->getHerenciaExhorto() ?></td>
    </tr>
    <tr>
      <th>Causa Exhorto:</th>
      <td><?php echo $causa->getCausaExhorto() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('causa/edit?id='.$causa->getId()) ?>">Editar</a>
&nbsp;
<a href="<?php echo url_for('causa/index') ?>">Listar</a>
