<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $causa_tribunal->getId() ?></td>
    </tr>
    <tr>
      <th>Causa:</th>
      <td><?php echo $causa_tribunal->getCausaId() ?></td>
    </tr>
    <tr>
      <th>Tribunal:</th>
      <td><?php echo $causa_tribunal->getTribunalId() ?></td>
    </tr>
    <tr>
      <th>Fecha de Cambio:</th>
      <td><?php echo $causa_tribunal->getFechaCambio() ?></td>
    </tr>
    <tr>
      <th>Motivo:</th>
      <td><?php echo $causa_tribunal->getMotivo() ?></td>
    </tr>
    <tr>
      <th>Tribunal Activo:</th>
      <td><?php echo $causa_tribunal->getActivo() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('causatribunal/edit?id='.$causa_tribunal->getId()) ?>">Editar</a>
&nbsp;
<a href="<?php echo url_for('causatribunal/index') ?>">Listar</a>
