<?php $clase_giro = $form->getObject() ?>
<h1><?php echo $clase_giro->isNew() ? 'New' : 'Edit' ?> Clasegiro</h1>

<form action="<?php echo url_for('clasegiro/update'.(!$clase_giro->isNew() ? '?id='.$clase_giro->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('clasegiro/index') ?>">Cancel</a>
          <?php if (!$clase_giro->isNew()): ?>
            &nbsp;<?php echo link_to('Delete', 'clasegiro/delete?id='.$clase_giro->getId(), array('post' => true, 'confirm' => 'Are you sure?')) ?>
          <?php endif; ?>
          <input type="submit" value="Save" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="clase_giro_nombre">Nombre</label></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
