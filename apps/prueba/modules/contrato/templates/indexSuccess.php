<h1>Lista de Contratos</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Acreedor</th>
      <th>Fecha de Inicio</th>
      <th>Fecha de T&eacute;rmino</th>
      <th>Porcentaje de Honorarios</th>
      <th>Estado</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($contratoList as $contrato): ?>
    <tr>
      <td><a href="<?php echo url_for('contrato/show?id='.$contrato->getId()) ?>"><?php echo $contrato->getId() ?></a></td>
      <td><?php echo $contrato->getNombreAcreedor() ?></td>
      <td><?php echo $contrato->getFechaInicio() ?></td>
      <td><?php echo $contrato->getFechaTermino() ?></td>
      <td><?php echo $contrato->getPorcentajeHonorario() ?></td>
      <td><?php echo $contrato->getEstado() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('contrato/create') ?>">Crear</a>
