<?php

/**
 * deuda actions.
 *
 * @package    sgcj
 * @subpackage deuda
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class deudaActions extends sfActions
{
  public function executeIndex()
  {
    $this->deudaList = DeudaPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->deuda = DeudaPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->deuda);
  }

  public function executeCreate()
  {
    $this->form = new DeudaForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new DeudaForm(DeudaPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new DeudaForm(DeudaPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('deuda'));
    if ($this->form->isValid())
    {
      $deuda = $this->form->save();

      $this->redirect('deuda/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($deuda = DeudaPeer::retrieveByPk($request->getParameter('id')));

    $deuda->delete();

    $this->redirect('deuda/index');
  }
}
