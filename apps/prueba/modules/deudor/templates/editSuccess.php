<?php $deudor = $form->getObject() ?>
<h1><?php echo $deudor->isNew() ? 'Ingresar' : 'Editar' ?> Deudor</h1>

<form action="<?php echo url_for('deudor/update'.(!$deudor->isNew() ? '?id='.$deudor->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('deudor/index') ?>">Cancelar</a>
          <?php if (!$deudor->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'deudor/delete?id='.$deudor->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar el deudor?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="deudor_rut_deudor">Rut</label></th>
        <td>
          <?php echo $form['rut_deudor']->renderError() ?>
          <?php echo $form['rut_deudor'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="deudor_nombres">Nombres</label></th>
        <td>
          <?php echo $form['nombres']->renderError() ?>
          <?php echo $form['nombres'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="deudor_apellido_paterno">Apellido Paterno</label></th>
        <td>
          <?php echo $form['apellido_paterno']->renderError() ?>
          <?php echo $form['apellido_paterno'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="deudor_apellido_materno">Apellido Materno</label></th>
        <td>
          <?php echo $form['apellido_materno']->renderError() ?>
          <?php echo $form['apellido_materno'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="deudor_celular">Tel&eacute;fono Celular</label></th>
        <td>
          <?php echo $form['celular']->renderError() ?>
          <?php echo $form['celular'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="deudor_email">Email</label></th>
        <td>
          <?php echo $form['email']->renderError() ?>
          <?php echo $form['email'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
