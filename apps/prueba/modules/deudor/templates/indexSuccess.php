<h1>Lista de Deudores</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Rut</th>
      <th>Nombres</th>
      <th>Apellido Paterno</th>
      <th>Apellido Materno</th>
      <th>Tel&eacute;fono Celular</th>
      <th>Email</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($deudorList as $deudor): ?>
    <tr>
      <td><a href="<?php echo url_for('deudor/show?id='.$deudor->getId()) ?>"><?php echo $deudor->getId() ?></a></td>
      <td><?php echo $deudor->getRutDeudor() ?></td>
      <td><?php echo $deudor->getNombres() ?></td>
      <td><?php echo $deudor->getApellidoPaterno() ?></td>
      <td><?php echo $deudor->getApellidoMaterno() ?></td>
      <td><?php echo $deudor->getCelular() ?></td>
      <td><?php echo $deudor->getEmail() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('deudor/create') ?>">Crear</a>
