<?php

/**
 * deudordomicilio actions.
 *
 * @package    sgcj
 * @subpackage deudordomicilio
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class deudordomicilioActions extends sfActions
{
  public function executeIndex()
  {
    $this->deudor_domicilioList = DeudorDomicilioPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->deudor_domicilio = DeudorDomicilioPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->deudor_domicilio);
  }

  public function executeCreate()
  {
    $this->form = new DeudorDomicilioForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new DeudorDomicilioForm(DeudorDomicilioPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new DeudorDomicilioForm(DeudorDomicilioPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('deudor_domicilio'));
    if ($this->form->isValid())
    {
      $deudor_domicilio = $this->form->save();

      $this->redirect('deudordomicilio/edit?id='.$deudor_domicilio->getId());
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($deudor_domicilio = DeudorDomicilioPeer::retrieveByPk($request->getParameter('id')));

    $deudor_domicilio->delete();

    $this->redirect('deudordomicilio/index');
  }
}
