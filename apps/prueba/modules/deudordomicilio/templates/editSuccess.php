<?php $deudor_domicilio = $form->getObject() ?>
<h1><?php echo $deudor_domicilio->isNew() ? 'New' : 'Edit' ?> Deudordomicilio</h1>

<form action="<?php echo url_for('deudordomicilio/update'.(!$deudor_domicilio->isNew() ? '?id='.$deudor_domicilio->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('deudordomicilio/index') ?>">Cancel</a>
          <?php if (!$deudor_domicilio->isNew()): ?>
            &nbsp;<?php echo link_to('Delete', 'deudordomicilio/delete?id='.$deudor_domicilio->getId(), array('post' => true, 'confirm' => 'Are you sure?')) ?>
          <?php endif; ?>
          <input type="submit" value="Save" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="deudor_domicilio_comuna_id">Comuna id</label></th>
        <td>
          <?php echo $form['comuna_id']->renderError() ?>
          <?php echo $form['comuna_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="deudor_domicilio_calle">Calle</label></th>
        <td>
          <?php echo $form['calle']->renderError() ?>
          <?php echo $form['calle'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="deudor_domicilio_numero">Numero</label></th>
        <td>
          <?php echo $form['numero']->renderError() ?>
          <?php echo $form['numero'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="deudor_domicilio_depto">Depto</label></th>
        <td>
          <?php echo $form['depto']->renderError() ?>
          <?php echo $form['depto'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="deudor_domicilio_villa">Villa</label></th>
        <td>
          <?php echo $form['villa']->renderError() ?>
          <?php echo $form['villa'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="deudor_domicilio_telefono">Telefono</label></th>
        <td>
          <?php echo $form['telefono']->renderError() ?>
          <?php echo $form['telefono'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="deudor_domicilio_region_id">Region id</label></th>
        <td>
          <?php echo $form['region_id']->renderError() ?>
          <?php echo $form['region_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="deudor_domicilio_ciudad_id">Ciudad id</label></th>
        <td>
          <?php echo $form['ciudad_id']->renderError() ?>
          <?php echo $form['ciudad_id'] ?>

        <?php echo $form['id'] ?>
        <?php echo $form['deudor_id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
