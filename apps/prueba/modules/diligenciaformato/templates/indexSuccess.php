<h1>Lista de Cartas / Escritos en Diligencias</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Diligencia</th>
      <th>Carta / Escrito</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($diligencia_formatoList as $diligencia_formato): ?>
    <tr>
      <td><a href="<?php echo url_for('diligenciaformato/show?id='.$diligencia_formato->getId()) ?>"><?php echo $diligencia_formato->getId() ?></a></td>
      <td><?php echo $diligencia_formato->getDiligenciaId() ?></td>
      <td><?php echo $diligencia_formato->getFormatoId() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('diligenciaformato/create') ?>">Crear</a>
