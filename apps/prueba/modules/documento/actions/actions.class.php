<?php

/**
 * documento actions.
 *
 * @package    sgcj
 * @subpackage documento
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class documentoActions extends sfActions
{
  public function executeIndex()
  {
    $this->documentoList = DocumentoPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->documento = DocumentoPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->documento);
  }

  public function executeCreate()
  {
    $this->form = new DocumentoForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new DocumentoForm(DocumentoPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new DocumentoForm(DocumentoPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('documento'));
    if ($this->form->isValid())
    {
      $documento = $this->form->save();

      $this->redirect('documento/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($documento = DocumentoPeer::retrieveByPk($request->getParameter('id')));

    $documento->delete();

    $this->redirect('documento/index');
  }
}
