<?php $documento = $form->getObject() ?>
<h1><?php echo $documento->isNew() ? 'Ingresar' : 'Editar' ?> Documento ( se llega desde la creacion o modificacion de la causa)</h1>

<form action="<?php echo url_for('documento/update'.(!$documento->isNew() ? '?id='.$documento->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('documento/index') ?>">Cancelar</a>
          <?php if (!$documento->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'documento/delete?id='.$documento->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar el documento?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="documento_tipo_documento_id">Tipo de Documento</label></th>
        <td>
          <?php echo $form['tipo_documento_id']->renderError() ?>
          <?php echo $form['tipo_documento_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="documento_causa_id">Causa</label></th>
        <td>
          <?php echo $form['causa_id']->renderError() ?>
          <?php echo $form['causa_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="documento_fecha_vencimiento">Fecha de Vencimiento</label></th>
        <td>
          <?php echo $form['fecha_vencimiento']->renderError() ?>
          <?php echo $form['fecha_vencimiento'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="documento_monto">Monto</label></th>
        <td>
          <?php echo $form['monto']->renderError() ?>
          <?php echo $form['monto'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="documento_nro_documento">N&uacute;mero de Documento</label></th>
        <td>
          <?php echo $form['nro_documento']->renderError() ?>
          <?php echo $form['nro_documento'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="documento_descripcion">Descripci&oacute;n</label></th>
        <td>
          <?php echo $form['descripcion']->renderError() ?>
          <?php echo $form['descripcion'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="documento_fecha_emision">Fecha de Emisi&oacute;n</label></th>
        <td>
          <?php echo $form['fecha_emision']->renderError() ?>
          <?php echo $form['fecha_emision'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
