<h1>Lista de Estados</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Descripci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($estadoList as $estado): ?>
    <tr>
      <td><a href="<?php echo url_for('estado/show?id='.$estado->getId()) ?>"><?php echo $estado->getId() ?></a></td>
      <td><?php echo $estado->getNombre() ?></td>
      <td><?php echo $estado->getDescripcion() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('estado/create') ?>">Crear</a>
