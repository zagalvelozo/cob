<?php

/**
 * estadocausa actions.
 *
 * @package    sgcj
 * @subpackage estadocausa
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class estadocausaActions extends sfActions
{
  public function executeIndex()
  {
    $this->estado_causaList = EstadoCausaPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->estado_causa = EstadoCausaPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->estado_causa);
  }

  public function executeCreate()
  {
    $this->form = new EstadoCausaForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new EstadoCausaForm(EstadoCausaPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new EstadoCausaForm(EstadoCausaPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('estado_causa'));
    if ($this->form->isValid())
    {
      $estado_causa = $this->form->save();

      $this->redirect('estadocausa/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($estado_causa = EstadoCausaPeer::retrieveByPk($request->getParameter('id')));

    $estado_causa->delete();

    $this->redirect('estadocausa/index');
  }
}
