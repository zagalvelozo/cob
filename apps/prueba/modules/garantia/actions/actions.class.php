<?php

/**
 * garantia actions.
 *
 * @package    sgcj
 * @subpackage garantia
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class garantiaActions extends sfActions
{
  public function executeIndex()
  {
    $this->garantiaList = GarantiaPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->garantia = GarantiaPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->garantia);
  }

  public function executeCreate()
  {
    $this->form = new GarantiaForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new GarantiaForm(GarantiaPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new GarantiaForm(GarantiaPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('garantia'));
    if ($this->form->isValid())
    {
      $garantia = $this->form->save();

      $this->redirect('garantia/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($garantia = GarantiaPeer::retrieveByPk($request->getParameter('id')));

    $garantia->delete();

    $this->redirect('garantia/index');
  }
}
