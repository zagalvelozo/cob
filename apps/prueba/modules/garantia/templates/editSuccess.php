<?php $garantia = $form->getObject() ?>
<h1><?php echo $garantia->isNew() ? 'Ingresar' : 'Editar' ?> Garant&iacute;a</h1>

<form action="<?php echo url_for('garantia/update'.(!$garantia->isNew() ? '?id='.$garantia->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('garantia/index') ?>">Cancelar</a>
          <?php if (!$garantia->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'garantia/delete?id='.$garantia->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar la garant&iacute;a?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="garantia_causa_id">Causa</label></th>
        <td>
          <?php echo $form['causa_id']->renderError() ?>
          <?php echo $form['causa_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="garantia_tipo_garantia_id">Tipo de Garant&iacute;a</label></th>
        <td>
          <?php echo $form['tipo_garantia_id']->renderError() ?>
          <?php echo $form['tipo_garantia_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="garantia_valor">Valor</label></th>
        <td>
          <?php echo $form['valor']->renderError() ?>
          <?php echo $form['valor'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
