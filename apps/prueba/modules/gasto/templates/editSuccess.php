<?php $gasto = $form->getObject() ?>
<h1><?php echo $gasto->isNew() ? 'Ingresar' : 'Editar' ?> Gasto</h1>

<form action="<?php echo url_for('gasto/update'.(!$gasto->isNew() ? '?id='.$gasto->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('gasto/index') ?>">Cancelar</a>
          <?php if (!$gasto->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'gasto/delete?id='.$gasto->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar el gasto?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="gasto_tipo_gasto_id">Tipo de Gasto</label></th>
        <td>
          <?php echo $form['tipo_gasto_id']->renderError() ?>
          <?php echo $form['tipo_gasto_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="gasto_diligencia_id">Diligencia</label></th>
        <td>
          <?php echo $form['diligencia_id']->renderError() ?>
          <?php echo $form['diligencia_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="gasto_monto">Monto</label></th>
        <td>
          <?php echo $form['monto']->renderError() ?>
          <?php echo $form['monto'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="gasto_fecha">Fecha</label></th>
        <td>
          <?php echo $form['fecha']->renderError() ?>
          <?php echo $form['fecha'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="gasto_motivo">Motivo</label></th>
        <td>
          <?php echo $form['motivo']->renderError() ?>
          <?php echo $form['motivo'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
