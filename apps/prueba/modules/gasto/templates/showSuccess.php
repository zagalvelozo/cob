<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $gasto->getId() ?></td>
    </tr>
    <tr>
      <th>Tipo de Gasto:</th>
      <td><?php echo $gasto->getTipoGastoId() ?></td>
    </tr>
    <tr>
      <th>Diligencia:</th>
      <td><?php echo $gasto->getDiligenciaId() ?></td>
    </tr>
    <tr>
      <th>Monto:</th>
      <td><?php echo $gasto->getMonto() ?></td>
    </tr>
    <tr>
      <th>Fecha:</th>
      <td><?php echo $gasto->getFecha() ?></td>
    </tr>
    <tr>
      <th>Motivo:</th>
      <td><?php echo $gasto->getMotivo() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('gasto/edit?id='.$gasto->getId()) ?>">Editar</a>
&nbsp;
<a href="<?php echo url_for('gasto/index') ?>">Listar</a>
