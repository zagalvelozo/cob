<?php

/**
 * giro actions.
 *
 * @package    sgcj
 * @subpackage giro
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class giroActions extends sfActions
{
  public function executeIndex()
  {
    $this->giroList = GiroPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->giro = GiroPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->giro);
  }

  public function executeCreate()
  {
    $this->form = new GiroForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new GiroForm(GiroPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new GiroForm(GiroPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('giro'));
    if ($this->form->isValid())
    {
      $giro = $this->form->save();

      $this->redirect('giro/edit?id='.$giro->getId());
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($giro = GiroPeer::retrieveByPk($request->getParameter('id')));

    $giro->delete();

    $this->redirect('giro/index');
  }
}
