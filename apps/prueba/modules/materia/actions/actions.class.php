<?php

/**
 * materia actions.
 *
 * @package    sgcj
 * @subpackage materia
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class materiaActions extends sfActions
{
  public function executeIndex()
  {
    $this->materiaList = MateriaPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->materia = MateriaPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->materia);
  }

  public function executeCreate()
  {
    $this->form = new MateriaForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new MateriaForm(MateriaPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new MateriaForm(MateriaPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('materia'));
    if ($this->form->isValid())
    {
      $materia = $this->form->save();

      $this->redirect('materia/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($materia = MateriaPeer::retrieveByPk($request->getParameter('id')));

    $materia->delete();

    $this->redirect('materia/index');
  }
}
