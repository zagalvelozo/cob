<h1>Lista de Materias</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Descripci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($materiaList as $materia): ?>
    <tr>
      <td><a href="<?php echo url_for('materia/show?id='.$materia->getId()) ?>"><?php echo $materia->getId() ?></a></td>
      <td><?php echo $materia->getNombre() ?></td>
      <td><?php echo $materia->getDescripcion() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('materia/create') ?>">Crear</a>
