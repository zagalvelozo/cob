<?php

/**
 * perfil actions.
 *
 * @package    sgcj
 * @subpackage perfil
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class perfilActions extends sfActions
{
  public function executeIndex()
  {
    $this->perfilList = PerfilPeer::doSelect(new Criteria());
  }

  public function executeCreate()
  {
    $this->form = new PerfilForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new PerfilForm(PerfilPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new PerfilForm(PerfilPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('perfil'));
    if ($this->form->isValid())
    {
      $perfil = $this->form->save();

      $this->redirect('perfil/edit?id='.$perfil->getId());
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($perfil = PerfilPeer::retrieveByPk($request->getParameter('id')));

    $perfil->delete();

    $this->redirect('perfil/index');
  }
}
