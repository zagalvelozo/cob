<?php $periodo = $form->getObject() ?>
<h1><?php echo $periodo->isNew() ? 'Ingresar' : 'Editar' ?> Periodo</h1>

<form action="<?php echo url_for('periodo/update'.(!$periodo->isNew() ? '?id='.$periodo->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('periodo/index') ?>">Cancelar</a>
          <?php if (!$periodo->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'periodo/delete?id='.$periodo->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar el periodo?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="periodo_nombre">Nombre</label></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="periodo_descripcion">Descripci&oacute;n</label></th>
        <td>
          <?php echo $form['descripcion']->renderError() ?>
          <?php echo $form['descripcion'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
