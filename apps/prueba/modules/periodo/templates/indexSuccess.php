<h1>Lista de Periodos</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Descripci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($periodoList as $periodo): ?>
    <tr>
      <td><a href="<?php echo url_for('periodo/show?id='.$periodo->getId()) ?>"><?php echo $periodo->getId() ?></a></td>
      <td><?php echo $periodo->getNombre() ?></td>
      <td><?php echo $periodo->getDescripcion() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('periodo/create') ?>">Crear</a>
