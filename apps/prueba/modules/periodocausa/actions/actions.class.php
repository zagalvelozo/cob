<?php

/**
 * periodocausa actions.
 *
 * @package    sgcj
 * @subpackage periodocausa
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class periodocausaActions extends sfActions
{
  public function executeIndex()
  {
    $this->periodo_causaList = PeriodoCausaPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->periodo_causa = PeriodoCausaPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->periodo_causa);
  }

  public function executeCreate()
  {
    $this->form = new PeriodoCausaForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new PeriodoCausaForm(PeriodoCausaPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new PeriodoCausaForm(PeriodoCausaPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('periodo_causa'));
    if ($this->form->isValid())
    {
      $periodo_causa = $this->form->save();

      $this->redirect('periodocausa/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($periodo_causa = PeriodoCausaPeer::retrieveByPk($request->getParameter('id')));

    $periodo_causa->delete();

    $this->redirect('periodocausa/index');
  }
}
