<?php $periodo_causa = $form->getObject() ?>
<h1><?php echo $periodo_causa->isNew() ? 'Ingresar' : 'Editar' ?> Periodo en Causa</h1>

<form action="<?php echo url_for('periodocausa/update'.(!$periodo_causa->isNew() ? '?id='.$periodo_causa->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('periodocausa/index') ?>">Cancelar</a>
          <?php if (!$periodo_causa->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'periodocausa/delete?id='.$periodo_causa->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar el periodo de la causa?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="periodo_causa_causa_id">Causa</label></th>
        <td>
          <?php echo $form['causa_id']->renderError() ?>
          <?php echo $form['causa_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="periodo_causa_periodo_id">Periodo</label></th>
        <td>
          <?php echo $form['periodo_id']->renderError() ?>
          <?php echo $form['periodo_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="periodo_causa_fecha_inicio">Fecha de Inicio</label></th>
        <td>
          <?php echo $form['fecha_inicio']->renderError() ?>
          <?php echo $form['fecha_inicio'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="periodo_causa_activo">Periodo activo?</label></th>
        <td>
          <?php echo $form['activo']->renderError() ?>
          <?php echo $form['activo'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
