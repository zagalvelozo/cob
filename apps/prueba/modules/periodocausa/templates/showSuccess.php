<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $periodo_causa->getId() ?></td>
    </tr>
    <tr>
      <th>Causa:</th>
      <td><?php echo $periodo_causa->getCausaId() ?></td>
    </tr>
    <tr>
      <th>Periodo:</th>
      <td><?php echo $periodo_causa->getPeriodoId() ?></td>
    </tr>
    <tr>
      <th>Fecha de Inicio:</th>
      <td><?php echo $periodo_causa->getFechaInicio() ?></td>
    </tr>
    <tr>
      <th>Periodo activo?:</th>
      <td><?php echo $periodo_causa->getActivo() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('periodocausa/edit?id='.$periodo_causa->getId()) ?>">Editar</a>
&nbsp;
<a href="<?php echo url_for('periodocausa/index') ?>">Listar</a>
