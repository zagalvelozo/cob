<?php

/**
 * telefonodomicilio actions.
 *
 * @package    sgcj
 * @subpackage telefonodomicilio
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class telefonodomicilioActions extends sfActions
{
  public function executeIndex()
  {
    $this->telefono_domicilioList = TelefonoDomicilioPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->telefono_domicilio = TelefonoDomicilioPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->telefono_domicilio);
  }

  public function executeCreate()
  {
    $this->form = new TelefonoDomicilioForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new TelefonoDomicilioForm(TelefonoDomicilioPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new TelefonoDomicilioForm(TelefonoDomicilioPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('telefono_domicilio'));
    if ($this->form->isValid())
    {
      $telefono_domicilio = $this->form->save();

      $this->redirect('telefonodomicilio/edit?id='.$telefono_domicilio->getId());
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($telefono_domicilio = TelefonoDomicilioPeer::retrieveByPk($request->getParameter('id')));

    $telefono_domicilio->delete();

    $this->redirect('telefonodomicilio/index');
  }
}
