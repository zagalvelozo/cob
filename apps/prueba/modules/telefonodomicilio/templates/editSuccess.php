<?php $telefono_domicilio = $form->getObject() ?>
<h1><?php echo $telefono_domicilio->isNew() ? 'Ingresar' : 'Editar' ?> Tel&eacute;fono en Domicilio de Deudor</h1>

<form action="<?php echo url_for('telefonodomicilio/update'.(!$telefono_domicilio->isNew() ? '?id='.$telefono_domicilio->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('telefonodomicilio/index') ?>">Cancelar</a>
          <?php if (!$telefono_domicilio->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'telefonodomicilio/delete?id='.$telefono_domicilio->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar el tel&eacute;fono del domicilio del deudor?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="telefono_domicilio_deudor_domicilio_id">Domicilio de Deudor:</label></th>
        <td>
          <?php echo $form['deudor_domicilio_id']->renderError() ?>
          <?php echo $form['deudor_domicilio_id'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="telefono_domicilio_telefono">Tel&eacute;fono</label></th>
        <td>
          <?php echo $form['telefono']->renderError() ?>
          <?php echo $form['telefono'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
