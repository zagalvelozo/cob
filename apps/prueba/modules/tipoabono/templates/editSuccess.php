<?php $tipo_abono = $form->getObject() ?>
<h1><?php echo $tipo_abono->isNew() ? 'Ingresar' : 'Editar' ?> Tipo de Abono</h1>

<form action="<?php echo url_for('tipoabono/update'.(!$tipo_abono->isNew() ? '?id='.$tipo_abono->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('tipoabono/index') ?>">Cancelar</a>
          <?php if (!$tipo_abono->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'tipoabono/delete?id='.$tipo_abono->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar el tipo de abono?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="tipo_abono_nombre">Nombre</label></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="tipo_abono_descripcion">Descripci&oacute;n</label></th>
        <td>
          <?php echo $form['descripcion']->renderError() ?>
          <?php echo $form['descripcion'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
