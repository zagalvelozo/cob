<h1>Lista de Tipos de Abono</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Descripci&oacute;n</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($tipo_abonoList as $tipo_abono): ?>
    <tr>
      <td><a href="<?php echo url_for('tipoabono/show?id='.$tipo_abono->getId()) ?>"><?php echo $tipo_abono->getId() ?></a></td>
      <td><?php echo $tipo_abono->getNombre() ?></td>
      <td><?php echo $tipo_abono->getDescripcion() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('tipoabono/create') ?>">Crear</a>
