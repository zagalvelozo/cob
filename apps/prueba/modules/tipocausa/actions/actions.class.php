<?php

/**
 * tipocausa actions.
 *
 * @package    sgcj
 * @subpackage tipocausa
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class tipocausaActions extends sfActions
{
  public function executeIndex()
  {
    $this->tipo_causaList = TipoCausaPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->tipo_causa = TipoCausaPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->tipo_causa);
  }

  public function executeCreate()
  {
    $this->form = new TipoCausaForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new TipoCausaForm(TipoCausaPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new TipoCausaForm(TipoCausaPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('tipo_causa'));
    if ($this->form->isValid())
    {
      $tipo_causa = $this->form->save();

      $this->redirect('tipocausa/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($tipo_causa = TipoCausaPeer::retrieveByPk($request->getParameter('id')));

    $tipo_causa->delete();

    $this->redirect('tipocausa/index');
  }
}
