<?php $tipo_causa = $form->getObject() ?>
<h1><?php echo $tipo_causa->isNew() ? 'Ingresar' : 'Editar' ?> Tipo de Causa</h1>

<form action="<?php echo url_for('tipocausa/update'.(!$tipo_causa->isNew() ? '?id='.$tipo_causa->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          &nbsp;<a href="<?php echo url_for('tipocausa/index') ?>">Cancelar</a>
          <?php if (!$tipo_causa->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'tipocausa/delete?id='.$tipo_causa->getId(), array('post' => true, 'confirm' => 'Esta seguro que desea eliminar el tipo de causa?')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><label for="tipo_causa_nombre">Nombre</label></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><label for="tipo_causa_descripcion">Descripci&oacute;n</label></th>
        <td>
          <?php echo $form['descripcion']->renderError() ?>
          <?php echo $form['descripcion'] ?>

        <?php echo $form['id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
