<?php

/**
 * tipodocumento actions.
 *
 * @package    sgcj
 * @subpackage tipodocumento
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class tipodocumentoActions extends sfActions
{
  public function executeIndex()
  {
    $this->tipo_documentoList = TipoDocumentoPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->tipo_documento = TipoDocumentoPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->tipo_documento);
  }

  public function executeCreate()
  {
    $this->form = new TipoDocumentoForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new TipoDocumentoForm(TipoDocumentoPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new TipoDocumentoForm(TipoDocumentoPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('tipo_documento'));
    if ($this->form->isValid())
    {
      $tipo_documento = $this->form->save();

      $this->redirect('tipodocumento/index');
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($tipo_documento = TipoDocumentoPeer::retrieveByPk($request->getParameter('id')));

    $tipo_documento->delete();

    $this->redirect('tipodocumento/index');
  }
}
