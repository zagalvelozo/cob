<?php

/**
 * tribunal actions.
 *
 * @package    sgcj
 * @subpackage tribunal
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 8507 2008-04-17 17:32:20Z fabien $
 */
class tribunalActions extends sfActions
{
  public function executeIndex()
  {
    $this->tribunalList = TribunalPeer::doSelect(new Criteria());
  }

  public function executeShow($request)
  {
    $this->tribunal = TribunalPeer::retrieveByPk($request->getParameter('id'));
    $this->forward404Unless($this->tribunal);
  }

  public function executeCreate()
  {
    $this->form = new TribunalForm();

    $this->setTemplate('edit');
  }

  public function executeEdit($request)
  {
    $this->form = new TribunalForm(TribunalPeer::retrieveByPk($request->getParameter('id')));
  }

  public function executeUpdate($request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new TribunalForm(TribunalPeer::retrieveByPk($request->getParameter('id')));

    $this->form->bind($request->getParameter('tribunal'));
    if ($this->form->isValid())
    {
      $tribunal = $this->form->save();

      $this->redirect('tribunal/edit?id='.$tribunal->getId());
    }

    $this->setTemplate('edit');
  }

  public function executeDelete($request)
  {
    $this->forward404Unless($tribunal = TribunalPeer::retrieveByPk($request->getParameter('id')));

    $tribunal->delete();

    $this->redirect('tribunal/index');
  }
}
