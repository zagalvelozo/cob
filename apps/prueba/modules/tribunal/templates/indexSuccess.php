<h1>Tribunal List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Comuna</th>
      <th>Nombre</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($tribunalList as $tribunal): ?>
    <tr>
      <td><a href="<?php echo url_for('tribunal/show?id='.$tribunal->getId()) ?>"><?php echo $tribunal->getId() ?></a></td>
      <td><?php echo $tribunal->getComunaId() ?></td>
      <td><?php echo $tribunal->getNombre() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a href="<?php echo url_for('tribunal/create') ?>">Create</a>
