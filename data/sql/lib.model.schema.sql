
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

#-----------------------------------------------------------------------------
#-- acreedor
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `acreedor`;


CREATE TABLE `acreedor`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`comuna_id` INTEGER  NOT NULL,
	`giro_id` INTEGER  NOT NULL,
	`sf_guard_user_id` INTEGER  NOT NULL,
	`rut_acreedor` VARCHAR(12)  NOT NULL,
	`nombres` VARCHAR(50)  NOT NULL,
	`apellido_paterno` VARCHAR(25)  NOT NULL,
	`apellido_materno` VARCHAR(25)  NOT NULL,
	`direccion` TEXT  NOT NULL,
	`telefono` VARCHAR(25)  NOT NULL,
	`fax` VARCHAR(25),
	`email` VARCHAR(50)  NOT NULL,
	`rep_legal` VARCHAR(70)  NOT NULL,
	`rut_rep_legal` VARCHAR(12)  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `unique_acreedor` (`rut_acreedor`),
	INDEX `acreedor_FI_1` (`comuna_id`),
	CONSTRAINT `acreedor_FK_1`
		FOREIGN KEY (`comuna_id`)
		REFERENCES `comuna` (`id`),
	INDEX `acreedor_FI_2` (`giro_id`),
	CONSTRAINT `acreedor_FK_2`
		FOREIGN KEY (`giro_id`)
		REFERENCES `giro` (`id`),
	INDEX `acreedor_FI_3` (`sf_guard_user_id`),
	CONSTRAINT `acreedor_FK_3`
		FOREIGN KEY (`sf_guard_user_id`)
		REFERENCES `sf_guard_user` (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- contrato
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `contrato`;


CREATE TABLE `contrato`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`acreedor_id` INTEGER  NOT NULL,
	`fecha_inicio` DATE  NOT NULL,
	`fecha_termino` DATE,
	`porcentaje_honorario` FLOAT  NOT NULL,
	`estado` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `contrato_FI_1` (`acreedor_id`),
	CONSTRAINT `contrato_FK_1`
		FOREIGN KEY (`acreedor_id`)
		REFERENCES `acreedor` (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- deudor
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `deudor`;


CREATE TABLE `deudor`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`rut_deudor` VARCHAR(12)  NOT NULL,
	`nombres` VARCHAR(50)  NOT NULL,
	`apellido_paterno` VARCHAR(25)  NOT NULL,
	`apellido_materno` VARCHAR(25)  NOT NULL,
	`celular` VARCHAR(25)  NOT NULL,
	`email` VARCHAR(50),
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- deudor_domicilio
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `deudor_domicilio`;


CREATE TABLE `deudor_domicilio`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`deudor_id` INTEGER  NOT NULL,
	`comuna_id` INTEGER  NOT NULL,
	`calle` VARCHAR(50)  NOT NULL,
	`numero` INTEGER  NOT NULL,
	`depto` VARCHAR(10),
	`villa` VARCHAR(25),
	`telefono` VARCHAR(25)  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `deudor_domicilio_FI_1` (`deudor_id`),
	CONSTRAINT `deudor_domicilio_FK_1`
		FOREIGN KEY (`deudor_id`)
		REFERENCES `deudor` (`id`),
	INDEX `deudor_domicilio_FI_2` (`comuna_id`),
	CONSTRAINT `deudor_domicilio_FK_2`
		FOREIGN KEY (`comuna_id`)
		REFERENCES `comuna` (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- documento
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `documento`;


CREATE TABLE `documento`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`tipo_documento_id` INTEGER  NOT NULL,
	`causa_id` INTEGER  NOT NULL,
	`fecha_vencimiento` DATE  NOT NULL,
	`monto` INTEGER  NOT NULL,
	`interes` FLOAT  NOT NULL,
	`nro_documento` VARCHAR(50)  NOT NULL,
	`descripcion` TEXT  NOT NULL,
	`fecha_emision` DATE  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `documento_FI_1` (`tipo_documento_id`),
	CONSTRAINT `documento_FK_1`
		FOREIGN KEY (`tipo_documento_id`)
		REFERENCES `tipo_documento` (`id`),
	INDEX `documento_FI_2` (`causa_id`),
	CONSTRAINT `documento_FK_2`
		FOREIGN KEY (`causa_id`)
		REFERENCES `causa` (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- tipo_documento
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `tipo_documento`;


CREATE TABLE `tipo_documento`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(80)  NOT NULL,
	`descripcion` TEXT  NOT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- materia
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `materia`;


CREATE TABLE `materia`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(50)  NOT NULL,
	`descripcion` TEXT  NOT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- deuda
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `deuda`;


CREATE TABLE `deuda`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`causa_id` INTEGER  NOT NULL,
	`monto` INTEGER  NOT NULL,
	`interes` INTEGER  NOT NULL,
	`gastos_cobranza` INTEGER  NOT NULL,
	`honorarios` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `deuda_FI_1` (`causa_id`),
	CONSTRAINT `deuda_FK_1`
		FOREIGN KEY (`causa_id`)
		REFERENCES `causa` (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- abono
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `abono`;


CREATE TABLE `abono`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`tipo_abono_id` INTEGER  NOT NULL,
	`tipo_pago_id` INTEGER  NOT NULL,
	`deuda_id` INTEGER  NOT NULL,
	`valor` INTEGER  NOT NULL,
	`fecha` DATE  NOT NULL,
	`motivo` TEXT  NOT NULL,
	`nro_recibo` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `abono_FI_1` (`tipo_abono_id`),
	CONSTRAINT `abono_FK_1`
		FOREIGN KEY (`tipo_abono_id`)
		REFERENCES `tipo_abono` (`id`),
	INDEX `abono_FI_2` (`tipo_pago_id`),
	CONSTRAINT `abono_FK_2`
		FOREIGN KEY (`tipo_pago_id`)
		REFERENCES `tipo_pago` (`id`),
	INDEX `abono_FI_3` (`deuda_id`),
	CONSTRAINT `abono_FK_3`
		FOREIGN KEY (`deuda_id`)
		REFERENCES `deuda` (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- tipo_abono
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `tipo_abono`;


CREATE TABLE `tipo_abono`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(50)  NOT NULL,
	`descripcion` TEXT  NOT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- tipo_pago
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `tipo_pago`;


CREATE TABLE `tipo_pago`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(50)  NOT NULL,
	`descripcion` TEXT  NOT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- estado_causa
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `estado_causa`;


CREATE TABLE `estado_causa`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`causa_id` INTEGER  NOT NULL,
	`estado_id` INTEGER  NOT NULL,
	`fecha` DATE  NOT NULL,
	`motivo` TEXT  NOT NULL,
	`activo` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `estado_causa_FI_1` (`causa_id`),
	CONSTRAINT `estado_causa_FK_1`
		FOREIGN KEY (`causa_id`)
		REFERENCES `causa` (`id`),
	INDEX `estado_causa_FI_2` (`estado_id`),
	CONSTRAINT `estado_causa_FK_2`
		FOREIGN KEY (`estado_id`)
		REFERENCES `estado` (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- estado
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `estado`;


CREATE TABLE `estado`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(50)  NOT NULL,
	`descripcion` TEXT  NOT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- periodo_causa
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `periodo_causa`;


CREATE TABLE `periodo_causa`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`causa_id` INTEGER  NOT NULL,
	`periodo_id` INTEGER  NOT NULL,
	`fecha_inicio` DATE  NOT NULL,
	`activo` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `periodo_causa_FI_1` (`causa_id`),
	CONSTRAINT `periodo_causa_FK_1`
		FOREIGN KEY (`causa_id`)
		REFERENCES `causa` (`id`),
	INDEX `periodo_causa_FI_2` (`periodo_id`),
	CONSTRAINT `periodo_causa_FK_2`
		FOREIGN KEY (`periodo_id`)
		REFERENCES `periodo` (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- periodo
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `periodo`;


CREATE TABLE `periodo`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(50)  NOT NULL,
	`descripcion` TEXT  NOT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- diligencia
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `diligencia`;


CREATE TABLE `diligencia`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`periodo_causa_id` INTEGER  NOT NULL,
	`fecha` DATE  NOT NULL,
	`descripcion` TEXT  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `diligencia_FI_1` (`periodo_causa_id`),
	CONSTRAINT `diligencia_FK_1`
		FOREIGN KEY (`periodo_causa_id`)
		REFERENCES `periodo_causa` (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- diligencia_formato
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `diligencia_formato`;


CREATE TABLE `diligencia_formato`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`diligencia_id` INTEGER  NOT NULL,
	`formato_id` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `diligencia_formato_FI_1` (`diligencia_id`),
	CONSTRAINT `diligencia_formato_FK_1`
		FOREIGN KEY (`diligencia_id`)
		REFERENCES `diligencia` (`id`),
	INDEX `diligencia_formato_FI_2` (`formato_id`),
	CONSTRAINT `diligencia_formato_FK_2`
		FOREIGN KEY (`formato_id`)
		REFERENCES `formato` (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- formato
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `formato`;


CREATE TABLE `formato`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`tipo` INTEGER  NOT NULL,
	`nombre` VARCHAR(50)  NOT NULL,
	`cuerpo` TEXT  NOT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- gasto
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `gasto`;


CREATE TABLE `gasto`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`tipo_gasto_id` INTEGER  NOT NULL,
	`diligencia_id` INTEGER  NOT NULL,
	`monto` INTEGER  NOT NULL,
	`fecha` DATE  NOT NULL,
	`motivo` TEXT  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `gasto_FI_1` (`tipo_gasto_id`),
	CONSTRAINT `gasto_FK_1`
		FOREIGN KEY (`tipo_gasto_id`)
		REFERENCES `tipo_gasto` (`id`),
	INDEX `gasto_FI_2` (`diligencia_id`),
	CONSTRAINT `gasto_FK_2`
		FOREIGN KEY (`diligencia_id`)
		REFERENCES `diligencia` (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- tipo_gasto
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `tipo_gasto`;


CREATE TABLE `tipo_gasto`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(50)  NOT NULL,
	`descripcion` TEXT  NOT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- tribunal
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `tribunal`;


CREATE TABLE `tribunal`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`comuna_id` INTEGER  NOT NULL,
	`nombre` VARCHAR(80)  NOT NULL,
	`direccion` TEXT  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `tribunal_FI_1` (`comuna_id`),
	CONSTRAINT `tribunal_FK_1`
		FOREIGN KEY (`comuna_id`)
		REFERENCES `comuna` (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- causa_tribunal
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `causa_tribunal`;


CREATE TABLE `causa_tribunal`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`causa_id` INTEGER  NOT NULL,
	`tribunal_id` INTEGER  NOT NULL,
	`fecha_cambio` DATE  NOT NULL,
	`motivo` TEXT  NOT NULL,
	`activo` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `causa_tribunal_FI_1` (`causa_id`),
	CONSTRAINT `causa_tribunal_FK_1`
		FOREIGN KEY (`causa_id`)
		REFERENCES `causa` (`id`),
	INDEX `causa_tribunal_FI_2` (`tribunal_id`),
	CONSTRAINT `causa_tribunal_FK_2`
		FOREIGN KEY (`tribunal_id`)
		REFERENCES `tribunal` (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- garantia
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `garantia`;


CREATE TABLE `garantia`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`causa_id` INTEGER  NOT NULL,
	`tipo_garantia_id` INTEGER  NOT NULL,
	`valor` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `garantia_FI_1` (`causa_id`),
	CONSTRAINT `garantia_FK_1`
		FOREIGN KEY (`causa_id`)
		REFERENCES `causa` (`id`),
	INDEX `garantia_FI_2` (`tipo_garantia_id`),
	CONSTRAINT `garantia_FK_2`
		FOREIGN KEY (`tipo_garantia_id`)
		REFERENCES `tipo_garantia` (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- tipo_garantia
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `tipo_garantia`;


CREATE TABLE `tipo_garantia`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(50)  NOT NULL,
	`descripcion` TEXT  NOT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- tipo_causa
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `tipo_causa`;


CREATE TABLE `tipo_causa`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(50)  NOT NULL,
	`descripcion` TEXT  NOT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- causa
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `causa`;


CREATE TABLE `causa`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`tipo_causa_id` INTEGER  NOT NULL,
	`deudor_id` INTEGER  NOT NULL,
	`materia_id` INTEGER  NOT NULL,
	`contrato_id` INTEGER  NOT NULL,
	`rol` VARCHAR(50)  NOT NULL,
	`competencia_id` INTEGER  NOT NULL,
	`fecha_inicio` DATE  NOT NULL,
	`herencia_exhorto` INTEGER,
	`causa_exhorto` INTEGER,
	PRIMARY KEY (`id`),
	INDEX `causa_FI_1` (`tipo_causa_id`),
	CONSTRAINT `causa_FK_1`
		FOREIGN KEY (`tipo_causa_id`)
		REFERENCES `tipo_causa` (`id`),
	INDEX `causa_FI_2` (`deudor_id`),
	CONSTRAINT `causa_FK_2`
		FOREIGN KEY (`deudor_id`)
		REFERENCES `deudor` (`id`),
	INDEX `causa_FI_3` (`materia_id`),
	CONSTRAINT `causa_FK_3`
		FOREIGN KEY (`materia_id`)
		REFERENCES `materia` (`id`),
	INDEX `causa_FI_4` (`contrato_id`),
	CONSTRAINT `causa_FK_4`
		FOREIGN KEY (`contrato_id`)
		REFERENCES `contrato` (`id`),
	INDEX `causa_FI_5` (`competencia_id`),
	CONSTRAINT `causa_FK_5`
		FOREIGN KEY (`competencia_id`)
		REFERENCES `competencia` (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- telefono_domicilio
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `telefono_domicilio`;


CREATE TABLE `telefono_domicilio`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`deudor_domicilio_id` INTEGER  NOT NULL,
	`telefono` VARCHAR(25)  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `telefono_domicilio_FI_1` (`deudor_domicilio_id`),
	CONSTRAINT `telefono_domicilio_FK_1`
		FOREIGN KEY (`deudor_domicilio_id`)
		REFERENCES `deudor_domicilio` (`id`)
		ON DELETE CASCADE
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- clase_giro
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `clase_giro`;


CREATE TABLE `clase_giro`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(80)  NOT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- giro
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `giro`;


CREATE TABLE `giro`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`clase_giro_id` INTEGER  NOT NULL,
	`nombre` VARCHAR(80)  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `giro_FI_1` (`clase_giro_id`),
	CONSTRAINT `giro_FK_1`
		FOREIGN KEY (`clase_giro_id`)
		REFERENCES `clase_giro` (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- region
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `region`;


CREATE TABLE `region`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(80)  NOT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- ciudad
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `ciudad`;


CREATE TABLE `ciudad`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`region_id` INTEGER  NOT NULL,
	`nombre` VARCHAR(25)  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `ciudad_FI_1` (`region_id`),
	CONSTRAINT `ciudad_FK_1`
		FOREIGN KEY (`region_id`)
		REFERENCES `region` (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- comuna
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `comuna`;


CREATE TABLE `comuna`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`ciudad_id` INTEGER  NOT NULL,
	`nombre` VARCHAR(25)  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `comuna_FI_1` (`ciudad_id`),
	CONSTRAINT `comuna_FK_1`
		FOREIGN KEY (`ciudad_id`)
		REFERENCES `ciudad` (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- competencia
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `competencia`;


CREATE TABLE `competencia`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(80)  NOT NULL,
	`descripcion` TEXT,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- perfil
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `perfil`;


CREATE TABLE `perfil`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`sf_guard_user_id` INTEGER  NOT NULL,
	`password1` VARCHAR(128),
	`password2` VARCHAR(128),
	`password3` VARCHAR(128),
	`pregunta` VARCHAR(128),
	`respuesta` VARCHAR(128),
	`ultimo_cambio` DATE  NOT NULL,
	`nombres` VARCHAR(50),
	`apellido_paterno` VARCHAR(25),
	`apellido_materno` VARCHAR(25),
	`comuna_id` INTEGER,
	`direccion` TEXT,
	`telefono` VARCHAR(25),
	`fax` VARCHAR(25),
	`email` VARCHAR(50),
	`envio_mail` INTEGER,
	PRIMARY KEY (`id`),
	INDEX `perfil_FI_1` (`sf_guard_user_id`),
	CONSTRAINT `perfil_FK_1`
		FOREIGN KEY (`sf_guard_user_id`)
		REFERENCES `sf_guard_user` (`id`),
	INDEX `perfil_FI_2` (`comuna_id`),
	CONSTRAINT `perfil_FK_2`
		FOREIGN KEY (`comuna_id`)
		REFERENCES `comuna` (`id`)
)ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- agenda
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `agenda`;


CREATE TABLE `agenda`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`perfil_id` INTEGER  NOT NULL,
	`descripcion` TEXT  NOT NULL,
	`fecha` DATETIME  NOT NULL,
	`realizada` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `agenda_FI_1` (`perfil_id`),
	CONSTRAINT `agenda_FK_1`
		FOREIGN KEY (`perfil_id`)
		REFERENCES `sf_guard_user` (`id`)
)ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
