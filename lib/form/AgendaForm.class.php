<?php

/**
 * Agenda form.
 *
 * @package    form
 * @subpackage agenda
 * @version    SVN: $Id: sfPropelFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class AgendaForm extends BaseAgendaForm
{
  
  public function configure()
  {
  	$dias = Funciones::getArrayDias();
  	$meses = Funciones::getArrayMeses();
  	$anios = Funciones::getArrayAnios(0,2);
  	
  	$this->widgetSchema['perfil_id'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['realizada'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['fecha'] = new sfWidgetFormDateTime(array('time'=>array('format' => '%hour%/%minute%','empty_values' => array('hour' => 'Hr.','minute' => 'Min.')),'date'=>array('format' => '%day%/%month%/%year%','years'=>$anios,'months'=>$meses,'days'=>$dias,'empty_values' => array('year' => 'A&ntilde;o','month' => 'Mes','day' => 'D&iacute;a'))));
  	
  	$this->widgetSchema['fecha_actual'] = new sfWidgetFormDateTime(array('time'=>array('format' => '%hour%/%minute%','empty_values' => array('hour' => 'Hr.','minute' => 'Min.')),'date'=>array('format' => '%day%/%month%/%year%','years'=>$anios,'months'=>$meses,'days'=>$dias,'empty_values' => array('year' => 'A&ntilde;o','month' => 'Mes','day' => 'D&iacute;a'))));
  	
  	$this->validatorSchema['fecha_actual'] = new sfValidatorDateTime(array('required'=>true));
  	
  	$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
  	
  	$this->validatorSchema->setPostValidator(new sfValidatorSchemaCompare('fecha',sfValidatorSchemaCompare::GREATER_THAN,'fecha_actual',
							    	array('required'=> true,'throw_global_error' => false),
							    	array('invalid' => 'La fecha de agendamiento ("%left_field%") debe ser posterior a la actual ("%right_field%")')
							    	)
							);
							
	
  }
}
