<?php

/**
 * Perfil form.
 *
 * @package    form
 * @subpackage perfil
 * @version    SVN: $Id: sfPropelFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class PerfilForm extends BasePerfilForm
{
  public function configure()
  {
  	$this->widgetSchema['sf_guard_user_id'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['password1'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['password2'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['password3'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['ultimo_cambio'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['comuna_id'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['pregunta'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['respuesta'] = new sfWidgetFormInputHidden();
  	$this->widgetSchema['envio_mail'] = new sfWidgetFormInputHidden();
  	
  	
  	$this->widgetSchema->setLabels(array(
	      'apellido_paterno' => 'Apellido Paterno',
	      'apellido_materno' => 'Apellido Materno',
	      'direccion'        => 'Direcci&oacute;n',
		));
  }
}
