<?php

/**
 * Abono form base class.
 *
 * @package    form
 * @subpackage abono
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseAbonoForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'tipo_abono_id' => new sfWidgetFormPropelSelect(array('model' => 'TipoAbono', 'add_empty' => false)),
      'tipo_pago_id'  => new sfWidgetFormPropelSelect(array('model' => 'TipoPago', 'add_empty' => false)),
      'deuda_id'      => new sfWidgetFormPropelSelect(array('model' => 'Deuda', 'add_empty' => false)),
      'valor'         => new sfWidgetFormInput(),
      'fecha'         => new sfWidgetFormDate(),
      'motivo'        => new sfWidgetFormTextarea(),
      'nro_recibo'    => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorPropelChoice(array('model' => 'Abono', 'column' => 'id', 'required' => false)),
      'tipo_abono_id' => new sfValidatorPropelChoice(array('model' => 'TipoAbono', 'column' => 'id')),
      'tipo_pago_id'  => new sfValidatorPropelChoice(array('model' => 'TipoPago', 'column' => 'id')),
      'deuda_id'      => new sfValidatorPropelChoice(array('model' => 'Deuda', 'column' => 'id')),
      'valor'         => new sfValidatorInteger(),
      'fecha'         => new sfValidatorDate(),
      'motivo'        => new sfValidatorString(),
      'nro_recibo'    => new sfValidatorInteger(),
    ));

    $this->widgetSchema->setNameFormat('abono[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Abono';
  }


}
