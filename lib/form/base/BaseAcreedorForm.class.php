<?php

/**
 * Acreedor form base class.
 *
 * @package    form
 * @subpackage acreedor
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseAcreedorForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'comuna_id'        => new sfWidgetFormPropelSelect(array('model' => 'Comuna', 'add_empty' => false)),
      'giro_id'          => new sfWidgetFormPropelSelect(array('model' => 'Giro', 'add_empty' => false)),
      'sf_guard_user_id' => new sfWidgetFormPropelSelect(array('model' => 'sfGuardUser', 'add_empty' => false)),
      'rut_acreedor'     => new sfWidgetFormInput(),
      'nombres'          => new sfWidgetFormInput(),
      'apellido_paterno' => new sfWidgetFormInput(),
      'apellido_materno' => new sfWidgetFormInput(),
      'direccion'        => new sfWidgetFormTextarea(),
      'telefono'         => new sfWidgetFormInput(),
      'fax'              => new sfWidgetFormInput(),
      'email'            => new sfWidgetFormInput(),
      'rep_legal'        => new sfWidgetFormInput(),
      'rut_rep_legal'    => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorPropelChoice(array('model' => 'Acreedor', 'column' => 'id', 'required' => false)),
      'comuna_id'        => new sfValidatorPropelChoice(array('model' => 'Comuna', 'column' => 'id')),
      'giro_id'          => new sfValidatorPropelChoice(array('model' => 'Giro', 'column' => 'id')),
      'sf_guard_user_id' => new sfValidatorPropelChoice(array('model' => 'sfGuardUser', 'column' => 'id')),
      'rut_acreedor'     => new sfValidatorString(array('max_length' => 12)),
      'nombres'          => new sfValidatorString(array('max_length' => 50)),
      'apellido_paterno' => new sfValidatorString(array('max_length' => 25)),
      'apellido_materno' => new sfValidatorString(array('max_length' => 25)),
      'direccion'        => new sfValidatorString(),
      'telefono'         => new sfValidatorString(array('max_length' => 25)),
      'fax'              => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'email'            => new sfValidatorString(array('max_length' => 50)),
      'rep_legal'        => new sfValidatorString(array('max_length' => 70)),
      'rut_rep_legal'    => new sfValidatorString(array('max_length' => 12)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorPropelUnique(array('model' => 'Acreedor', 'column' => array('rut_acreedor')))
    );

    $this->widgetSchema->setNameFormat('acreedor[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Acreedor';
  }


}
