<?php

/**
 * Causa form base class.
 *
 * @package    form
 * @subpackage causa
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseCausaForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'tipo_causa_id'    => new sfWidgetFormPropelSelect(array('model' => 'TipoCausa', 'add_empty' => false)),
      'deudor_id'        => new sfWidgetFormPropelSelect(array('model' => 'Deudor', 'add_empty' => false)),
      'materia_id'       => new sfWidgetFormPropelSelect(array('model' => 'Materia', 'add_empty' => false)),
      'contrato_id'      => new sfWidgetFormPropelSelect(array('model' => 'Contrato', 'add_empty' => false)),
      'rol'              => new sfWidgetFormInput(),
      'competencia_id'   => new sfWidgetFormPropelSelect(array('model' => 'Competencia', 'add_empty' => false)),
      'fecha_inicio'     => new sfWidgetFormDate(),
      'herencia_exhorto' => new sfWidgetFormInputCheckbox(),
      'causa_exhorto'    => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorPropelChoice(array('model' => 'Causa', 'column' => 'id', 'required' => false)),
      'tipo_causa_id'    => new sfValidatorPropelChoice(array('model' => 'TipoCausa', 'column' => 'id')),
      'deudor_id'        => new sfValidatorPropelChoice(array('model' => 'Deudor', 'column' => 'id')),
      'materia_id'       => new sfValidatorPropelChoice(array('model' => 'Materia', 'column' => 'id')),
      'contrato_id'      => new sfValidatorPropelChoice(array('model' => 'Contrato', 'column' => 'id')),
      'rol'              => new sfValidatorString(array('max_length' => 50)),
      'competencia_id'   => new sfValidatorPropelChoice(array('model' => 'Competencia', 'column' => 'id')),
      'fecha_inicio'     => new sfValidatorDate(),
      'herencia_exhorto' => new sfValidatorBoolean(array('required' => false)),
      'causa_exhorto'    => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('causa[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Causa';
  }


}
