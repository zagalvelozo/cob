<?php

/**
 * CausaTribunal form base class.
 *
 * @package    form
 * @subpackage causa_tribunal
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseCausaTribunalForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'causa_id'     => new sfWidgetFormPropelSelect(array('model' => 'Causa', 'add_empty' => false)),
      'tribunal_id'  => new sfWidgetFormPropelSelect(array('model' => 'Tribunal', 'add_empty' => false)),
      'fecha_cambio' => new sfWidgetFormDate(),
      'motivo'       => new sfWidgetFormTextarea(),
      'activo'       => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorPropelChoice(array('model' => 'CausaTribunal', 'column' => 'id', 'required' => false)),
      'causa_id'     => new sfValidatorPropelChoice(array('model' => 'Causa', 'column' => 'id')),
      'tribunal_id'  => new sfValidatorPropelChoice(array('model' => 'Tribunal', 'column' => 'id')),
      'fecha_cambio' => new sfValidatorDate(),
      'motivo'       => new sfValidatorString(),
      'activo'       => new sfValidatorBoolean(),
    ));

    $this->widgetSchema->setNameFormat('causa_tribunal[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'CausaTribunal';
  }


}
