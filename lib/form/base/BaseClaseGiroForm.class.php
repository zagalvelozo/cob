<?php

/**
 * ClaseGiro form base class.
 *
 * @package    form
 * @subpackage clase_giro
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseClaseGiroForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'     => new sfWidgetFormInputHidden(),
      'nombre' => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'     => new sfValidatorPropelChoice(array('model' => 'ClaseGiro', 'column' => 'id', 'required' => false)),
      'nombre' => new sfValidatorString(array('max_length' => 80)),
    ));

    $this->widgetSchema->setNameFormat('clase_giro[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'ClaseGiro';
  }


}
