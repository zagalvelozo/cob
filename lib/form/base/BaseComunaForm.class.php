<?php

/**
 * Comuna form base class.
 *
 * @package    form
 * @subpackage comuna
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseComunaForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'        => new sfWidgetFormInputHidden(),
      'ciudad_id' => new sfWidgetFormPropelSelect(array('model' => 'Ciudad', 'add_empty' => false)),
      'nombre'    => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'        => new sfValidatorPropelChoice(array('model' => 'Comuna', 'column' => 'id', 'required' => false)),
      'ciudad_id' => new sfValidatorPropelChoice(array('model' => 'Ciudad', 'column' => 'id')),
      'nombre'    => new sfValidatorString(array('max_length' => 25)),
    ));

    $this->widgetSchema->setNameFormat('comuna[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Comuna';
  }


}
