<?php

/**
 * Contrato form base class.
 *
 * @package    form
 * @subpackage contrato
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseContratoForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'acreedor_id'          => new sfWidgetFormPropelSelect(array('model' => 'Acreedor', 'add_empty' => false)),
      'fecha_inicio'         => new sfWidgetFormDate(),
      'fecha_termino'        => new sfWidgetFormDate(),
      'porcentaje_honorario' => new sfWidgetFormInput(),
      'estado'               => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorPropelChoice(array('model' => 'Contrato', 'column' => 'id', 'required' => false)),
      'acreedor_id'          => new sfValidatorPropelChoice(array('model' => 'Acreedor', 'column' => 'id')),
      'fecha_inicio'         => new sfValidatorDate(),
      'fecha_termino'        => new sfValidatorDate(array('required' => false)),
      'porcentaje_honorario' => new sfValidatorNumber(),
      'estado'               => new sfValidatorBoolean(),
    ));

    $this->widgetSchema->setNameFormat('contrato[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Contrato';
  }


}
