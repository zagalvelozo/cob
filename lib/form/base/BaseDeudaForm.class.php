<?php

/**
 * Deuda form base class.
 *
 * @package    form
 * @subpackage deuda
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseDeudaForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'causa_id'        => new sfWidgetFormPropelSelect(array('model' => 'Causa', 'add_empty' => false)),
      'monto'           => new sfWidgetFormInput(),
      'interes'         => new sfWidgetFormInput(),
      'gastos_cobranza' => new sfWidgetFormInput(),
      'honorarios'      => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorPropelChoice(array('model' => 'Deuda', 'column' => 'id', 'required' => false)),
      'causa_id'        => new sfValidatorPropelChoice(array('model' => 'Causa', 'column' => 'id')),
      'monto'           => new sfValidatorInteger(),
      'interes'         => new sfValidatorInteger(),
      'gastos_cobranza' => new sfValidatorInteger(),
      'honorarios'      => new sfValidatorInteger(),
    ));

    $this->widgetSchema->setNameFormat('deuda[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Deuda';
  }


}
