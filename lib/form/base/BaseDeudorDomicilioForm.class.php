<?php

/**
 * DeudorDomicilio form base class.
 *
 * @package    form
 * @subpackage deudor_domicilio
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseDeudorDomicilioForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'        => new sfWidgetFormInputHidden(),
      'deudor_id' => new sfWidgetFormPropelSelect(array('model' => 'Deudor', 'add_empty' => false)),
      'comuna_id' => new sfWidgetFormPropelSelect(array('model' => 'Comuna', 'add_empty' => false)),
      'calle'     => new sfWidgetFormInput(),
      'numero'    => new sfWidgetFormInput(),
      'depto'     => new sfWidgetFormInput(),
      'villa'     => new sfWidgetFormInput(),
      'telefono'  => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'        => new sfValidatorPropelChoice(array('model' => 'DeudorDomicilio', 'column' => 'id', 'required' => false)),
      'deudor_id' => new sfValidatorPropelChoice(array('model' => 'Deudor', 'column' => 'id')),
      'comuna_id' => new sfValidatorPropelChoice(array('model' => 'Comuna', 'column' => 'id')),
      'calle'     => new sfValidatorString(array('max_length' => 50)),
      'numero'    => new sfValidatorInteger(),
      'depto'     => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'villa'     => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'telefono'  => new sfValidatorString(array('max_length' => 25)),
    ));

    $this->widgetSchema->setNameFormat('deudor_domicilio[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'DeudorDomicilio';
  }


}
