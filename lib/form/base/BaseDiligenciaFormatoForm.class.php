<?php

/**
 * DiligenciaFormato form base class.
 *
 * @package    form
 * @subpackage diligencia_formato
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseDiligenciaFormatoForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'diligencia_id' => new sfWidgetFormPropelSelect(array('model' => 'Diligencia', 'add_empty' => false)),
      'formato_id'    => new sfWidgetFormPropelSelect(array('model' => 'Formato', 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorPropelChoice(array('model' => 'DiligenciaFormato', 'column' => 'id', 'required' => false)),
      'diligencia_id' => new sfValidatorPropelChoice(array('model' => 'Diligencia', 'column' => 'id')),
      'formato_id'    => new sfValidatorPropelChoice(array('model' => 'Formato', 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('diligencia_formato[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'DiligenciaFormato';
  }


}
