<?php

/**
 * Perfil form base class.
 *
 * @package    form
 * @subpackage perfil
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BasePerfilForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'sf_guard_user_id' => new sfWidgetFormPropelSelect(array('model' => 'sfGuardUser', 'add_empty' => false)),
      'password1'        => new sfWidgetFormInput(),
      'password2'        => new sfWidgetFormInput(),
      'password3'        => new sfWidgetFormInput(),
      'pregunta'         => new sfWidgetFormInput(),
      'respuesta'        => new sfWidgetFormInput(),
      'ultimo_cambio'    => new sfWidgetFormDate(),
      'nombres'          => new sfWidgetFormInput(),
      'apellido_paterno' => new sfWidgetFormInput(),
      'apellido_materno' => new sfWidgetFormInput(),
      'comuna_id'        => new sfWidgetFormPropelSelect(array('model' => 'Comuna', 'add_empty' => true)),
      'direccion'        => new sfWidgetFormTextarea(),
      'telefono'         => new sfWidgetFormInput(),
      'fax'              => new sfWidgetFormInput(),
      'email'            => new sfWidgetFormInput(),
      'envio_mail'       => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorPropelChoice(array('model' => 'Perfil', 'column' => 'id', 'required' => false)),
      'sf_guard_user_id' => new sfValidatorPropelChoice(array('model' => 'sfGuardUser', 'column' => 'id')),
      'password1'        => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'password2'        => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'password3'        => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'pregunta'         => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'respuesta'        => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'ultimo_cambio'    => new sfValidatorDate(),
      'nombres'          => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'apellido_paterno' => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'apellido_materno' => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'comuna_id'        => new sfValidatorPropelChoice(array('model' => 'Comuna', 'column' => 'id', 'required' => false)),
      'direccion'        => new sfValidatorString(array('required' => false)),
      'telefono'         => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'fax'              => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'email'            => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'envio_mail'       => new sfValidatorBoolean(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('perfil[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Perfil';
  }


}
