<?php

/**
 * PeriodoCausa form base class.
 *
 * @package    form
 * @subpackage periodo_causa
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BasePeriodoCausaForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'causa_id'     => new sfWidgetFormPropelSelect(array('model' => 'Causa', 'add_empty' => false)),
      'periodo_id'   => new sfWidgetFormPropelSelect(array('model' => 'Periodo', 'add_empty' => false)),
      'fecha_inicio' => new sfWidgetFormDate(),
      'activo'       => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorPropelChoice(array('model' => 'PeriodoCausa', 'column' => 'id', 'required' => false)),
      'causa_id'     => new sfValidatorPropelChoice(array('model' => 'Causa', 'column' => 'id')),
      'periodo_id'   => new sfValidatorPropelChoice(array('model' => 'Periodo', 'column' => 'id')),
      'fecha_inicio' => new sfValidatorDate(),
      'activo'       => new sfValidatorBoolean(),
    ));

    $this->widgetSchema->setNameFormat('periodo_causa[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PeriodoCausa';
  }


}
