<?php

/**
 * TelefonoDomicilio form base class.
 *
 * @package    form
 * @subpackage telefono_domicilio
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseTelefonoDomicilioForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'deudor_domicilio_id' => new sfWidgetFormPropelSelect(array('model' => 'DeudorDomicilio', 'add_empty' => false)),
      'telefono'            => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorPropelChoice(array('model' => 'TelefonoDomicilio', 'column' => 'id', 'required' => false)),
      'deudor_domicilio_id' => new sfValidatorPropelChoice(array('model' => 'DeudorDomicilio', 'column' => 'id')),
      'telefono'            => new sfValidatorString(array('max_length' => 25)),
    ));

    $this->widgetSchema->setNameFormat('telefono_domicilio[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'TelefonoDomicilio';
  }


}
