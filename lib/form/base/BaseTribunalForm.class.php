<?php

/**
 * Tribunal form base class.
 *
 * @package    form
 * @subpackage tribunal
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 15484 2009-02-13 13:13:51Z fabien $
 */
class BaseTribunalForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'        => new sfWidgetFormInputHidden(),
      'comuna_id' => new sfWidgetFormPropelSelect(array('model' => 'Comuna', 'add_empty' => false)),
      'nombre'    => new sfWidgetFormInput(),
      'direccion' => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'        => new sfValidatorPropelChoice(array('model' => 'Tribunal', 'column' => 'id', 'required' => false)),
      'comuna_id' => new sfValidatorPropelChoice(array('model' => 'Comuna', 'column' => 'id')),
      'nombre'    => new sfValidatorString(array('max_length' => 80)),
      'direccion' => new sfValidatorString(),
    ));

    $this->widgetSchema->setNameFormat('tribunal[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Tribunal';
  }


}
