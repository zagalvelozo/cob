<?php

/**
 * Subclass for representing a row from the 'perfil' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Perfil extends BasePerfil
{
	public function debeCambiarPass($usuario)
	{
		$perfilUsuario = $usuario->getPerfil();
		
		$fecha2['ano'] = $perfilUsuario->getUltimoCambio('Y');
		$fecha2['mes'] = $perfilUsuario->getUltimoCambio('m');
		$fecha2['dia'] = $perfilUsuario->getUltimoCambio('d');
		
		$fecha1['ano'] = date('Y');
		$fecha1['mes'] = date('m');
		$fecha1['dia'] = date('d');
		
		$diferencia = Funciones::diferenciaDias($fecha1,$fecha2);
		
		if($diferencia > 30)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function getNombreCompleto()
	{
		return $this->getNombres() . ' ' . $this->getApellidoPaterno() . ' ' . $this->getApellidoMaterno();
	}
	
	public function tienePregunta()
	{
		if($this->getPregunta() != '')
		{
			return true;
		}
		
		return false;
	}
}
