<?php


/**
 * This class adds structure of 'acreedor' table to 'propel' DatabaseMap object.
 *
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class AcreedorMapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.AcreedorMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('acreedor');
		$tMap->setPhpName('Acreedor');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('COMUNA_ID', 'ComunaId', 'int', CreoleTypes::INTEGER, 'comuna', 'ID', true, null);

		$tMap->addForeignKey('GIRO_ID', 'GiroId', 'int', CreoleTypes::INTEGER, 'giro', 'ID', true, null);

		$tMap->addForeignKey('SF_GUARD_USER_ID', 'SfGuardUserId', 'int', CreoleTypes::INTEGER, 'sf_guard_user', 'ID', true, null);

		$tMap->addColumn('RUT_ACREEDOR', 'RutAcreedor', 'string', CreoleTypes::VARCHAR, true, 12);

		$tMap->addColumn('NOMBRES', 'Nombres', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('APELLIDO_PATERNO', 'ApellidoPaterno', 'string', CreoleTypes::VARCHAR, true, 25);

		$tMap->addColumn('APELLIDO_MATERNO', 'ApellidoMaterno', 'string', CreoleTypes::VARCHAR, true, 25);

		$tMap->addColumn('DIRECCION', 'Direccion', 'string', CreoleTypes::LONGVARCHAR, true, null);

		$tMap->addColumn('TELEFONO', 'Telefono', 'string', CreoleTypes::VARCHAR, true, 25);

		$tMap->addColumn('FAX', 'Fax', 'string', CreoleTypes::VARCHAR, false, 25);

		$tMap->addColumn('EMAIL', 'Email', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('REP_LEGAL', 'RepLegal', 'string', CreoleTypes::VARCHAR, true, 70);

		$tMap->addColumn('RUT_REP_LEGAL', 'RutRepLegal', 'string', CreoleTypes::VARCHAR, true, 12);

	} // doBuild()

} // AcreedorMapBuilder
