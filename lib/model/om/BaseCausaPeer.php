<?php

/**
 * Base static class for performing query and update operations on the 'causa' table.
 *
 * 
 *
 * @package    lib.model.om
 */
abstract class BaseCausaPeer {

	/** the default database name for this class */
	const DATABASE_NAME = 'propel';

	/** the table name for this class */
	const TABLE_NAME = 'causa';

	/** A class that can be returned by this peer. */
	const CLASS_DEFAULT = 'lib.model.Causa';

	/** The total number of columns. */
	const NUM_COLUMNS = 10;

	/** The number of lazy-loaded columns. */
	const NUM_LAZY_LOAD_COLUMNS = 0;


	/** the column name for the ID field */
	const ID = 'causa.ID';

	/** the column name for the TIPO_CAUSA_ID field */
	const TIPO_CAUSA_ID = 'causa.TIPO_CAUSA_ID';

	/** the column name for the DEUDOR_ID field */
	const DEUDOR_ID = 'causa.DEUDOR_ID';

	/** the column name for the MATERIA_ID field */
	const MATERIA_ID = 'causa.MATERIA_ID';

	/** the column name for the CONTRATO_ID field */
	const CONTRATO_ID = 'causa.CONTRATO_ID';

	/** the column name for the ROL field */
	const ROL = 'causa.ROL';

	/** the column name for the COMPETENCIA_ID field */
	const COMPETENCIA_ID = 'causa.COMPETENCIA_ID';

	/** the column name for the FECHA_INICIO field */
	const FECHA_INICIO = 'causa.FECHA_INICIO';

	/** the column name for the HERENCIA_EXHORTO field */
	const HERENCIA_EXHORTO = 'causa.HERENCIA_EXHORTO';

	/** the column name for the CAUSA_EXHORTO field */
	const CAUSA_EXHORTO = 'causa.CAUSA_EXHORTO';

	/** The PHP to DB Name Mapping */
	private static $phpNameMap = null;


	/**
	 * holds an array of fieldnames
	 *
	 * first dimension keys are the type constants
	 * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
	 */
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'TipoCausaId', 'DeudorId', 'MateriaId', 'ContratoId', 'Rol', 'CompetenciaId', 'FechaInicio', 'HerenciaExhorto', 'CausaExhorto', ),
		BasePeer::TYPE_COLNAME => array (CausaPeer::ID, CausaPeer::TIPO_CAUSA_ID, CausaPeer::DEUDOR_ID, CausaPeer::MATERIA_ID, CausaPeer::CONTRATO_ID, CausaPeer::ROL, CausaPeer::COMPETENCIA_ID, CausaPeer::FECHA_INICIO, CausaPeer::HERENCIA_EXHORTO, CausaPeer::CAUSA_EXHORTO, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'tipo_causa_id', 'deudor_id', 'materia_id', 'contrato_id', 'rol', 'competencia_id', 'fecha_inicio', 'herencia_exhorto', 'causa_exhorto', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
	);

	/**
	 * holds an array of keys for quick access to the fieldnames array
	 *
	 * first dimension keys are the type constants
	 * e.g. self::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
	 */
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'TipoCausaId' => 1, 'DeudorId' => 2, 'MateriaId' => 3, 'ContratoId' => 4, 'Rol' => 5, 'CompetenciaId' => 6, 'FechaInicio' => 7, 'HerenciaExhorto' => 8, 'CausaExhorto' => 9, ),
		BasePeer::TYPE_COLNAME => array (CausaPeer::ID => 0, CausaPeer::TIPO_CAUSA_ID => 1, CausaPeer::DEUDOR_ID => 2, CausaPeer::MATERIA_ID => 3, CausaPeer::CONTRATO_ID => 4, CausaPeer::ROL => 5, CausaPeer::COMPETENCIA_ID => 6, CausaPeer::FECHA_INICIO => 7, CausaPeer::HERENCIA_EXHORTO => 8, CausaPeer::CAUSA_EXHORTO => 9, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'tipo_causa_id' => 1, 'deudor_id' => 2, 'materia_id' => 3, 'contrato_id' => 4, 'rol' => 5, 'competencia_id' => 6, 'fecha_inicio' => 7, 'herencia_exhorto' => 8, 'causa_exhorto' => 9, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
	);

	/**
	 * @return     MapBuilder the map builder for this peer
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function getMapBuilder()
	{
		return BasePeer::getMapBuilder('lib.model.map.CausaMapBuilder');
	}
	/**
	 * Gets a map (hash) of PHP names to DB column names.
	 *
	 * @return     array The PHP to DB name map for this peer
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 * @deprecated Use the getFieldNames() and translateFieldName() methods instead of this.
	 */
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = CausaPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	/**
	 * Translates a fieldname to another type
	 *
	 * @param      string $name field name
	 * @param      string $fromType One of the class type constants TYPE_PHPNAME,
	 *                         TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @param      string $toType   One of the class type constants
	 * @return     string translated name of the field.
	 */
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	/**
	 * Returns an array of of field names.
	 *
	 * @param      string $type The type of fieldnames to return:
	 *                      One of the class type constants TYPE_PHPNAME,
	 *                      TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     array A list of field names
	 */

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	/**
	 * Convenience method which changes table.column to alias.column.
	 *
	 * Using this method you can maintain SQL abstraction while using column aliases.
	 * <code>
	 *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
	 *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
	 * </code>
	 * @param      string $alias The alias for the current table.
	 * @param      string $column The column name for current table. (i.e. CausaPeer::COLUMN_NAME).
	 * @return     string
	 */
	public static function alias($alias, $column)
	{
		return str_replace(CausaPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	/**
	 * Add all the columns needed to create a new object.
	 *
	 * Note: any columns that were marked with lazyLoad="true" in the
	 * XML schema will not be added to the select list and only loaded
	 * on demand.
	 *
	 * @param      criteria object containing the columns to add.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(CausaPeer::ID);

		$criteria->addSelectColumn(CausaPeer::TIPO_CAUSA_ID);

		$criteria->addSelectColumn(CausaPeer::DEUDOR_ID);

		$criteria->addSelectColumn(CausaPeer::MATERIA_ID);

		$criteria->addSelectColumn(CausaPeer::CONTRATO_ID);

		$criteria->addSelectColumn(CausaPeer::ROL);

		$criteria->addSelectColumn(CausaPeer::COMPETENCIA_ID);

		$criteria->addSelectColumn(CausaPeer::FECHA_INICIO);

		$criteria->addSelectColumn(CausaPeer::HERENCIA_EXHORTO);

		$criteria->addSelectColumn(CausaPeer::CAUSA_EXHORTO);

	}

	const COUNT = 'COUNT(causa.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT causa.ID)';

	/**
	 * Returns the number of rows matching criteria.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct Whether to select only distinct columns (You can also set DISTINCT modifier in Criteria).
	 * @param      Connection $con
	 * @return     int Number of matching rows.
	 */
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// clear out anything that might confuse the ORDER BY clause
		$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(CausaPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(CausaPeer::COUNT);
		}

		// just in case we're grouping: add those columns to the select statement
		foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = CausaPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
			// no rows returned; we infer that means 0 matches.
			return 0;
		}
	}
	/**
	 * Method to select one object from the DB.
	 *
	 * @param      Criteria $criteria object used to create the SELECT statement.
	 * @param      Connection $con
	 * @return     Causa
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = CausaPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	/**
	 * Method to do selects.
	 *
	 * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
	 * @param      Connection $con
	 * @return     array Array of selected Objects
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return CausaPeer::populateObjects(CausaPeer::doSelectRS($criteria, $con));
	}
	/**
	 * Prepares the Criteria object and uses the parent doSelect()
	 * method to get a ResultSet.
	 *
	 * Use this method directly if you want to just get the resultset
	 * (instead of an array of objects).
	 *
	 * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
	 * @param      Connection $con the connection to use
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 * @return     ResultSet The resultset object with numerically-indexed fields.
	 * @see        BasePeer::doSelect()
	 */
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			CausaPeer::addSelectColumns($criteria);
		}

		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		// BasePeer returns a Creole ResultSet, set to return
		// rows indexed numerically.
		return BasePeer::doSelect($criteria, $con);
	}
	/**
	 * The returned array will contain objects of the default type or
	 * objects that inherit from the default.
	 *
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
		// set the class once to avoid overhead in the loop
		$cls = CausaPeer::getOMClass();
		$cls = sfPropel::import($cls);
		// populate the object(s)
		while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	/**
	 * Returns the number of rows matching criteria, joining the related TipoCausa table
	 *
	 * @param      Criteria $c
	 * @param      boolean $distinct Whether to select only distinct columns (You can also set DISTINCT modifier in Criteria).
	 * @param      Connection $con
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinTipoCausa(Criteria $criteria, $distinct = false, $con = null)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// clear out anything that might confuse the ORDER BY clause
		$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(CausaPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(CausaPeer::COUNT);
		}

		// just in case we're grouping: add those columns to the select statement
		foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(CausaPeer::TIPO_CAUSA_ID, TipoCausaPeer::ID);

		$rs = CausaPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
			// no rows returned; we infer that means 0 matches.
			return 0;
		}
	}


	/**
	 * Returns the number of rows matching criteria, joining the related Deudor table
	 *
	 * @param      Criteria $c
	 * @param      boolean $distinct Whether to select only distinct columns (You can also set DISTINCT modifier in Criteria).
	 * @param      Connection $con
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinDeudor(Criteria $criteria, $distinct = false, $con = null)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// clear out anything that might confuse the ORDER BY clause
		$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(CausaPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(CausaPeer::COUNT);
		}

		// just in case we're grouping: add those columns to the select statement
		foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(CausaPeer::DEUDOR_ID, DeudorPeer::ID);

		$rs = CausaPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
			// no rows returned; we infer that means 0 matches.
			return 0;
		}
	}


	/**
	 * Returns the number of rows matching criteria, joining the related Materia table
	 *
	 * @param      Criteria $c
	 * @param      boolean $distinct Whether to select only distinct columns (You can also set DISTINCT modifier in Criteria).
	 * @param      Connection $con
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinMateria(Criteria $criteria, $distinct = false, $con = null)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// clear out anything that might confuse the ORDER BY clause
		$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(CausaPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(CausaPeer::COUNT);
		}

		// just in case we're grouping: add those columns to the select statement
		foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(CausaPeer::MATERIA_ID, MateriaPeer::ID);

		$rs = CausaPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
			// no rows returned; we infer that means 0 matches.
			return 0;
		}
	}


	/**
	 * Returns the number of rows matching criteria, joining the related Contrato table
	 *
	 * @param      Criteria $c
	 * @param      boolean $distinct Whether to select only distinct columns (You can also set DISTINCT modifier in Criteria).
	 * @param      Connection $con
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinContrato(Criteria $criteria, $distinct = false, $con = null)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// clear out anything that might confuse the ORDER BY clause
		$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(CausaPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(CausaPeer::COUNT);
		}

		// just in case we're grouping: add those columns to the select statement
		foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(CausaPeer::CONTRATO_ID, ContratoPeer::ID);

		$rs = CausaPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
			// no rows returned; we infer that means 0 matches.
			return 0;
		}
	}


	/**
	 * Returns the number of rows matching criteria, joining the related Competencia table
	 *
	 * @param      Criteria $c
	 * @param      boolean $distinct Whether to select only distinct columns (You can also set DISTINCT modifier in Criteria).
	 * @param      Connection $con
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinCompetencia(Criteria $criteria, $distinct = false, $con = null)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// clear out anything that might confuse the ORDER BY clause
		$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(CausaPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(CausaPeer::COUNT);
		}

		// just in case we're grouping: add those columns to the select statement
		foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(CausaPeer::COMPETENCIA_ID, CompetenciaPeer::ID);

		$rs = CausaPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
			// no rows returned; we infer that means 0 matches.
			return 0;
		}
	}


	/**
	 * Selects a collection of Causa objects pre-filled with their TipoCausa objects.
	 *
	 * @return     array Array of Causa objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinTipoCausa(Criteria $c, $con = null)
	{
		$c = clone $c;

		// Set the correct dbName if it has not been overridden
		if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		CausaPeer::addSelectColumns($c);
		$startcol = (CausaPeer::NUM_COLUMNS - CausaPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		TipoCausaPeer::addSelectColumns($c);

		$c->addJoin(CausaPeer::TIPO_CAUSA_ID, TipoCausaPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = CausaPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = TipoCausaPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getTipoCausa(); //CHECKME
				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					// e.g. $author->addBookRelatedByBookId()
					$temp_obj2->addCausa($obj1); //CHECKME
					break;
				}
			}
			if ($newObject) {
				$obj2->initCausas();
				$obj2->addCausa($obj1); //CHECKME
			}
			$results[] = $obj1;
		}
		return $results;
	}


	/**
	 * Selects a collection of Causa objects pre-filled with their Deudor objects.
	 *
	 * @return     array Array of Causa objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinDeudor(Criteria $c, $con = null)
	{
		$c = clone $c;

		// Set the correct dbName if it has not been overridden
		if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		CausaPeer::addSelectColumns($c);
		$startcol = (CausaPeer::NUM_COLUMNS - CausaPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		DeudorPeer::addSelectColumns($c);

		$c->addJoin(CausaPeer::DEUDOR_ID, DeudorPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = CausaPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = DeudorPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getDeudor(); //CHECKME
				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					// e.g. $author->addBookRelatedByBookId()
					$temp_obj2->addCausa($obj1); //CHECKME
					break;
				}
			}
			if ($newObject) {
				$obj2->initCausas();
				$obj2->addCausa($obj1); //CHECKME
			}
			$results[] = $obj1;
		}
		return $results;
	}


	/**
	 * Selects a collection of Causa objects pre-filled with their Materia objects.
	 *
	 * @return     array Array of Causa objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinMateria(Criteria $c, $con = null)
	{
		$c = clone $c;

		// Set the correct dbName if it has not been overridden
		if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		CausaPeer::addSelectColumns($c);
		$startcol = (CausaPeer::NUM_COLUMNS - CausaPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		MateriaPeer::addSelectColumns($c);

		$c->addJoin(CausaPeer::MATERIA_ID, MateriaPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = CausaPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = MateriaPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getMateria(); //CHECKME
				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					// e.g. $author->addBookRelatedByBookId()
					$temp_obj2->addCausa($obj1); //CHECKME
					break;
				}
			}
			if ($newObject) {
				$obj2->initCausas();
				$obj2->addCausa($obj1); //CHECKME
			}
			$results[] = $obj1;
		}
		return $results;
	}


	/**
	 * Selects a collection of Causa objects pre-filled with their Contrato objects.
	 *
	 * @return     array Array of Causa objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinContrato(Criteria $c, $con = null)
	{
		$c = clone $c;

		// Set the correct dbName if it has not been overridden
		if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		CausaPeer::addSelectColumns($c);
		$startcol = (CausaPeer::NUM_COLUMNS - CausaPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		ContratoPeer::addSelectColumns($c);

		$c->addJoin(CausaPeer::CONTRATO_ID, ContratoPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = CausaPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = ContratoPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getContrato(); //CHECKME
				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					// e.g. $author->addBookRelatedByBookId()
					$temp_obj2->addCausa($obj1); //CHECKME
					break;
				}
			}
			if ($newObject) {
				$obj2->initCausas();
				$obj2->addCausa($obj1); //CHECKME
			}
			$results[] = $obj1;
		}
		return $results;
	}


	/**
	 * Selects a collection of Causa objects pre-filled with their Competencia objects.
	 *
	 * @return     array Array of Causa objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinCompetencia(Criteria $c, $con = null)
	{
		$c = clone $c;

		// Set the correct dbName if it has not been overridden
		if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		CausaPeer::addSelectColumns($c);
		$startcol = (CausaPeer::NUM_COLUMNS - CausaPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		CompetenciaPeer::addSelectColumns($c);

		$c->addJoin(CausaPeer::COMPETENCIA_ID, CompetenciaPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = CausaPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = CompetenciaPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getCompetencia(); //CHECKME
				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					// e.g. $author->addBookRelatedByBookId()
					$temp_obj2->addCausa($obj1); //CHECKME
					break;
				}
			}
			if ($newObject) {
				$obj2->initCausas();
				$obj2->addCausa($obj1); //CHECKME
			}
			$results[] = $obj1;
		}
		return $results;
	}


	/**
	 * Returns the number of rows matching criteria, joining all related tables
	 *
	 * @param      Criteria $c
	 * @param      boolean $distinct Whether to select only distinct columns (You can also set DISTINCT modifier in Criteria).
	 * @param      Connection $con
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

		// clear out anything that might confuse the ORDER BY clause
		$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(CausaPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(CausaPeer::COUNT);
		}

		// just in case we're grouping: add those columns to the select statement
		foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(CausaPeer::TIPO_CAUSA_ID, TipoCausaPeer::ID);

		$criteria->addJoin(CausaPeer::DEUDOR_ID, DeudorPeer::ID);

		$criteria->addJoin(CausaPeer::MATERIA_ID, MateriaPeer::ID);

		$criteria->addJoin(CausaPeer::CONTRATO_ID, ContratoPeer::ID);

		$criteria->addJoin(CausaPeer::COMPETENCIA_ID, CompetenciaPeer::ID);

		$rs = CausaPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
			// no rows returned; we infer that means 0 matches.
			return 0;
		}
	}


	/**
	 * Selects a collection of Causa objects pre-filled with all related objects.
	 *
	 * @return     array Array of Causa objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

		// Set the correct dbName if it has not been overridden
		if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		CausaPeer::addSelectColumns($c);
		$startcol2 = (CausaPeer::NUM_COLUMNS - CausaPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		TipoCausaPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + TipoCausaPeer::NUM_COLUMNS;

		DeudorPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + DeudorPeer::NUM_COLUMNS;

		MateriaPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + MateriaPeer::NUM_COLUMNS;

		ContratoPeer::addSelectColumns($c);
		$startcol6 = $startcol5 + ContratoPeer::NUM_COLUMNS;

		CompetenciaPeer::addSelectColumns($c);
		$startcol7 = $startcol6 + CompetenciaPeer::NUM_COLUMNS;

		$c->addJoin(CausaPeer::TIPO_CAUSA_ID, TipoCausaPeer::ID);

		$c->addJoin(CausaPeer::DEUDOR_ID, DeudorPeer::ID);

		$c->addJoin(CausaPeer::MATERIA_ID, MateriaPeer::ID);

		$c->addJoin(CausaPeer::CONTRATO_ID, ContratoPeer::ID);

		$c->addJoin(CausaPeer::COMPETENCIA_ID, CompetenciaPeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = CausaPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


				// Add objects for joined TipoCausa rows
	
			$omClass = TipoCausaPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getTipoCausa(); // CHECKME
				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addCausa($obj1); // CHECKME
					break;
				}
			}

			if ($newObject) {
				$obj2->initCausas();
				$obj2->addCausa($obj1);
			}


				// Add objects for joined Deudor rows
	
			$omClass = DeudorPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj3 = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getDeudor(); // CHECKME
				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addCausa($obj1); // CHECKME
					break;
				}
			}

			if ($newObject) {
				$obj3->initCausas();
				$obj3->addCausa($obj1);
			}


				// Add objects for joined Materia rows
	
			$omClass = MateriaPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj4 = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getMateria(); // CHECKME
				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addCausa($obj1); // CHECKME
					break;
				}
			}

			if ($newObject) {
				$obj4->initCausas();
				$obj4->addCausa($obj1);
			}


				// Add objects for joined Contrato rows
	
			$omClass = ContratoPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj5 = new $cls();
			$obj5->hydrate($rs, $startcol5);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj5 = $temp_obj1->getContrato(); // CHECKME
				if ($temp_obj5->getPrimaryKey() === $obj5->getPrimaryKey()) {
					$newObject = false;
					$temp_obj5->addCausa($obj1); // CHECKME
					break;
				}
			}

			if ($newObject) {
				$obj5->initCausas();
				$obj5->addCausa($obj1);
			}


				// Add objects for joined Competencia rows
	
			$omClass = CompetenciaPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj6 = new $cls();
			$obj6->hydrate($rs, $startcol6);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj6 = $temp_obj1->getCompetencia(); // CHECKME
				if ($temp_obj6->getPrimaryKey() === $obj6->getPrimaryKey()) {
					$newObject = false;
					$temp_obj6->addCausa($obj1); // CHECKME
					break;
				}
			}

			if ($newObject) {
				$obj6->initCausas();
				$obj6->addCausa($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	/**
	 * Returns the number of rows matching criteria, joining the related TipoCausa table
	 *
	 * @param      Criteria $c
	 * @param      boolean $distinct Whether to select only distinct columns (You can also set DISTINCT modifier in Criteria).
	 * @param      Connection $con
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinAllExceptTipoCausa(Criteria $criteria, $distinct = false, $con = null)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// clear out anything that might confuse the ORDER BY clause
		$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(CausaPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(CausaPeer::COUNT);
		}

		// just in case we're grouping: add those columns to the select statement
		foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(CausaPeer::DEUDOR_ID, DeudorPeer::ID);

		$criteria->addJoin(CausaPeer::MATERIA_ID, MateriaPeer::ID);

		$criteria->addJoin(CausaPeer::CONTRATO_ID, ContratoPeer::ID);

		$criteria->addJoin(CausaPeer::COMPETENCIA_ID, CompetenciaPeer::ID);

		$rs = CausaPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
			// no rows returned; we infer that means 0 matches.
			return 0;
		}
	}


	/**
	 * Returns the number of rows matching criteria, joining the related Deudor table
	 *
	 * @param      Criteria $c
	 * @param      boolean $distinct Whether to select only distinct columns (You can also set DISTINCT modifier in Criteria).
	 * @param      Connection $con
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinAllExceptDeudor(Criteria $criteria, $distinct = false, $con = null)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// clear out anything that might confuse the ORDER BY clause
		$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(CausaPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(CausaPeer::COUNT);
		}

		// just in case we're grouping: add those columns to the select statement
		foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(CausaPeer::TIPO_CAUSA_ID, TipoCausaPeer::ID);

		$criteria->addJoin(CausaPeer::MATERIA_ID, MateriaPeer::ID);

		$criteria->addJoin(CausaPeer::CONTRATO_ID, ContratoPeer::ID);

		$criteria->addJoin(CausaPeer::COMPETENCIA_ID, CompetenciaPeer::ID);

		$rs = CausaPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
			// no rows returned; we infer that means 0 matches.
			return 0;
		}
	}


	/**
	 * Returns the number of rows matching criteria, joining the related Materia table
	 *
	 * @param      Criteria $c
	 * @param      boolean $distinct Whether to select only distinct columns (You can also set DISTINCT modifier in Criteria).
	 * @param      Connection $con
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinAllExceptMateria(Criteria $criteria, $distinct = false, $con = null)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// clear out anything that might confuse the ORDER BY clause
		$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(CausaPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(CausaPeer::COUNT);
		}

		// just in case we're grouping: add those columns to the select statement
		foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(CausaPeer::TIPO_CAUSA_ID, TipoCausaPeer::ID);

		$criteria->addJoin(CausaPeer::DEUDOR_ID, DeudorPeer::ID);

		$criteria->addJoin(CausaPeer::CONTRATO_ID, ContratoPeer::ID);

		$criteria->addJoin(CausaPeer::COMPETENCIA_ID, CompetenciaPeer::ID);

		$rs = CausaPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
			// no rows returned; we infer that means 0 matches.
			return 0;
		}
	}


	/**
	 * Returns the number of rows matching criteria, joining the related Contrato table
	 *
	 * @param      Criteria $c
	 * @param      boolean $distinct Whether to select only distinct columns (You can also set DISTINCT modifier in Criteria).
	 * @param      Connection $con
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinAllExceptContrato(Criteria $criteria, $distinct = false, $con = null)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// clear out anything that might confuse the ORDER BY clause
		$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(CausaPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(CausaPeer::COUNT);
		}

		// just in case we're grouping: add those columns to the select statement
		foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(CausaPeer::TIPO_CAUSA_ID, TipoCausaPeer::ID);

		$criteria->addJoin(CausaPeer::DEUDOR_ID, DeudorPeer::ID);

		$criteria->addJoin(CausaPeer::MATERIA_ID, MateriaPeer::ID);

		$criteria->addJoin(CausaPeer::COMPETENCIA_ID, CompetenciaPeer::ID);

		$rs = CausaPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
			// no rows returned; we infer that means 0 matches.
			return 0;
		}
	}


	/**
	 * Returns the number of rows matching criteria, joining the related Competencia table
	 *
	 * @param      Criteria $c
	 * @param      boolean $distinct Whether to select only distinct columns (You can also set DISTINCT modifier in Criteria).
	 * @param      Connection $con
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinAllExceptCompetencia(Criteria $criteria, $distinct = false, $con = null)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// clear out anything that might confuse the ORDER BY clause
		$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(CausaPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(CausaPeer::COUNT);
		}

		// just in case we're grouping: add those columns to the select statement
		foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(CausaPeer::TIPO_CAUSA_ID, TipoCausaPeer::ID);

		$criteria->addJoin(CausaPeer::DEUDOR_ID, DeudorPeer::ID);

		$criteria->addJoin(CausaPeer::MATERIA_ID, MateriaPeer::ID);

		$criteria->addJoin(CausaPeer::CONTRATO_ID, ContratoPeer::ID);

		$rs = CausaPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
			// no rows returned; we infer that means 0 matches.
			return 0;
		}
	}


	/**
	 * Selects a collection of Causa objects pre-filled with all related objects except TipoCausa.
	 *
	 * @return     array Array of Causa objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinAllExceptTipoCausa(Criteria $c, $con = null)
	{
		$c = clone $c;

		// Set the correct dbName if it has not been overridden
		// $c->getDbName() will return the same object if not set to another value
		// so == check is okay and faster
		if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		CausaPeer::addSelectColumns($c);
		$startcol2 = (CausaPeer::NUM_COLUMNS - CausaPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		DeudorPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + DeudorPeer::NUM_COLUMNS;

		MateriaPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + MateriaPeer::NUM_COLUMNS;

		ContratoPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + ContratoPeer::NUM_COLUMNS;

		CompetenciaPeer::addSelectColumns($c);
		$startcol6 = $startcol5 + CompetenciaPeer::NUM_COLUMNS;

		$c->addJoin(CausaPeer::DEUDOR_ID, DeudorPeer::ID);

		$c->addJoin(CausaPeer::MATERIA_ID, MateriaPeer::ID);

		$c->addJoin(CausaPeer::CONTRATO_ID, ContratoPeer::ID);

		$c->addJoin(CausaPeer::COMPETENCIA_ID, CompetenciaPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = CausaPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = DeudorPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getDeudor(); //CHECKME
				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addCausa($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initCausas();
				$obj2->addCausa($obj1);
			}

			$omClass = MateriaPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getMateria(); //CHECKME
				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addCausa($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initCausas();
				$obj3->addCausa($obj1);
			}

			$omClass = ContratoPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getContrato(); //CHECKME
				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addCausa($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initCausas();
				$obj4->addCausa($obj1);
			}

			$omClass = CompetenciaPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj5  = new $cls();
			$obj5->hydrate($rs, $startcol5);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj5 = $temp_obj1->getCompetencia(); //CHECKME
				if ($temp_obj5->getPrimaryKey() === $obj5->getPrimaryKey()) {
					$newObject = false;
					$temp_obj5->addCausa($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj5->initCausas();
				$obj5->addCausa($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	/**
	 * Selects a collection of Causa objects pre-filled with all related objects except Deudor.
	 *
	 * @return     array Array of Causa objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinAllExceptDeudor(Criteria $c, $con = null)
	{
		$c = clone $c;

		// Set the correct dbName if it has not been overridden
		// $c->getDbName() will return the same object if not set to another value
		// so == check is okay and faster
		if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		CausaPeer::addSelectColumns($c);
		$startcol2 = (CausaPeer::NUM_COLUMNS - CausaPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		TipoCausaPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + TipoCausaPeer::NUM_COLUMNS;

		MateriaPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + MateriaPeer::NUM_COLUMNS;

		ContratoPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + ContratoPeer::NUM_COLUMNS;

		CompetenciaPeer::addSelectColumns($c);
		$startcol6 = $startcol5 + CompetenciaPeer::NUM_COLUMNS;

		$c->addJoin(CausaPeer::TIPO_CAUSA_ID, TipoCausaPeer::ID);

		$c->addJoin(CausaPeer::MATERIA_ID, MateriaPeer::ID);

		$c->addJoin(CausaPeer::CONTRATO_ID, ContratoPeer::ID);

		$c->addJoin(CausaPeer::COMPETENCIA_ID, CompetenciaPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = CausaPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = TipoCausaPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getTipoCausa(); //CHECKME
				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addCausa($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initCausas();
				$obj2->addCausa($obj1);
			}

			$omClass = MateriaPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getMateria(); //CHECKME
				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addCausa($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initCausas();
				$obj3->addCausa($obj1);
			}

			$omClass = ContratoPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getContrato(); //CHECKME
				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addCausa($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initCausas();
				$obj4->addCausa($obj1);
			}

			$omClass = CompetenciaPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj5  = new $cls();
			$obj5->hydrate($rs, $startcol5);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj5 = $temp_obj1->getCompetencia(); //CHECKME
				if ($temp_obj5->getPrimaryKey() === $obj5->getPrimaryKey()) {
					$newObject = false;
					$temp_obj5->addCausa($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj5->initCausas();
				$obj5->addCausa($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	/**
	 * Selects a collection of Causa objects pre-filled with all related objects except Materia.
	 *
	 * @return     array Array of Causa objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinAllExceptMateria(Criteria $c, $con = null)
	{
		$c = clone $c;

		// Set the correct dbName if it has not been overridden
		// $c->getDbName() will return the same object if not set to another value
		// so == check is okay and faster
		if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		CausaPeer::addSelectColumns($c);
		$startcol2 = (CausaPeer::NUM_COLUMNS - CausaPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		TipoCausaPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + TipoCausaPeer::NUM_COLUMNS;

		DeudorPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + DeudorPeer::NUM_COLUMNS;

		ContratoPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + ContratoPeer::NUM_COLUMNS;

		CompetenciaPeer::addSelectColumns($c);
		$startcol6 = $startcol5 + CompetenciaPeer::NUM_COLUMNS;

		$c->addJoin(CausaPeer::TIPO_CAUSA_ID, TipoCausaPeer::ID);

		$c->addJoin(CausaPeer::DEUDOR_ID, DeudorPeer::ID);

		$c->addJoin(CausaPeer::CONTRATO_ID, ContratoPeer::ID);

		$c->addJoin(CausaPeer::COMPETENCIA_ID, CompetenciaPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = CausaPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = TipoCausaPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getTipoCausa(); //CHECKME
				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addCausa($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initCausas();
				$obj2->addCausa($obj1);
			}

			$omClass = DeudorPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getDeudor(); //CHECKME
				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addCausa($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initCausas();
				$obj3->addCausa($obj1);
			}

			$omClass = ContratoPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getContrato(); //CHECKME
				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addCausa($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initCausas();
				$obj4->addCausa($obj1);
			}

			$omClass = CompetenciaPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj5  = new $cls();
			$obj5->hydrate($rs, $startcol5);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj5 = $temp_obj1->getCompetencia(); //CHECKME
				if ($temp_obj5->getPrimaryKey() === $obj5->getPrimaryKey()) {
					$newObject = false;
					$temp_obj5->addCausa($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj5->initCausas();
				$obj5->addCausa($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	/**
	 * Selects a collection of Causa objects pre-filled with all related objects except Contrato.
	 *
	 * @return     array Array of Causa objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinAllExceptContrato(Criteria $c, $con = null)
	{
		$c = clone $c;

		// Set the correct dbName if it has not been overridden
		// $c->getDbName() will return the same object if not set to another value
		// so == check is okay and faster
		if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		CausaPeer::addSelectColumns($c);
		$startcol2 = (CausaPeer::NUM_COLUMNS - CausaPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		TipoCausaPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + TipoCausaPeer::NUM_COLUMNS;

		DeudorPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + DeudorPeer::NUM_COLUMNS;

		MateriaPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + MateriaPeer::NUM_COLUMNS;

		CompetenciaPeer::addSelectColumns($c);
		$startcol6 = $startcol5 + CompetenciaPeer::NUM_COLUMNS;

		$c->addJoin(CausaPeer::TIPO_CAUSA_ID, TipoCausaPeer::ID);

		$c->addJoin(CausaPeer::DEUDOR_ID, DeudorPeer::ID);

		$c->addJoin(CausaPeer::MATERIA_ID, MateriaPeer::ID);

		$c->addJoin(CausaPeer::COMPETENCIA_ID, CompetenciaPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = CausaPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = TipoCausaPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getTipoCausa(); //CHECKME
				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addCausa($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initCausas();
				$obj2->addCausa($obj1);
			}

			$omClass = DeudorPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getDeudor(); //CHECKME
				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addCausa($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initCausas();
				$obj3->addCausa($obj1);
			}

			$omClass = MateriaPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getMateria(); //CHECKME
				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addCausa($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initCausas();
				$obj4->addCausa($obj1);
			}

			$omClass = CompetenciaPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj5  = new $cls();
			$obj5->hydrate($rs, $startcol5);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj5 = $temp_obj1->getCompetencia(); //CHECKME
				if ($temp_obj5->getPrimaryKey() === $obj5->getPrimaryKey()) {
					$newObject = false;
					$temp_obj5->addCausa($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj5->initCausas();
				$obj5->addCausa($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	/**
	 * Selects a collection of Causa objects pre-filled with all related objects except Competencia.
	 *
	 * @return     array Array of Causa objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinAllExceptCompetencia(Criteria $c, $con = null)
	{
		$c = clone $c;

		// Set the correct dbName if it has not been overridden
		// $c->getDbName() will return the same object if not set to another value
		// so == check is okay and faster
		if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		CausaPeer::addSelectColumns($c);
		$startcol2 = (CausaPeer::NUM_COLUMNS - CausaPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		TipoCausaPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + TipoCausaPeer::NUM_COLUMNS;

		DeudorPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + DeudorPeer::NUM_COLUMNS;

		MateriaPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + MateriaPeer::NUM_COLUMNS;

		ContratoPeer::addSelectColumns($c);
		$startcol6 = $startcol5 + ContratoPeer::NUM_COLUMNS;

		$c->addJoin(CausaPeer::TIPO_CAUSA_ID, TipoCausaPeer::ID);

		$c->addJoin(CausaPeer::DEUDOR_ID, DeudorPeer::ID);

		$c->addJoin(CausaPeer::MATERIA_ID, MateriaPeer::ID);

		$c->addJoin(CausaPeer::CONTRATO_ID, ContratoPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = CausaPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = TipoCausaPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getTipoCausa(); //CHECKME
				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addCausa($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initCausas();
				$obj2->addCausa($obj1);
			}

			$omClass = DeudorPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getDeudor(); //CHECKME
				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addCausa($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initCausas();
				$obj3->addCausa($obj1);
			}

			$omClass = MateriaPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getMateria(); //CHECKME
				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addCausa($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initCausas();
				$obj4->addCausa($obj1);
			}

			$omClass = ContratoPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj5  = new $cls();
			$obj5->hydrate($rs, $startcol5);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj5 = $temp_obj1->getContrato(); //CHECKME
				if ($temp_obj5->getPrimaryKey() === $obj5->getPrimaryKey()) {
					$newObject = false;
					$temp_obj5->addCausa($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj5->initCausas();
				$obj5->addCausa($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


  static public function getUniqueColumnNames()
  {
    return array();
  }
	/**
	 * Returns the TableMap related to this peer.
	 * This method is not needed for general use but a specific application could have a need.
	 * @return     TableMap
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	/**
	 * The class that the Peer will make instances of.
	 *
	 * This uses a dot-path notation which is tranalted into a path
	 * relative to a location on the PHP include_path.
	 * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
	 *
	 * @return     string path.to.ClassName
	 */
	public static function getOMClass()
	{
		return CausaPeer::CLASS_DEFAULT;
	}

	/**
	 * Method perform an INSERT on the database, given a Causa or Criteria object.
	 *
	 * @param      mixed $values Criteria or Causa object containing data that is used to create the INSERT statement.
	 * @param      Connection $con the connection to use
	 * @return     mixed The new primary key.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; // rename for clarity
		} else {
			$criteria = $values->buildCriteria(); // build Criteria from Causa object
		}

		$criteria->remove(CausaPeer::ID); // remove pkey col since this table uses auto-increment


		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		try {
			// use transaction because $criteria could contain info
			// for more than one table (I guess, conceivably)
			$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	/**
	 * Method perform an UPDATE on the database, given a Causa or Criteria object.
	 *
	 * @param      mixed $values Criteria or Causa object containing data that is used to create the UPDATE statement.
	 * @param      Connection $con The connection to use (specify Connection object to exert more control over transactions).
	 * @return     int The number of affected rows (if supported by underlying database driver).
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; // rename for clarity

			$comparison = $criteria->getComparison(CausaPeer::ID);
			$selectCriteria->add(CausaPeer::ID, $criteria->remove(CausaPeer::ID), $comparison);

		} else { // $values is Causa object
			$criteria = $values->buildCriteria(); // gets full criteria
			$selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
		}

		// set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	/**
	 * Method to DELETE all rows from the causa table.
	 *
	 * @return     int The number of affected rows (if supported by underlying database driver).
	 */
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; // initialize var to track total num of affected rows
		try {
			// use transaction because $criteria could contain info
			// for more than one table or we could emulating ON DELETE CASCADE, etc.
			$con->begin();
			$affectedRows += BasePeer::doDeleteAll(CausaPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Method perform a DELETE on the database, given a Causa or Criteria object OR a primary key value.
	 *
	 * @param      mixed $values Criteria or Causa object or primary key or array of primary keys
	 *              which is used to create the DELETE statement
	 * @param      Connection $con the connection to use
	 * @return     int 	The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
	 *				if supported by native driver or if emulated using Propel.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(CausaPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; // rename for clarity
		} elseif ($values instanceof Causa) {

			$criteria = $values->buildPkeyCriteria();
		} else {
			// it must be the primary key
			$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(CausaPeer::ID, (array) $values, Criteria::IN);
		}

		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; // initialize var to track total num of affected rows

		try {
			// use transaction because $criteria could contain info
			// for more than one table or we could emulating ON DELETE CASCADE, etc.
			$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Validates all modified columns of given Causa object.
	 * If parameter $columns is either a single column name or an array of column names
	 * than only those columns are validated.
	 *
	 * NOTICE: This does not apply to primary or foreign keys for now.
	 *
	 * @param      Causa $obj The object to validate.
	 * @param      mixed $cols Column name or array of column names.
	 *
	 * @return     mixed TRUE if all columns are valid or the error message of the first invalid column.
	 */
	public static function doValidate(Causa $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(CausaPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(CausaPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(CausaPeer::DATABASE_NAME, CausaPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = CausaPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	/**
	 * Retrieve a single object by pkey.
	 *
	 * @param      mixed $pk the primary key.
	 * @param      Connection $con the connection to use
	 * @return     Causa
	 */
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(CausaPeer::DATABASE_NAME);

		$criteria->add(CausaPeer::ID, $pk);


		$v = CausaPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	/**
	 * Retrieve multiple objects by pkey.
	 *
	 * @param      array $pks List of primary keys
	 * @param      Connection $con the connection to use
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(CausaPeer::ID, $pks, Criteria::IN);
			$objs = CausaPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} // BaseCausaPeer

// static code to register the map builder for this Peer with the main Propel class
if (Propel::isInit()) {
	// the MapBuilder classes register themselves with Propel during initialization
	// so we need to load them here.
	try {
		BaseCausaPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
	// even if Propel is not yet initialized, the map builder class can be registered
	// now and then it will be loaded when Propel initializes.
	Propel::registerMapBuilder('lib.model.map.CausaMapBuilder');
}
