<?php

/**
 * Base class that represents a row from the 'comuna' table.
 *
 * 
 *
 * @package    lib.model.om
 */
abstract class BaseComuna extends BaseObject  implements Persistent {


	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        ComunaPeer
	 */
	protected static $peer;


	/**
	 * The value for the id field.
	 * @var        int
	 */
	protected $id;


	/**
	 * The value for the ciudad_id field.
	 * @var        int
	 */
	protected $ciudad_id;


	/**
	 * The value for the nombre field.
	 * @var        string
	 */
	protected $nombre;

	/**
	 * @var        Ciudad
	 */
	protected $aCiudad;

	/**
	 * Collection to store aggregation of collAcreedors.
	 * @var        array
	 */
	protected $collAcreedors;

	/**
	 * The criteria used to select the current contents of collAcreedors.
	 * @var        Criteria
	 */
	protected $lastAcreedorCriteria = null;

	/**
	 * Collection to store aggregation of collDeudorDomicilios.
	 * @var        array
	 */
	protected $collDeudorDomicilios;

	/**
	 * The criteria used to select the current contents of collDeudorDomicilios.
	 * @var        Criteria
	 */
	protected $lastDeudorDomicilioCriteria = null;

	/**
	 * Collection to store aggregation of collTribunals.
	 * @var        array
	 */
	protected $collTribunals;

	/**
	 * The criteria used to select the current contents of collTribunals.
	 * @var        Criteria
	 */
	protected $lastTribunalCriteria = null;

	/**
	 * Collection to store aggregation of collPerfils.
	 * @var        array
	 */
	protected $collPerfils;

	/**
	 * The criteria used to select the current contents of collPerfils.
	 * @var        Criteria
	 */
	protected $lastPerfilCriteria = null;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Get the [id] column value.
	 * 
	 * @return     int
	 */
	public function getId()
	{

		return $this->id;
	}

	/**
	 * Get the [ciudad_id] column value.
	 * 
	 * @return     int
	 */
	public function getCiudadId()
	{

		return $this->ciudad_id;
	}

	/**
	 * Get the [nombre] column value.
	 * 
	 * @return     string
	 */
	public function getNombre()
	{

		return $this->nombre;
	}

	/**
	 * Set the value of [id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ComunaPeer::ID;
		}

	} // setId()

	/**
	 * Set the value of [ciudad_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setCiudadId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->ciudad_id !== $v) {
			$this->ciudad_id = $v;
			$this->modifiedColumns[] = ComunaPeer::CIUDAD_ID;
		}

		if ($this->aCiudad !== null && $this->aCiudad->getId() !== $v) {
			$this->aCiudad = null;
		}

	} // setCiudadId()

	/**
	 * Set the value of [nombre] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setNombre($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nombre !== $v) {
			$this->nombre = $v;
			$this->modifiedColumns[] = ComunaPeer::NOMBRE;
		}

	} // setNombre()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (1-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      ResultSet $rs The ResultSet class with cursor advanced to desired record pos.
	 * @param      int $startcol 1-based offset column which indicates which restultset column to start with.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->ciudad_id = $rs->getInt($startcol + 1);

			$this->nombre = $rs->getString($startcol + 2);

			$this->resetModified();

			$this->setNew(false);

			// FIXME - using NUM_COLUMNS may be clearer.
			return $startcol + 3; // 3 = ComunaPeer::NUM_COLUMNS - ComunaPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating Comuna object", $e);
		}
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      Connection $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ComunaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ComunaPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.  If the object is new,
	 * it inserts it; otherwise an update is performed.  This method
	 * wraps the doSave() worker method in a transaction.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ComunaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave($con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aCiudad !== null) {
				if ($this->aCiudad->isModified()) {
					$affectedRows += $this->aCiudad->save($con);
				}
				$this->setCiudad($this->aCiudad);
			}


			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ComunaPeer::doInsert($this, $con);
					$affectedRows += 1; // we are assuming that there is only 1 row per doInsert() which
										 // should always be true here (even though technically
										 // BasePeer::doInsert() can insert multiple rows).

					$this->setId($pk);  //[IMV] update autoincrement primary key

					$this->setNew(false);
				} else {
					$affectedRows += ComunaPeer::doUpdate($this, $con);
				}
				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			if ($this->collAcreedors !== null) {
				foreach($this->collAcreedors as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collDeudorDomicilios !== null) {
				foreach($this->collDeudorDomicilios as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collTribunals !== null) {
				foreach($this->collTribunals as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collPerfils !== null) {
				foreach($this->collPerfils as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aCiudad !== null) {
				if (!$this->aCiudad->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aCiudad->getValidationFailures());
				}
			}


			if (($retval = ComunaPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collAcreedors !== null) {
					foreach($this->collAcreedors as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collDeudorDomicilios !== null) {
					foreach($this->collDeudorDomicilios as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collTribunals !== null) {
					foreach($this->collTribunals as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collPerfils !== null) {
					foreach($this->collPerfils as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ComunaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getCiudadId();
				break;
			case 2:
				return $this->getNombre();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param      string $keyType One of the class type constants TYPE_PHPNAME,
	 *                        TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ComunaPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getCiudadId(),
			$keys[2] => $this->getNombre(),
		);
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ComunaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setCiudadId($value);
				break;
			case 2:
				$this->setNombre($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME,
	 * TYPE_NUM. The default key type is the column's phpname (e.g. 'authorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ComunaPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setCiudadId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNombre($arr[$keys[2]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(ComunaPeer::DATABASE_NAME);

		if ($this->isColumnModified(ComunaPeer::ID)) $criteria->add(ComunaPeer::ID, $this->id);
		if ($this->isColumnModified(ComunaPeer::CIUDAD_ID)) $criteria->add(ComunaPeer::CIUDAD_ID, $this->ciudad_id);
		if ($this->isColumnModified(ComunaPeer::NOMBRE)) $criteria->add(ComunaPeer::NOMBRE, $this->nombre);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ComunaPeer::DATABASE_NAME);

		$criteria->add(ComunaPeer::ID, $this->id);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	/**
	 * Generic method to set the primary key (id column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Comuna (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setCiudadId($this->ciudad_id);

		$copyObj->setNombre($this->nombre);


		if ($deepCopy) {
			// important: temporarily setNew(false) because this affects the behavior of
			// the getter/setter methods for fkey referrer objects.
			$copyObj->setNew(false);

			foreach($this->getAcreedors() as $relObj) {
				$copyObj->addAcreedor($relObj->copy($deepCopy));
			}

			foreach($this->getDeudorDomicilios() as $relObj) {
				$copyObj->addDeudorDomicilio($relObj->copy($deepCopy));
			}

			foreach($this->getTribunals() as $relObj) {
				$copyObj->addTribunal($relObj->copy($deepCopy));
			}

			foreach($this->getPerfils() as $relObj) {
				$copyObj->addPerfil($relObj->copy($deepCopy));
			}

		} // if ($deepCopy)


		$copyObj->setNew(true);

		$copyObj->setId(NULL); // this is a pkey column, so set to default value

	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Comuna Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     ComunaPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ComunaPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a Ciudad object.
	 *
	 * @param      Ciudad $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setCiudad($v)
	{


		if ($v === null) {
			$this->setCiudadId(NULL);
		} else {
			$this->setCiudadId($v->getId());
		}


		$this->aCiudad = $v;
	}


	/**
	 * Get the associated Ciudad object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     Ciudad The associated Ciudad object.
	 * @throws     PropelException
	 */
	public function getCiudad($con = null)
	{
		if ($this->aCiudad === null && ($this->ciudad_id !== null)) {
			// include the related Peer class
			$this->aCiudad = CiudadPeer::retrieveByPK($this->ciudad_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = CiudadPeer::retrieveByPK($this->ciudad_id, $con);
			   $obj->addCiudads($this);
			 */
		}
		return $this->aCiudad;
	}

	/**
	 * Temporary storage of collAcreedors to save a possible db hit in
	 * the event objects are add to the collection, but the
	 * complete collection is never requested.
	 * @return     void
	 */
	public function initAcreedors()
	{
		if ($this->collAcreedors === null) {
			$this->collAcreedors = array();
		}
	}

	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Comuna has previously
	 * been saved, it will retrieve related Acreedors from storage.
	 * If this Comuna is new, it will return
	 * an empty collection or the current collection, the criteria
	 * is ignored on a new object.
	 *
	 * @param      Connection $con
	 * @param      Criteria $criteria
	 * @throws     PropelException
	 */
	public function getAcreedors($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collAcreedors === null) {
			if ($this->isNew()) {
			   $this->collAcreedors = array();
			} else {

				$criteria->add(AcreedorPeer::COMUNA_ID, $this->getId());

				AcreedorPeer::addSelectColumns($criteria);
				$this->collAcreedors = AcreedorPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(AcreedorPeer::COMUNA_ID, $this->getId());

				AcreedorPeer::addSelectColumns($criteria);
				if (!isset($this->lastAcreedorCriteria) || !$this->lastAcreedorCriteria->equals($criteria)) {
					$this->collAcreedors = AcreedorPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastAcreedorCriteria = $criteria;
		return $this->collAcreedors;
	}

	/**
	 * Returns the number of related Acreedors.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      Connection $con
	 * @throws     PropelException
	 */
	public function countAcreedors($criteria = null, $distinct = false, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(AcreedorPeer::COMUNA_ID, $this->getId());

		return AcreedorPeer::doCount($criteria, $distinct, $con);
	}

	/**
	 * Method called to associate a Acreedor object to this object
	 * through the Acreedor foreign key attribute
	 *
	 * @param      Acreedor $l Acreedor
	 * @return     void
	 * @throws     PropelException
	 */
	public function addAcreedor(Acreedor $l)
	{
		$this->collAcreedors[] = $l;
		$l->setComuna($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Comuna is new, it will return
	 * an empty collection; or if this Comuna has previously
	 * been saved, it will retrieve related Acreedors from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Comuna.
	 */
	public function getAcreedorsJoinGiro($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collAcreedors === null) {
			if ($this->isNew()) {
				$this->collAcreedors = array();
			} else {

				$criteria->add(AcreedorPeer::COMUNA_ID, $this->getId());

				$this->collAcreedors = AcreedorPeer::doSelectJoinGiro($criteria, $con);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(AcreedorPeer::COMUNA_ID, $this->getId());

			if (!isset($this->lastAcreedorCriteria) || !$this->lastAcreedorCriteria->equals($criteria)) {
				$this->collAcreedors = AcreedorPeer::doSelectJoinGiro($criteria, $con);
			}
		}
		$this->lastAcreedorCriteria = $criteria;

		return $this->collAcreedors;
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Comuna is new, it will return
	 * an empty collection; or if this Comuna has previously
	 * been saved, it will retrieve related Acreedors from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Comuna.
	 */
	public function getAcreedorsJoinsfGuardUser($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collAcreedors === null) {
			if ($this->isNew()) {
				$this->collAcreedors = array();
			} else {

				$criteria->add(AcreedorPeer::COMUNA_ID, $this->getId());

				$this->collAcreedors = AcreedorPeer::doSelectJoinsfGuardUser($criteria, $con);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(AcreedorPeer::COMUNA_ID, $this->getId());

			if (!isset($this->lastAcreedorCriteria) || !$this->lastAcreedorCriteria->equals($criteria)) {
				$this->collAcreedors = AcreedorPeer::doSelectJoinsfGuardUser($criteria, $con);
			}
		}
		$this->lastAcreedorCriteria = $criteria;

		return $this->collAcreedors;
	}

	/**
	 * Temporary storage of collDeudorDomicilios to save a possible db hit in
	 * the event objects are add to the collection, but the
	 * complete collection is never requested.
	 * @return     void
	 */
	public function initDeudorDomicilios()
	{
		if ($this->collDeudorDomicilios === null) {
			$this->collDeudorDomicilios = array();
		}
	}

	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Comuna has previously
	 * been saved, it will retrieve related DeudorDomicilios from storage.
	 * If this Comuna is new, it will return
	 * an empty collection or the current collection, the criteria
	 * is ignored on a new object.
	 *
	 * @param      Connection $con
	 * @param      Criteria $criteria
	 * @throws     PropelException
	 */
	public function getDeudorDomicilios($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collDeudorDomicilios === null) {
			if ($this->isNew()) {
			   $this->collDeudorDomicilios = array();
			} else {

				$criteria->add(DeudorDomicilioPeer::COMUNA_ID, $this->getId());

				DeudorDomicilioPeer::addSelectColumns($criteria);
				$this->collDeudorDomicilios = DeudorDomicilioPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(DeudorDomicilioPeer::COMUNA_ID, $this->getId());

				DeudorDomicilioPeer::addSelectColumns($criteria);
				if (!isset($this->lastDeudorDomicilioCriteria) || !$this->lastDeudorDomicilioCriteria->equals($criteria)) {
					$this->collDeudorDomicilios = DeudorDomicilioPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastDeudorDomicilioCriteria = $criteria;
		return $this->collDeudorDomicilios;
	}

	/**
	 * Returns the number of related DeudorDomicilios.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      Connection $con
	 * @throws     PropelException
	 */
	public function countDeudorDomicilios($criteria = null, $distinct = false, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(DeudorDomicilioPeer::COMUNA_ID, $this->getId());

		return DeudorDomicilioPeer::doCount($criteria, $distinct, $con);
	}

	/**
	 * Method called to associate a DeudorDomicilio object to this object
	 * through the DeudorDomicilio foreign key attribute
	 *
	 * @param      DeudorDomicilio $l DeudorDomicilio
	 * @return     void
	 * @throws     PropelException
	 */
	public function addDeudorDomicilio(DeudorDomicilio $l)
	{
		$this->collDeudorDomicilios[] = $l;
		$l->setComuna($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Comuna is new, it will return
	 * an empty collection; or if this Comuna has previously
	 * been saved, it will retrieve related DeudorDomicilios from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Comuna.
	 */
	public function getDeudorDomiciliosJoinDeudor($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collDeudorDomicilios === null) {
			if ($this->isNew()) {
				$this->collDeudorDomicilios = array();
			} else {

				$criteria->add(DeudorDomicilioPeer::COMUNA_ID, $this->getId());

				$this->collDeudorDomicilios = DeudorDomicilioPeer::doSelectJoinDeudor($criteria, $con);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(DeudorDomicilioPeer::COMUNA_ID, $this->getId());

			if (!isset($this->lastDeudorDomicilioCriteria) || !$this->lastDeudorDomicilioCriteria->equals($criteria)) {
				$this->collDeudorDomicilios = DeudorDomicilioPeer::doSelectJoinDeudor($criteria, $con);
			}
		}
		$this->lastDeudorDomicilioCriteria = $criteria;

		return $this->collDeudorDomicilios;
	}

	/**
	 * Temporary storage of collTribunals to save a possible db hit in
	 * the event objects are add to the collection, but the
	 * complete collection is never requested.
	 * @return     void
	 */
	public function initTribunals()
	{
		if ($this->collTribunals === null) {
			$this->collTribunals = array();
		}
	}

	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Comuna has previously
	 * been saved, it will retrieve related Tribunals from storage.
	 * If this Comuna is new, it will return
	 * an empty collection or the current collection, the criteria
	 * is ignored on a new object.
	 *
	 * @param      Connection $con
	 * @param      Criteria $criteria
	 * @throws     PropelException
	 */
	public function getTribunals($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTribunals === null) {
			if ($this->isNew()) {
			   $this->collTribunals = array();
			} else {

				$criteria->add(TribunalPeer::COMUNA_ID, $this->getId());

				TribunalPeer::addSelectColumns($criteria);
				$this->collTribunals = TribunalPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(TribunalPeer::COMUNA_ID, $this->getId());

				TribunalPeer::addSelectColumns($criteria);
				if (!isset($this->lastTribunalCriteria) || !$this->lastTribunalCriteria->equals($criteria)) {
					$this->collTribunals = TribunalPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastTribunalCriteria = $criteria;
		return $this->collTribunals;
	}

	/**
	 * Returns the number of related Tribunals.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      Connection $con
	 * @throws     PropelException
	 */
	public function countTribunals($criteria = null, $distinct = false, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(TribunalPeer::COMUNA_ID, $this->getId());

		return TribunalPeer::doCount($criteria, $distinct, $con);
	}

	/**
	 * Method called to associate a Tribunal object to this object
	 * through the Tribunal foreign key attribute
	 *
	 * @param      Tribunal $l Tribunal
	 * @return     void
	 * @throws     PropelException
	 */
	public function addTribunal(Tribunal $l)
	{
		$this->collTribunals[] = $l;
		$l->setComuna($this);
	}

	/**
	 * Temporary storage of collPerfils to save a possible db hit in
	 * the event objects are add to the collection, but the
	 * complete collection is never requested.
	 * @return     void
	 */
	public function initPerfils()
	{
		if ($this->collPerfils === null) {
			$this->collPerfils = array();
		}
	}

	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Comuna has previously
	 * been saved, it will retrieve related Perfils from storage.
	 * If this Comuna is new, it will return
	 * an empty collection or the current collection, the criteria
	 * is ignored on a new object.
	 *
	 * @param      Connection $con
	 * @param      Criteria $criteria
	 * @throws     PropelException
	 */
	public function getPerfils($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collPerfils === null) {
			if ($this->isNew()) {
			   $this->collPerfils = array();
			} else {

				$criteria->add(PerfilPeer::COMUNA_ID, $this->getId());

				PerfilPeer::addSelectColumns($criteria);
				$this->collPerfils = PerfilPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(PerfilPeer::COMUNA_ID, $this->getId());

				PerfilPeer::addSelectColumns($criteria);
				if (!isset($this->lastPerfilCriteria) || !$this->lastPerfilCriteria->equals($criteria)) {
					$this->collPerfils = PerfilPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastPerfilCriteria = $criteria;
		return $this->collPerfils;
	}

	/**
	 * Returns the number of related Perfils.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      Connection $con
	 * @throws     PropelException
	 */
	public function countPerfils($criteria = null, $distinct = false, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(PerfilPeer::COMUNA_ID, $this->getId());

		return PerfilPeer::doCount($criteria, $distinct, $con);
	}

	/**
	 * Method called to associate a Perfil object to this object
	 * through the Perfil foreign key attribute
	 *
	 * @param      Perfil $l Perfil
	 * @return     void
	 * @throws     PropelException
	 */
	public function addPerfil(Perfil $l)
	{
		$this->collPerfils[] = $l;
		$l->setComuna($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Comuna is new, it will return
	 * an empty collection; or if this Comuna has previously
	 * been saved, it will retrieve related Perfils from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Comuna.
	 */
	public function getPerfilsJoinsfGuardUser($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collPerfils === null) {
			if ($this->isNew()) {
				$this->collPerfils = array();
			} else {

				$criteria->add(PerfilPeer::COMUNA_ID, $this->getId());

				$this->collPerfils = PerfilPeer::doSelectJoinsfGuardUser($criteria, $con);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(PerfilPeer::COMUNA_ID, $this->getId());

			if (!isset($this->lastPerfilCriteria) || !$this->lastPerfilCriteria->equals($criteria)) {
				$this->collPerfils = PerfilPeer::doSelectJoinsfGuardUser($criteria, $con);
			}
		}
		$this->lastPerfilCriteria = $criteria;

		return $this->collPerfils;
	}

} // BaseComuna
