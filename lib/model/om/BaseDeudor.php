<?php

/**
 * Base class that represents a row from the 'deudor' table.
 *
 * 
 *
 * @package    lib.model.om
 */
abstract class BaseDeudor extends BaseObject  implements Persistent {


	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        DeudorPeer
	 */
	protected static $peer;


	/**
	 * The value for the id field.
	 * @var        int
	 */
	protected $id;


	/**
	 * The value for the rut_deudor field.
	 * @var        string
	 */
	protected $rut_deudor;


	/**
	 * The value for the nombres field.
	 * @var        string
	 */
	protected $nombres;


	/**
	 * The value for the apellido_paterno field.
	 * @var        string
	 */
	protected $apellido_paterno;


	/**
	 * The value for the apellido_materno field.
	 * @var        string
	 */
	protected $apellido_materno;


	/**
	 * The value for the celular field.
	 * @var        string
	 */
	protected $celular;


	/**
	 * The value for the email field.
	 * @var        string
	 */
	protected $email;

	/**
	 * Collection to store aggregation of collDeudorDomicilios.
	 * @var        array
	 */
	protected $collDeudorDomicilios;

	/**
	 * The criteria used to select the current contents of collDeudorDomicilios.
	 * @var        Criteria
	 */
	protected $lastDeudorDomicilioCriteria = null;

	/**
	 * Collection to store aggregation of collCausas.
	 * @var        array
	 */
	protected $collCausas;

	/**
	 * The criteria used to select the current contents of collCausas.
	 * @var        Criteria
	 */
	protected $lastCausaCriteria = null;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Get the [id] column value.
	 * 
	 * @return     int
	 */
	public function getId()
	{

		return $this->id;
	}

	/**
	 * Get the [rut_deudor] column value.
	 * 
	 * @return     string
	 */
	public function getRutDeudor()
	{

		return $this->rut_deudor;
	}

	/**
	 * Get the [nombres] column value.
	 * 
	 * @return     string
	 */
	public function getNombres()
	{

		return $this->nombres;
	}

	/**
	 * Get the [apellido_paterno] column value.
	 * 
	 * @return     string
	 */
	public function getApellidoPaterno()
	{

		return $this->apellido_paterno;
	}

	/**
	 * Get the [apellido_materno] column value.
	 * 
	 * @return     string
	 */
	public function getApellidoMaterno()
	{

		return $this->apellido_materno;
	}

	/**
	 * Get the [celular] column value.
	 * 
	 * @return     string
	 */
	public function getCelular()
	{

		return $this->celular;
	}

	/**
	 * Get the [email] column value.
	 * 
	 * @return     string
	 */
	public function getEmail()
	{

		return $this->email;
	}

	/**
	 * Set the value of [id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = DeudorPeer::ID;
		}

	} // setId()

	/**
	 * Set the value of [rut_deudor] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setRutDeudor($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rut_deudor !== $v) {
			$this->rut_deudor = $v;
			$this->modifiedColumns[] = DeudorPeer::RUT_DEUDOR;
		}

	} // setRutDeudor()

	/**
	 * Set the value of [nombres] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setNombres($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nombres !== $v) {
			$this->nombres = $v;
			$this->modifiedColumns[] = DeudorPeer::NOMBRES;
		}

	} // setNombres()

	/**
	 * Set the value of [apellido_paterno] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setApellidoPaterno($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->apellido_paterno !== $v) {
			$this->apellido_paterno = $v;
			$this->modifiedColumns[] = DeudorPeer::APELLIDO_PATERNO;
		}

	} // setApellidoPaterno()

	/**
	 * Set the value of [apellido_materno] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setApellidoMaterno($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->apellido_materno !== $v) {
			$this->apellido_materno = $v;
			$this->modifiedColumns[] = DeudorPeer::APELLIDO_MATERNO;
		}

	} // setApellidoMaterno()

	/**
	 * Set the value of [celular] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setCelular($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->celular !== $v) {
			$this->celular = $v;
			$this->modifiedColumns[] = DeudorPeer::CELULAR;
		}

	} // setCelular()

	/**
	 * Set the value of [email] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setEmail($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->email !== $v) {
			$this->email = $v;
			$this->modifiedColumns[] = DeudorPeer::EMAIL;
		}

	} // setEmail()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (1-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      ResultSet $rs The ResultSet class with cursor advanced to desired record pos.
	 * @param      int $startcol 1-based offset column which indicates which restultset column to start with.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->rut_deudor = $rs->getString($startcol + 1);

			$this->nombres = $rs->getString($startcol + 2);

			$this->apellido_paterno = $rs->getString($startcol + 3);

			$this->apellido_materno = $rs->getString($startcol + 4);

			$this->celular = $rs->getString($startcol + 5);

			$this->email = $rs->getString($startcol + 6);

			$this->resetModified();

			$this->setNew(false);

			// FIXME - using NUM_COLUMNS may be clearer.
			return $startcol + 7; // 7 = DeudorPeer::NUM_COLUMNS - DeudorPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating Deudor object", $e);
		}
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      Connection $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(DeudorPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			DeudorPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.  If the object is new,
	 * it inserts it; otherwise an update is performed.  This method
	 * wraps the doSave() worker method in a transaction.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(DeudorPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave($con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = DeudorPeer::doInsert($this, $con);
					$affectedRows += 1; // we are assuming that there is only 1 row per doInsert() which
										 // should always be true here (even though technically
										 // BasePeer::doInsert() can insert multiple rows).

					$this->setId($pk);  //[IMV] update autoincrement primary key

					$this->setNew(false);
				} else {
					$affectedRows += DeudorPeer::doUpdate($this, $con);
				}
				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			if ($this->collDeudorDomicilios !== null) {
				foreach($this->collDeudorDomicilios as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collCausas !== null) {
				foreach($this->collCausas as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = DeudorPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collDeudorDomicilios !== null) {
					foreach($this->collDeudorDomicilios as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collCausas !== null) {
					foreach($this->collCausas as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = DeudorPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getRutDeudor();
				break;
			case 2:
				return $this->getNombres();
				break;
			case 3:
				return $this->getApellidoPaterno();
				break;
			case 4:
				return $this->getApellidoMaterno();
				break;
			case 5:
				return $this->getCelular();
				break;
			case 6:
				return $this->getEmail();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param      string $keyType One of the class type constants TYPE_PHPNAME,
	 *                        TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = DeudorPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getRutDeudor(),
			$keys[2] => $this->getNombres(),
			$keys[3] => $this->getApellidoPaterno(),
			$keys[4] => $this->getApellidoMaterno(),
			$keys[5] => $this->getCelular(),
			$keys[6] => $this->getEmail(),
		);
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = DeudorPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setRutDeudor($value);
				break;
			case 2:
				$this->setNombres($value);
				break;
			case 3:
				$this->setApellidoPaterno($value);
				break;
			case 4:
				$this->setApellidoMaterno($value);
				break;
			case 5:
				$this->setCelular($value);
				break;
			case 6:
				$this->setEmail($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME,
	 * TYPE_NUM. The default key type is the column's phpname (e.g. 'authorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = DeudorPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setRutDeudor($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNombres($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setApellidoPaterno($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setApellidoMaterno($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setCelular($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setEmail($arr[$keys[6]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(DeudorPeer::DATABASE_NAME);

		if ($this->isColumnModified(DeudorPeer::ID)) $criteria->add(DeudorPeer::ID, $this->id);
		if ($this->isColumnModified(DeudorPeer::RUT_DEUDOR)) $criteria->add(DeudorPeer::RUT_DEUDOR, $this->rut_deudor);
		if ($this->isColumnModified(DeudorPeer::NOMBRES)) $criteria->add(DeudorPeer::NOMBRES, $this->nombres);
		if ($this->isColumnModified(DeudorPeer::APELLIDO_PATERNO)) $criteria->add(DeudorPeer::APELLIDO_PATERNO, $this->apellido_paterno);
		if ($this->isColumnModified(DeudorPeer::APELLIDO_MATERNO)) $criteria->add(DeudorPeer::APELLIDO_MATERNO, $this->apellido_materno);
		if ($this->isColumnModified(DeudorPeer::CELULAR)) $criteria->add(DeudorPeer::CELULAR, $this->celular);
		if ($this->isColumnModified(DeudorPeer::EMAIL)) $criteria->add(DeudorPeer::EMAIL, $this->email);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(DeudorPeer::DATABASE_NAME);

		$criteria->add(DeudorPeer::ID, $this->id);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	/**
	 * Generic method to set the primary key (id column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Deudor (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setRutDeudor($this->rut_deudor);

		$copyObj->setNombres($this->nombres);

		$copyObj->setApellidoPaterno($this->apellido_paterno);

		$copyObj->setApellidoMaterno($this->apellido_materno);

		$copyObj->setCelular($this->celular);

		$copyObj->setEmail($this->email);


		if ($deepCopy) {
			// important: temporarily setNew(false) because this affects the behavior of
			// the getter/setter methods for fkey referrer objects.
			$copyObj->setNew(false);

			foreach($this->getDeudorDomicilios() as $relObj) {
				$copyObj->addDeudorDomicilio($relObj->copy($deepCopy));
			}

			foreach($this->getCausas() as $relObj) {
				$copyObj->addCausa($relObj->copy($deepCopy));
			}

		} // if ($deepCopy)


		$copyObj->setNew(true);

		$copyObj->setId(NULL); // this is a pkey column, so set to default value

	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Deudor Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     DeudorPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new DeudorPeer();
		}
		return self::$peer;
	}

	/**
	 * Temporary storage of collDeudorDomicilios to save a possible db hit in
	 * the event objects are add to the collection, but the
	 * complete collection is never requested.
	 * @return     void
	 */
	public function initDeudorDomicilios()
	{
		if ($this->collDeudorDomicilios === null) {
			$this->collDeudorDomicilios = array();
		}
	}

	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Deudor has previously
	 * been saved, it will retrieve related DeudorDomicilios from storage.
	 * If this Deudor is new, it will return
	 * an empty collection or the current collection, the criteria
	 * is ignored on a new object.
	 *
	 * @param      Connection $con
	 * @param      Criteria $criteria
	 * @throws     PropelException
	 */
	public function getDeudorDomicilios($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collDeudorDomicilios === null) {
			if ($this->isNew()) {
			   $this->collDeudorDomicilios = array();
			} else {

				$criteria->add(DeudorDomicilioPeer::DEUDOR_ID, $this->getId());

				DeudorDomicilioPeer::addSelectColumns($criteria);
				$this->collDeudorDomicilios = DeudorDomicilioPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(DeudorDomicilioPeer::DEUDOR_ID, $this->getId());

				DeudorDomicilioPeer::addSelectColumns($criteria);
				if (!isset($this->lastDeudorDomicilioCriteria) || !$this->lastDeudorDomicilioCriteria->equals($criteria)) {
					$this->collDeudorDomicilios = DeudorDomicilioPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastDeudorDomicilioCriteria = $criteria;
		return $this->collDeudorDomicilios;
	}

	/**
	 * Returns the number of related DeudorDomicilios.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      Connection $con
	 * @throws     PropelException
	 */
	public function countDeudorDomicilios($criteria = null, $distinct = false, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(DeudorDomicilioPeer::DEUDOR_ID, $this->getId());

		return DeudorDomicilioPeer::doCount($criteria, $distinct, $con);
	}

	/**
	 * Method called to associate a DeudorDomicilio object to this object
	 * through the DeudorDomicilio foreign key attribute
	 *
	 * @param      DeudorDomicilio $l DeudorDomicilio
	 * @return     void
	 * @throws     PropelException
	 */
	public function addDeudorDomicilio(DeudorDomicilio $l)
	{
		$this->collDeudorDomicilios[] = $l;
		$l->setDeudor($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Deudor is new, it will return
	 * an empty collection; or if this Deudor has previously
	 * been saved, it will retrieve related DeudorDomicilios from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Deudor.
	 */
	public function getDeudorDomiciliosJoinComuna($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collDeudorDomicilios === null) {
			if ($this->isNew()) {
				$this->collDeudorDomicilios = array();
			} else {

				$criteria->add(DeudorDomicilioPeer::DEUDOR_ID, $this->getId());

				$this->collDeudorDomicilios = DeudorDomicilioPeer::doSelectJoinComuna($criteria, $con);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(DeudorDomicilioPeer::DEUDOR_ID, $this->getId());

			if (!isset($this->lastDeudorDomicilioCriteria) || !$this->lastDeudorDomicilioCriteria->equals($criteria)) {
				$this->collDeudorDomicilios = DeudorDomicilioPeer::doSelectJoinComuna($criteria, $con);
			}
		}
		$this->lastDeudorDomicilioCriteria = $criteria;

		return $this->collDeudorDomicilios;
	}

	/**
	 * Temporary storage of collCausas to save a possible db hit in
	 * the event objects are add to the collection, but the
	 * complete collection is never requested.
	 * @return     void
	 */
	public function initCausas()
	{
		if ($this->collCausas === null) {
			$this->collCausas = array();
		}
	}

	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Deudor has previously
	 * been saved, it will retrieve related Causas from storage.
	 * If this Deudor is new, it will return
	 * an empty collection or the current collection, the criteria
	 * is ignored on a new object.
	 *
	 * @param      Connection $con
	 * @param      Criteria $criteria
	 * @throws     PropelException
	 */
	public function getCausas($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collCausas === null) {
			if ($this->isNew()) {
			   $this->collCausas = array();
			} else {

				$criteria->add(CausaPeer::DEUDOR_ID, $this->getId());

				CausaPeer::addSelectColumns($criteria);
				$this->collCausas = CausaPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(CausaPeer::DEUDOR_ID, $this->getId());

				CausaPeer::addSelectColumns($criteria);
				if (!isset($this->lastCausaCriteria) || !$this->lastCausaCriteria->equals($criteria)) {
					$this->collCausas = CausaPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastCausaCriteria = $criteria;
		return $this->collCausas;
	}

	/**
	 * Returns the number of related Causas.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      Connection $con
	 * @throws     PropelException
	 */
	public function countCausas($criteria = null, $distinct = false, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(CausaPeer::DEUDOR_ID, $this->getId());

		return CausaPeer::doCount($criteria, $distinct, $con);
	}

	/**
	 * Method called to associate a Causa object to this object
	 * through the Causa foreign key attribute
	 *
	 * @param      Causa $l Causa
	 * @return     void
	 * @throws     PropelException
	 */
	public function addCausa(Causa $l)
	{
		$this->collCausas[] = $l;
		$l->setDeudor($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Deudor is new, it will return
	 * an empty collection; or if this Deudor has previously
	 * been saved, it will retrieve related Causas from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Deudor.
	 */
	public function getCausasJoinTipoCausa($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collCausas === null) {
			if ($this->isNew()) {
				$this->collCausas = array();
			} else {

				$criteria->add(CausaPeer::DEUDOR_ID, $this->getId());

				$this->collCausas = CausaPeer::doSelectJoinTipoCausa($criteria, $con);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(CausaPeer::DEUDOR_ID, $this->getId());

			if (!isset($this->lastCausaCriteria) || !$this->lastCausaCriteria->equals($criteria)) {
				$this->collCausas = CausaPeer::doSelectJoinTipoCausa($criteria, $con);
			}
		}
		$this->lastCausaCriteria = $criteria;

		return $this->collCausas;
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Deudor is new, it will return
	 * an empty collection; or if this Deudor has previously
	 * been saved, it will retrieve related Causas from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Deudor.
	 */
	public function getCausasJoinMateria($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collCausas === null) {
			if ($this->isNew()) {
				$this->collCausas = array();
			} else {

				$criteria->add(CausaPeer::DEUDOR_ID, $this->getId());

				$this->collCausas = CausaPeer::doSelectJoinMateria($criteria, $con);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(CausaPeer::DEUDOR_ID, $this->getId());

			if (!isset($this->lastCausaCriteria) || !$this->lastCausaCriteria->equals($criteria)) {
				$this->collCausas = CausaPeer::doSelectJoinMateria($criteria, $con);
			}
		}
		$this->lastCausaCriteria = $criteria;

		return $this->collCausas;
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Deudor is new, it will return
	 * an empty collection; or if this Deudor has previously
	 * been saved, it will retrieve related Causas from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Deudor.
	 */
	public function getCausasJoinContrato($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collCausas === null) {
			if ($this->isNew()) {
				$this->collCausas = array();
			} else {

				$criteria->add(CausaPeer::DEUDOR_ID, $this->getId());

				$this->collCausas = CausaPeer::doSelectJoinContrato($criteria, $con);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(CausaPeer::DEUDOR_ID, $this->getId());

			if (!isset($this->lastCausaCriteria) || !$this->lastCausaCriteria->equals($criteria)) {
				$this->collCausas = CausaPeer::doSelectJoinContrato($criteria, $con);
			}
		}
		$this->lastCausaCriteria = $criteria;

		return $this->collCausas;
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Deudor is new, it will return
	 * an empty collection; or if this Deudor has previously
	 * been saved, it will retrieve related Causas from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Deudor.
	 */
	public function getCausasJoinCompetencia($criteria = null, $con = null)
	{
		// include the Peer class
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collCausas === null) {
			if ($this->isNew()) {
				$this->collCausas = array();
			} else {

				$criteria->add(CausaPeer::DEUDOR_ID, $this->getId());

				$this->collCausas = CausaPeer::doSelectJoinCompetencia($criteria, $con);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(CausaPeer::DEUDOR_ID, $this->getId());

			if (!isset($this->lastCausaCriteria) || !$this->lastCausaCriteria->equals($criteria)) {
				$this->collCausas = CausaPeer::doSelectJoinCompetencia($criteria, $con);
			}
		}
		$this->lastCausaCriteria = $criteria;

		return $this->collCausas;
	}

} // BaseDeudor
