<?php

/**
 * Base class that represents a row from the 'diligencia_formato' table.
 *
 * 
 *
 * @package    lib.model.om
 */
abstract class BaseDiligenciaFormato extends BaseObject  implements Persistent {


	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        DiligenciaFormatoPeer
	 */
	protected static $peer;


	/**
	 * The value for the id field.
	 * @var        int
	 */
	protected $id;


	/**
	 * The value for the diligencia_id field.
	 * @var        int
	 */
	protected $diligencia_id;


	/**
	 * The value for the formato_id field.
	 * @var        int
	 */
	protected $formato_id;

	/**
	 * @var        Diligencia
	 */
	protected $aDiligencia;

	/**
	 * @var        Formato
	 */
	protected $aFormato;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Get the [id] column value.
	 * 
	 * @return     int
	 */
	public function getId()
	{

		return $this->id;
	}

	/**
	 * Get the [diligencia_id] column value.
	 * 
	 * @return     int
	 */
	public function getDiligenciaId()
	{

		return $this->diligencia_id;
	}

	/**
	 * Get the [formato_id] column value.
	 * 
	 * @return     int
	 */
	public function getFormatoId()
	{

		return $this->formato_id;
	}

	/**
	 * Set the value of [id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = DiligenciaFormatoPeer::ID;
		}

	} // setId()

	/**
	 * Set the value of [diligencia_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setDiligenciaId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->diligencia_id !== $v) {
			$this->diligencia_id = $v;
			$this->modifiedColumns[] = DiligenciaFormatoPeer::DILIGENCIA_ID;
		}

		if ($this->aDiligencia !== null && $this->aDiligencia->getId() !== $v) {
			$this->aDiligencia = null;
		}

	} // setDiligenciaId()

	/**
	 * Set the value of [formato_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setFormatoId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->formato_id !== $v) {
			$this->formato_id = $v;
			$this->modifiedColumns[] = DiligenciaFormatoPeer::FORMATO_ID;
		}

		if ($this->aFormato !== null && $this->aFormato->getId() !== $v) {
			$this->aFormato = null;
		}

	} // setFormatoId()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (1-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      ResultSet $rs The ResultSet class with cursor advanced to desired record pos.
	 * @param      int $startcol 1-based offset column which indicates which restultset column to start with.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->diligencia_id = $rs->getInt($startcol + 1);

			$this->formato_id = $rs->getInt($startcol + 2);

			$this->resetModified();

			$this->setNew(false);

			// FIXME - using NUM_COLUMNS may be clearer.
			return $startcol + 3; // 3 = DiligenciaFormatoPeer::NUM_COLUMNS - DiligenciaFormatoPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating DiligenciaFormato object", $e);
		}
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      Connection $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(DiligenciaFormatoPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			DiligenciaFormatoPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.  If the object is new,
	 * it inserts it; otherwise an update is performed.  This method
	 * wraps the doSave() worker method in a transaction.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(DiligenciaFormatoPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave($con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aDiligencia !== null) {
				if ($this->aDiligencia->isModified()) {
					$affectedRows += $this->aDiligencia->save($con);
				}
				$this->setDiligencia($this->aDiligencia);
			}

			if ($this->aFormato !== null) {
				if ($this->aFormato->isModified()) {
					$affectedRows += $this->aFormato->save($con);
				}
				$this->setFormato($this->aFormato);
			}


			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = DiligenciaFormatoPeer::doInsert($this, $con);
					$affectedRows += 1; // we are assuming that there is only 1 row per doInsert() which
										 // should always be true here (even though technically
										 // BasePeer::doInsert() can insert multiple rows).

					$this->setId($pk);  //[IMV] update autoincrement primary key

					$this->setNew(false);
				} else {
					$affectedRows += DiligenciaFormatoPeer::doUpdate($this, $con);
				}
				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aDiligencia !== null) {
				if (!$this->aDiligencia->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aDiligencia->getValidationFailures());
				}
			}

			if ($this->aFormato !== null) {
				if (!$this->aFormato->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aFormato->getValidationFailures());
				}
			}


			if (($retval = DiligenciaFormatoPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = DiligenciaFormatoPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getDiligenciaId();
				break;
			case 2:
				return $this->getFormatoId();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param      string $keyType One of the class type constants TYPE_PHPNAME,
	 *                        TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = DiligenciaFormatoPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getDiligenciaId(),
			$keys[2] => $this->getFormatoId(),
		);
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = DiligenciaFormatoPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setDiligenciaId($value);
				break;
			case 2:
				$this->setFormatoId($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME,
	 * TYPE_NUM. The default key type is the column's phpname (e.g. 'authorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = DiligenciaFormatoPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setDiligenciaId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setFormatoId($arr[$keys[2]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(DiligenciaFormatoPeer::DATABASE_NAME);

		if ($this->isColumnModified(DiligenciaFormatoPeer::ID)) $criteria->add(DiligenciaFormatoPeer::ID, $this->id);
		if ($this->isColumnModified(DiligenciaFormatoPeer::DILIGENCIA_ID)) $criteria->add(DiligenciaFormatoPeer::DILIGENCIA_ID, $this->diligencia_id);
		if ($this->isColumnModified(DiligenciaFormatoPeer::FORMATO_ID)) $criteria->add(DiligenciaFormatoPeer::FORMATO_ID, $this->formato_id);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(DiligenciaFormatoPeer::DATABASE_NAME);

		$criteria->add(DiligenciaFormatoPeer::ID, $this->id);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	/**
	 * Generic method to set the primary key (id column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of DiligenciaFormato (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setDiligenciaId($this->diligencia_id);

		$copyObj->setFormatoId($this->formato_id);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); // this is a pkey column, so set to default value

	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     DiligenciaFormato Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     DiligenciaFormatoPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new DiligenciaFormatoPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a Diligencia object.
	 *
	 * @param      Diligencia $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setDiligencia($v)
	{


		if ($v === null) {
			$this->setDiligenciaId(NULL);
		} else {
			$this->setDiligenciaId($v->getId());
		}


		$this->aDiligencia = $v;
	}


	/**
	 * Get the associated Diligencia object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     Diligencia The associated Diligencia object.
	 * @throws     PropelException
	 */
	public function getDiligencia($con = null)
	{
		if ($this->aDiligencia === null && ($this->diligencia_id !== null)) {
			// include the related Peer class
			$this->aDiligencia = DiligenciaPeer::retrieveByPK($this->diligencia_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = DiligenciaPeer::retrieveByPK($this->diligencia_id, $con);
			   $obj->addDiligencias($this);
			 */
		}
		return $this->aDiligencia;
	}

	/**
	 * Declares an association between this object and a Formato object.
	 *
	 * @param      Formato $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setFormato($v)
	{


		if ($v === null) {
			$this->setFormatoId(NULL);
		} else {
			$this->setFormatoId($v->getId());
		}


		$this->aFormato = $v;
	}


	/**
	 * Get the associated Formato object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     Formato The associated Formato object.
	 * @throws     PropelException
	 */
	public function getFormato($con = null)
	{
		if ($this->aFormato === null && ($this->formato_id !== null)) {
			// include the related Peer class
			$this->aFormato = FormatoPeer::retrieveByPK($this->formato_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = FormatoPeer::retrieveByPK($this->formato_id, $con);
			   $obj->addFormatos($this);
			 */
		}
		return $this->aFormato;
	}

} // BaseDiligenciaFormato
