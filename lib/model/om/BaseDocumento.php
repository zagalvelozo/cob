<?php

/**
 * Base class that represents a row from the 'documento' table.
 *
 * 
 *
 * @package    lib.model.om
 */
abstract class BaseDocumento extends BaseObject  implements Persistent {


	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        DocumentoPeer
	 */
	protected static $peer;


	/**
	 * The value for the id field.
	 * @var        int
	 */
	protected $id;


	/**
	 * The value for the tipo_documento_id field.
	 * @var        int
	 */
	protected $tipo_documento_id;


	/**
	 * The value for the causa_id field.
	 * @var        int
	 */
	protected $causa_id;


	/**
	 * The value for the fecha_vencimiento field.
	 * @var        int
	 */
	protected $fecha_vencimiento;


	/**
	 * The value for the monto field.
	 * @var        int
	 */
	protected $monto;


	/**
	 * The value for the interes field.
	 * @var        double
	 */
	protected $interes;


	/**
	 * The value for the nro_documento field.
	 * @var        string
	 */
	protected $nro_documento;


	/**
	 * The value for the descripcion field.
	 * @var        string
	 */
	protected $descripcion;


	/**
	 * The value for the fecha_emision field.
	 * @var        int
	 */
	protected $fecha_emision;

	/**
	 * @var        TipoDocumento
	 */
	protected $aTipoDocumento;

	/**
	 * @var        Causa
	 */
	protected $aCausa;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Get the [id] column value.
	 * 
	 * @return     int
	 */
	public function getId()
	{

		return $this->id;
	}

	/**
	 * Get the [tipo_documento_id] column value.
	 * 
	 * @return     int
	 */
	public function getTipoDocumentoId()
	{

		return $this->tipo_documento_id;
	}

	/**
	 * Get the [causa_id] column value.
	 * 
	 * @return     int
	 */
	public function getCausaId()
	{

		return $this->causa_id;
	}

	/**
	 * Get the [optionally formatted] [fecha_vencimiento] column value.
	 * 
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the integer unix timestamp will be returned.
	 * @return     mixed Formatted date/time value as string or integer unix timestamp (if format is NULL).
	 * @throws     PropelException - if unable to convert the date/time to timestamp.
	 */
	public function getFechaVencimiento($format = 'Y-m-d')
	{

		if ($this->fecha_vencimiento === null || $this->fecha_vencimiento === '') {
			return null;
		} elseif (!is_int($this->fecha_vencimiento)) {
			// a non-timestamp value was set externally, so we convert it
			$ts = strtotime($this->fecha_vencimiento);
			if ($ts === -1 || $ts === false) { // in PHP 5.1 return value changes to FALSE
				throw new PropelException("Unable to parse value of [fecha_vencimiento] as date/time value: " . var_export($this->fecha_vencimiento, true));
			}
		} else {
			$ts = $this->fecha_vencimiento;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	/**
	 * Get the [monto] column value.
	 * 
	 * @return     int
	 */
	public function getMonto()
	{

		return $this->monto;
	}

	/**
	 * Get the [interes] column value.
	 * 
	 * @return     double
	 */
	public function getInteres()
	{

		return $this->interes;
	}

	/**
	 * Get the [nro_documento] column value.
	 * 
	 * @return     string
	 */
	public function getNroDocumento()
	{

		return $this->nro_documento;
	}

	/**
	 * Get the [descripcion] column value.
	 * 
	 * @return     string
	 */
	public function getDescripcion()
	{

		return $this->descripcion;
	}

	/**
	 * Get the [optionally formatted] [fecha_emision] column value.
	 * 
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the integer unix timestamp will be returned.
	 * @return     mixed Formatted date/time value as string or integer unix timestamp (if format is NULL).
	 * @throws     PropelException - if unable to convert the date/time to timestamp.
	 */
	public function getFechaEmision($format = 'Y-m-d')
	{

		if ($this->fecha_emision === null || $this->fecha_emision === '') {
			return null;
		} elseif (!is_int($this->fecha_emision)) {
			// a non-timestamp value was set externally, so we convert it
			$ts = strtotime($this->fecha_emision);
			if ($ts === -1 || $ts === false) { // in PHP 5.1 return value changes to FALSE
				throw new PropelException("Unable to parse value of [fecha_emision] as date/time value: " . var_export($this->fecha_emision, true));
			}
		} else {
			$ts = $this->fecha_emision;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	/**
	 * Set the value of [id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = DocumentoPeer::ID;
		}

	} // setId()

	/**
	 * Set the value of [tipo_documento_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setTipoDocumentoId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->tipo_documento_id !== $v) {
			$this->tipo_documento_id = $v;
			$this->modifiedColumns[] = DocumentoPeer::TIPO_DOCUMENTO_ID;
		}

		if ($this->aTipoDocumento !== null && $this->aTipoDocumento->getId() !== $v) {
			$this->aTipoDocumento = null;
		}

	} // setTipoDocumentoId()

	/**
	 * Set the value of [causa_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setCausaId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->causa_id !== $v) {
			$this->causa_id = $v;
			$this->modifiedColumns[] = DocumentoPeer::CAUSA_ID;
		}

		if ($this->aCausa !== null && $this->aCausa->getId() !== $v) {
			$this->aCausa = null;
		}

	} // setCausaId()

	/**
	 * Set the value of [fecha_vencimiento] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setFechaVencimiento($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { // in PHP 5.1 return value changes to FALSE
				throw new PropelException("Unable to parse date/time value for [fecha_vencimiento] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->fecha_vencimiento !== $ts) {
			$this->fecha_vencimiento = $ts;
			$this->modifiedColumns[] = DocumentoPeer::FECHA_VENCIMIENTO;
		}

	} // setFechaVencimiento()

	/**
	 * Set the value of [monto] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setMonto($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->monto !== $v) {
			$this->monto = $v;
			$this->modifiedColumns[] = DocumentoPeer::MONTO;
		}

	} // setMonto()

	/**
	 * Set the value of [interes] column.
	 * 
	 * @param      double $v new value
	 * @return     void
	 */
	public function setInteres($v)
	{

		if ($this->interes !== $v) {
			$this->interes = $v;
			$this->modifiedColumns[] = DocumentoPeer::INTERES;
		}

	} // setInteres()

	/**
	 * Set the value of [nro_documento] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setNroDocumento($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nro_documento !== $v) {
			$this->nro_documento = $v;
			$this->modifiedColumns[] = DocumentoPeer::NRO_DOCUMENTO;
		}

	} // setNroDocumento()

	/**
	 * Set the value of [descripcion] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setDescripcion($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->descripcion !== $v) {
			$this->descripcion = $v;
			$this->modifiedColumns[] = DocumentoPeer::DESCRIPCION;
		}

	} // setDescripcion()

	/**
	 * Set the value of [fecha_emision] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setFechaEmision($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { // in PHP 5.1 return value changes to FALSE
				throw new PropelException("Unable to parse date/time value for [fecha_emision] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->fecha_emision !== $ts) {
			$this->fecha_emision = $ts;
			$this->modifiedColumns[] = DocumentoPeer::FECHA_EMISION;
		}

	} // setFechaEmision()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (1-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      ResultSet $rs The ResultSet class with cursor advanced to desired record pos.
	 * @param      int $startcol 1-based offset column which indicates which restultset column to start with.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->tipo_documento_id = $rs->getInt($startcol + 1);

			$this->causa_id = $rs->getInt($startcol + 2);

			$this->fecha_vencimiento = $rs->getDate($startcol + 3, null);

			$this->monto = $rs->getInt($startcol + 4);

			$this->interes = $rs->getFloat($startcol + 5);

			$this->nro_documento = $rs->getString($startcol + 6);

			$this->descripcion = $rs->getString($startcol + 7);

			$this->fecha_emision = $rs->getDate($startcol + 8, null);

			$this->resetModified();

			$this->setNew(false);

			// FIXME - using NUM_COLUMNS may be clearer.
			return $startcol + 9; // 9 = DocumentoPeer::NUM_COLUMNS - DocumentoPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating Documento object", $e);
		}
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      Connection $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(DocumentoPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			DocumentoPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.  If the object is new,
	 * it inserts it; otherwise an update is performed.  This method
	 * wraps the doSave() worker method in a transaction.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(DocumentoPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave($con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aTipoDocumento !== null) {
				if ($this->aTipoDocumento->isModified()) {
					$affectedRows += $this->aTipoDocumento->save($con);
				}
				$this->setTipoDocumento($this->aTipoDocumento);
			}

			if ($this->aCausa !== null) {
				if ($this->aCausa->isModified()) {
					$affectedRows += $this->aCausa->save($con);
				}
				$this->setCausa($this->aCausa);
			}


			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = DocumentoPeer::doInsert($this, $con);
					$affectedRows += 1; // we are assuming that there is only 1 row per doInsert() which
										 // should always be true here (even though technically
										 // BasePeer::doInsert() can insert multiple rows).

					$this->setId($pk);  //[IMV] update autoincrement primary key

					$this->setNew(false);
				} else {
					$affectedRows += DocumentoPeer::doUpdate($this, $con);
				}
				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aTipoDocumento !== null) {
				if (!$this->aTipoDocumento->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTipoDocumento->getValidationFailures());
				}
			}

			if ($this->aCausa !== null) {
				if (!$this->aCausa->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aCausa->getValidationFailures());
				}
			}


			if (($retval = DocumentoPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = DocumentoPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getTipoDocumentoId();
				break;
			case 2:
				return $this->getCausaId();
				break;
			case 3:
				return $this->getFechaVencimiento();
				break;
			case 4:
				return $this->getMonto();
				break;
			case 5:
				return $this->getInteres();
				break;
			case 6:
				return $this->getNroDocumento();
				break;
			case 7:
				return $this->getDescripcion();
				break;
			case 8:
				return $this->getFechaEmision();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param      string $keyType One of the class type constants TYPE_PHPNAME,
	 *                        TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = DocumentoPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getTipoDocumentoId(),
			$keys[2] => $this->getCausaId(),
			$keys[3] => $this->getFechaVencimiento(),
			$keys[4] => $this->getMonto(),
			$keys[5] => $this->getInteres(),
			$keys[6] => $this->getNroDocumento(),
			$keys[7] => $this->getDescripcion(),
			$keys[8] => $this->getFechaEmision(),
		);
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = DocumentoPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setTipoDocumentoId($value);
				break;
			case 2:
				$this->setCausaId($value);
				break;
			case 3:
				$this->setFechaVencimiento($value);
				break;
			case 4:
				$this->setMonto($value);
				break;
			case 5:
				$this->setInteres($value);
				break;
			case 6:
				$this->setNroDocumento($value);
				break;
			case 7:
				$this->setDescripcion($value);
				break;
			case 8:
				$this->setFechaEmision($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME,
	 * TYPE_NUM. The default key type is the column's phpname (e.g. 'authorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = DocumentoPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTipoDocumentoId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setCausaId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setFechaVencimiento($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setMonto($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setInteres($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setNroDocumento($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setDescripcion($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setFechaEmision($arr[$keys[8]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(DocumentoPeer::DATABASE_NAME);

		if ($this->isColumnModified(DocumentoPeer::ID)) $criteria->add(DocumentoPeer::ID, $this->id);
		if ($this->isColumnModified(DocumentoPeer::TIPO_DOCUMENTO_ID)) $criteria->add(DocumentoPeer::TIPO_DOCUMENTO_ID, $this->tipo_documento_id);
		if ($this->isColumnModified(DocumentoPeer::CAUSA_ID)) $criteria->add(DocumentoPeer::CAUSA_ID, $this->causa_id);
		if ($this->isColumnModified(DocumentoPeer::FECHA_VENCIMIENTO)) $criteria->add(DocumentoPeer::FECHA_VENCIMIENTO, $this->fecha_vencimiento);
		if ($this->isColumnModified(DocumentoPeer::MONTO)) $criteria->add(DocumentoPeer::MONTO, $this->monto);
		if ($this->isColumnModified(DocumentoPeer::INTERES)) $criteria->add(DocumentoPeer::INTERES, $this->interes);
		if ($this->isColumnModified(DocumentoPeer::NRO_DOCUMENTO)) $criteria->add(DocumentoPeer::NRO_DOCUMENTO, $this->nro_documento);
		if ($this->isColumnModified(DocumentoPeer::DESCRIPCION)) $criteria->add(DocumentoPeer::DESCRIPCION, $this->descripcion);
		if ($this->isColumnModified(DocumentoPeer::FECHA_EMISION)) $criteria->add(DocumentoPeer::FECHA_EMISION, $this->fecha_emision);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(DocumentoPeer::DATABASE_NAME);

		$criteria->add(DocumentoPeer::ID, $this->id);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	/**
	 * Generic method to set the primary key (id column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Documento (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTipoDocumentoId($this->tipo_documento_id);

		$copyObj->setCausaId($this->causa_id);

		$copyObj->setFechaVencimiento($this->fecha_vencimiento);

		$copyObj->setMonto($this->monto);

		$copyObj->setInteres($this->interes);

		$copyObj->setNroDocumento($this->nro_documento);

		$copyObj->setDescripcion($this->descripcion);

		$copyObj->setFechaEmision($this->fecha_emision);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); // this is a pkey column, so set to default value

	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Documento Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     DocumentoPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new DocumentoPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a TipoDocumento object.
	 *
	 * @param      TipoDocumento $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setTipoDocumento($v)
	{


		if ($v === null) {
			$this->setTipoDocumentoId(NULL);
		} else {
			$this->setTipoDocumentoId($v->getId());
		}


		$this->aTipoDocumento = $v;
	}


	/**
	 * Get the associated TipoDocumento object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     TipoDocumento The associated TipoDocumento object.
	 * @throws     PropelException
	 */
	public function getTipoDocumento($con = null)
	{
		if ($this->aTipoDocumento === null && ($this->tipo_documento_id !== null)) {
			// include the related Peer class
			$this->aTipoDocumento = TipoDocumentoPeer::retrieveByPK($this->tipo_documento_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = TipoDocumentoPeer::retrieveByPK($this->tipo_documento_id, $con);
			   $obj->addTipoDocumentos($this);
			 */
		}
		return $this->aTipoDocumento;
	}

	/**
	 * Declares an association between this object and a Causa object.
	 *
	 * @param      Causa $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setCausa($v)
	{


		if ($v === null) {
			$this->setCausaId(NULL);
		} else {
			$this->setCausaId($v->getId());
		}


		$this->aCausa = $v;
	}


	/**
	 * Get the associated Causa object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     Causa The associated Causa object.
	 * @throws     PropelException
	 */
	public function getCausa($con = null)
	{
		if ($this->aCausa === null && ($this->causa_id !== null)) {
			// include the related Peer class
			$this->aCausa = CausaPeer::retrieveByPK($this->causa_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = CausaPeer::retrieveByPK($this->causa_id, $con);
			   $obj->addCausas($this);
			 */
		}
		return $this->aCausa;
	}

} // BaseDocumento
