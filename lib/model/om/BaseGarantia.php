<?php

/**
 * Base class that represents a row from the 'garantia' table.
 *
 * 
 *
 * @package    lib.model.om
 */
abstract class BaseGarantia extends BaseObject  implements Persistent {


	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        GarantiaPeer
	 */
	protected static $peer;


	/**
	 * The value for the id field.
	 * @var        int
	 */
	protected $id;


	/**
	 * The value for the causa_id field.
	 * @var        int
	 */
	protected $causa_id;


	/**
	 * The value for the tipo_garantia_id field.
	 * @var        int
	 */
	protected $tipo_garantia_id;


	/**
	 * The value for the valor field.
	 * @var        int
	 */
	protected $valor;

	/**
	 * @var        Causa
	 */
	protected $aCausa;

	/**
	 * @var        TipoGarantia
	 */
	protected $aTipoGarantia;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Get the [id] column value.
	 * 
	 * @return     int
	 */
	public function getId()
	{

		return $this->id;
	}

	/**
	 * Get the [causa_id] column value.
	 * 
	 * @return     int
	 */
	public function getCausaId()
	{

		return $this->causa_id;
	}

	/**
	 * Get the [tipo_garantia_id] column value.
	 * 
	 * @return     int
	 */
	public function getTipoGarantiaId()
	{

		return $this->tipo_garantia_id;
	}

	/**
	 * Get the [valor] column value.
	 * 
	 * @return     int
	 */
	public function getValor()
	{

		return $this->valor;
	}

	/**
	 * Set the value of [id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = GarantiaPeer::ID;
		}

	} // setId()

	/**
	 * Set the value of [causa_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setCausaId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->causa_id !== $v) {
			$this->causa_id = $v;
			$this->modifiedColumns[] = GarantiaPeer::CAUSA_ID;
		}

		if ($this->aCausa !== null && $this->aCausa->getId() !== $v) {
			$this->aCausa = null;
		}

	} // setCausaId()

	/**
	 * Set the value of [tipo_garantia_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setTipoGarantiaId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->tipo_garantia_id !== $v) {
			$this->tipo_garantia_id = $v;
			$this->modifiedColumns[] = GarantiaPeer::TIPO_GARANTIA_ID;
		}

		if ($this->aTipoGarantia !== null && $this->aTipoGarantia->getId() !== $v) {
			$this->aTipoGarantia = null;
		}

	} // setTipoGarantiaId()

	/**
	 * Set the value of [valor] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setValor($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->valor !== $v) {
			$this->valor = $v;
			$this->modifiedColumns[] = GarantiaPeer::VALOR;
		}

	} // setValor()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (1-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      ResultSet $rs The ResultSet class with cursor advanced to desired record pos.
	 * @param      int $startcol 1-based offset column which indicates which restultset column to start with.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->causa_id = $rs->getInt($startcol + 1);

			$this->tipo_garantia_id = $rs->getInt($startcol + 2);

			$this->valor = $rs->getInt($startcol + 3);

			$this->resetModified();

			$this->setNew(false);

			// FIXME - using NUM_COLUMNS may be clearer.
			return $startcol + 4; // 4 = GarantiaPeer::NUM_COLUMNS - GarantiaPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating Garantia object", $e);
		}
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      Connection $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GarantiaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			GarantiaPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.  If the object is new,
	 * it inserts it; otherwise an update is performed.  This method
	 * wraps the doSave() worker method in a transaction.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GarantiaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave($con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aCausa !== null) {
				if ($this->aCausa->isModified()) {
					$affectedRows += $this->aCausa->save($con);
				}
				$this->setCausa($this->aCausa);
			}

			if ($this->aTipoGarantia !== null) {
				if ($this->aTipoGarantia->isModified()) {
					$affectedRows += $this->aTipoGarantia->save($con);
				}
				$this->setTipoGarantia($this->aTipoGarantia);
			}


			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = GarantiaPeer::doInsert($this, $con);
					$affectedRows += 1; // we are assuming that there is only 1 row per doInsert() which
										 // should always be true here (even though technically
										 // BasePeer::doInsert() can insert multiple rows).

					$this->setId($pk);  //[IMV] update autoincrement primary key

					$this->setNew(false);
				} else {
					$affectedRows += GarantiaPeer::doUpdate($this, $con);
				}
				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aCausa !== null) {
				if (!$this->aCausa->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aCausa->getValidationFailures());
				}
			}

			if ($this->aTipoGarantia !== null) {
				if (!$this->aTipoGarantia->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTipoGarantia->getValidationFailures());
				}
			}


			if (($retval = GarantiaPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = GarantiaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getCausaId();
				break;
			case 2:
				return $this->getTipoGarantiaId();
				break;
			case 3:
				return $this->getValor();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param      string $keyType One of the class type constants TYPE_PHPNAME,
	 *                        TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = GarantiaPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getCausaId(),
			$keys[2] => $this->getTipoGarantiaId(),
			$keys[3] => $this->getValor(),
		);
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = GarantiaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setCausaId($value);
				break;
			case 2:
				$this->setTipoGarantiaId($value);
				break;
			case 3:
				$this->setValor($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME,
	 * TYPE_NUM. The default key type is the column's phpname (e.g. 'authorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = GarantiaPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setCausaId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setTipoGarantiaId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setValor($arr[$keys[3]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(GarantiaPeer::DATABASE_NAME);

		if ($this->isColumnModified(GarantiaPeer::ID)) $criteria->add(GarantiaPeer::ID, $this->id);
		if ($this->isColumnModified(GarantiaPeer::CAUSA_ID)) $criteria->add(GarantiaPeer::CAUSA_ID, $this->causa_id);
		if ($this->isColumnModified(GarantiaPeer::TIPO_GARANTIA_ID)) $criteria->add(GarantiaPeer::TIPO_GARANTIA_ID, $this->tipo_garantia_id);
		if ($this->isColumnModified(GarantiaPeer::VALOR)) $criteria->add(GarantiaPeer::VALOR, $this->valor);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(GarantiaPeer::DATABASE_NAME);

		$criteria->add(GarantiaPeer::ID, $this->id);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	/**
	 * Generic method to set the primary key (id column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Garantia (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setCausaId($this->causa_id);

		$copyObj->setTipoGarantiaId($this->tipo_garantia_id);

		$copyObj->setValor($this->valor);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); // this is a pkey column, so set to default value

	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Garantia Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     GarantiaPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new GarantiaPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a Causa object.
	 *
	 * @param      Causa $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setCausa($v)
	{


		if ($v === null) {
			$this->setCausaId(NULL);
		} else {
			$this->setCausaId($v->getId());
		}


		$this->aCausa = $v;
	}


	/**
	 * Get the associated Causa object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     Causa The associated Causa object.
	 * @throws     PropelException
	 */
	public function getCausa($con = null)
	{
		if ($this->aCausa === null && ($this->causa_id !== null)) {
			// include the related Peer class
			$this->aCausa = CausaPeer::retrieveByPK($this->causa_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = CausaPeer::retrieveByPK($this->causa_id, $con);
			   $obj->addCausas($this);
			 */
		}
		return $this->aCausa;
	}

	/**
	 * Declares an association between this object and a TipoGarantia object.
	 *
	 * @param      TipoGarantia $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setTipoGarantia($v)
	{


		if ($v === null) {
			$this->setTipoGarantiaId(NULL);
		} else {
			$this->setTipoGarantiaId($v->getId());
		}


		$this->aTipoGarantia = $v;
	}


	/**
	 * Get the associated TipoGarantia object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     TipoGarantia The associated TipoGarantia object.
	 * @throws     PropelException
	 */
	public function getTipoGarantia($con = null)
	{
		if ($this->aTipoGarantia === null && ($this->tipo_garantia_id !== null)) {
			// include the related Peer class
			$this->aTipoGarantia = TipoGarantiaPeer::retrieveByPK($this->tipo_garantia_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = TipoGarantiaPeer::retrieveByPK($this->tipo_garantia_id, $con);
			   $obj->addTipoGarantias($this);
			 */
		}
		return $this->aTipoGarantia;
	}

} // BaseGarantia
