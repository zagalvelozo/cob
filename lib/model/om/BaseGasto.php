<?php

/**
 * Base class that represents a row from the 'gasto' table.
 *
 * 
 *
 * @package    lib.model.om
 */
abstract class BaseGasto extends BaseObject  implements Persistent {


	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        GastoPeer
	 */
	protected static $peer;


	/**
	 * The value for the id field.
	 * @var        int
	 */
	protected $id;


	/**
	 * The value for the tipo_gasto_id field.
	 * @var        int
	 */
	protected $tipo_gasto_id;


	/**
	 * The value for the diligencia_id field.
	 * @var        int
	 */
	protected $diligencia_id;


	/**
	 * The value for the monto field.
	 * @var        int
	 */
	protected $monto;


	/**
	 * The value for the fecha field.
	 * @var        int
	 */
	protected $fecha;


	/**
	 * The value for the motivo field.
	 * @var        string
	 */
	protected $motivo;

	/**
	 * @var        TipoGasto
	 */
	protected $aTipoGasto;

	/**
	 * @var        Diligencia
	 */
	protected $aDiligencia;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Get the [id] column value.
	 * 
	 * @return     int
	 */
	public function getId()
	{

		return $this->id;
	}

	/**
	 * Get the [tipo_gasto_id] column value.
	 * 
	 * @return     int
	 */
	public function getTipoGastoId()
	{

		return $this->tipo_gasto_id;
	}

	/**
	 * Get the [diligencia_id] column value.
	 * 
	 * @return     int
	 */
	public function getDiligenciaId()
	{

		return $this->diligencia_id;
	}

	/**
	 * Get the [monto] column value.
	 * 
	 * @return     int
	 */
	public function getMonto()
	{

		return $this->monto;
	}

	/**
	 * Get the [optionally formatted] [fecha] column value.
	 * 
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the integer unix timestamp will be returned.
	 * @return     mixed Formatted date/time value as string or integer unix timestamp (if format is NULL).
	 * @throws     PropelException - if unable to convert the date/time to timestamp.
	 */
	public function getFecha($format = 'Y-m-d')
	{

		if ($this->fecha === null || $this->fecha === '') {
			return null;
		} elseif (!is_int($this->fecha)) {
			// a non-timestamp value was set externally, so we convert it
			$ts = strtotime($this->fecha);
			if ($ts === -1 || $ts === false) { // in PHP 5.1 return value changes to FALSE
				throw new PropelException("Unable to parse value of [fecha] as date/time value: " . var_export($this->fecha, true));
			}
		} else {
			$ts = $this->fecha;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	/**
	 * Get the [motivo] column value.
	 * 
	 * @return     string
	 */
	public function getMotivo()
	{

		return $this->motivo;
	}

	/**
	 * Set the value of [id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = GastoPeer::ID;
		}

	} // setId()

	/**
	 * Set the value of [tipo_gasto_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setTipoGastoId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->tipo_gasto_id !== $v) {
			$this->tipo_gasto_id = $v;
			$this->modifiedColumns[] = GastoPeer::TIPO_GASTO_ID;
		}

		if ($this->aTipoGasto !== null && $this->aTipoGasto->getId() !== $v) {
			$this->aTipoGasto = null;
		}

	} // setTipoGastoId()

	/**
	 * Set the value of [diligencia_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setDiligenciaId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->diligencia_id !== $v) {
			$this->diligencia_id = $v;
			$this->modifiedColumns[] = GastoPeer::DILIGENCIA_ID;
		}

		if ($this->aDiligencia !== null && $this->aDiligencia->getId() !== $v) {
			$this->aDiligencia = null;
		}

	} // setDiligenciaId()

	/**
	 * Set the value of [monto] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setMonto($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->monto !== $v) {
			$this->monto = $v;
			$this->modifiedColumns[] = GastoPeer::MONTO;
		}

	} // setMonto()

	/**
	 * Set the value of [fecha] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setFecha($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { // in PHP 5.1 return value changes to FALSE
				throw new PropelException("Unable to parse date/time value for [fecha] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->fecha !== $ts) {
			$this->fecha = $ts;
			$this->modifiedColumns[] = GastoPeer::FECHA;
		}

	} // setFecha()

	/**
	 * Set the value of [motivo] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setMotivo($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->motivo !== $v) {
			$this->motivo = $v;
			$this->modifiedColumns[] = GastoPeer::MOTIVO;
		}

	} // setMotivo()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (1-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      ResultSet $rs The ResultSet class with cursor advanced to desired record pos.
	 * @param      int $startcol 1-based offset column which indicates which restultset column to start with.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->tipo_gasto_id = $rs->getInt($startcol + 1);

			$this->diligencia_id = $rs->getInt($startcol + 2);

			$this->monto = $rs->getInt($startcol + 3);

			$this->fecha = $rs->getDate($startcol + 4, null);

			$this->motivo = $rs->getString($startcol + 5);

			$this->resetModified();

			$this->setNew(false);

			// FIXME - using NUM_COLUMNS may be clearer.
			return $startcol + 6; // 6 = GastoPeer::NUM_COLUMNS - GastoPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating Gasto object", $e);
		}
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      Connection $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GastoPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			GastoPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.  If the object is new,
	 * it inserts it; otherwise an update is performed.  This method
	 * wraps the doSave() worker method in a transaction.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GastoPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave($con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aTipoGasto !== null) {
				if ($this->aTipoGasto->isModified()) {
					$affectedRows += $this->aTipoGasto->save($con);
				}
				$this->setTipoGasto($this->aTipoGasto);
			}

			if ($this->aDiligencia !== null) {
				if ($this->aDiligencia->isModified()) {
					$affectedRows += $this->aDiligencia->save($con);
				}
				$this->setDiligencia($this->aDiligencia);
			}


			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = GastoPeer::doInsert($this, $con);
					$affectedRows += 1; // we are assuming that there is only 1 row per doInsert() which
										 // should always be true here (even though technically
										 // BasePeer::doInsert() can insert multiple rows).

					$this->setId($pk);  //[IMV] update autoincrement primary key

					$this->setNew(false);
				} else {
					$affectedRows += GastoPeer::doUpdate($this, $con);
				}
				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aTipoGasto !== null) {
				if (!$this->aTipoGasto->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTipoGasto->getValidationFailures());
				}
			}

			if ($this->aDiligencia !== null) {
				if (!$this->aDiligencia->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aDiligencia->getValidationFailures());
				}
			}


			if (($retval = GastoPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = GastoPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getTipoGastoId();
				break;
			case 2:
				return $this->getDiligenciaId();
				break;
			case 3:
				return $this->getMonto();
				break;
			case 4:
				return $this->getFecha();
				break;
			case 5:
				return $this->getMotivo();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param      string $keyType One of the class type constants TYPE_PHPNAME,
	 *                        TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = GastoPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getTipoGastoId(),
			$keys[2] => $this->getDiligenciaId(),
			$keys[3] => $this->getMonto(),
			$keys[4] => $this->getFecha(),
			$keys[5] => $this->getMotivo(),
		);
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = GastoPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setTipoGastoId($value);
				break;
			case 2:
				$this->setDiligenciaId($value);
				break;
			case 3:
				$this->setMonto($value);
				break;
			case 4:
				$this->setFecha($value);
				break;
			case 5:
				$this->setMotivo($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME,
	 * TYPE_NUM. The default key type is the column's phpname (e.g. 'authorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = GastoPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTipoGastoId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setDiligenciaId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setMonto($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setFecha($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setMotivo($arr[$keys[5]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(GastoPeer::DATABASE_NAME);

		if ($this->isColumnModified(GastoPeer::ID)) $criteria->add(GastoPeer::ID, $this->id);
		if ($this->isColumnModified(GastoPeer::TIPO_GASTO_ID)) $criteria->add(GastoPeer::TIPO_GASTO_ID, $this->tipo_gasto_id);
		if ($this->isColumnModified(GastoPeer::DILIGENCIA_ID)) $criteria->add(GastoPeer::DILIGENCIA_ID, $this->diligencia_id);
		if ($this->isColumnModified(GastoPeer::MONTO)) $criteria->add(GastoPeer::MONTO, $this->monto);
		if ($this->isColumnModified(GastoPeer::FECHA)) $criteria->add(GastoPeer::FECHA, $this->fecha);
		if ($this->isColumnModified(GastoPeer::MOTIVO)) $criteria->add(GastoPeer::MOTIVO, $this->motivo);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(GastoPeer::DATABASE_NAME);

		$criteria->add(GastoPeer::ID, $this->id);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	/**
	 * Generic method to set the primary key (id column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Gasto (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTipoGastoId($this->tipo_gasto_id);

		$copyObj->setDiligenciaId($this->diligencia_id);

		$copyObj->setMonto($this->monto);

		$copyObj->setFecha($this->fecha);

		$copyObj->setMotivo($this->motivo);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); // this is a pkey column, so set to default value

	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Gasto Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     GastoPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new GastoPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a TipoGasto object.
	 *
	 * @param      TipoGasto $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setTipoGasto($v)
	{


		if ($v === null) {
			$this->setTipoGastoId(NULL);
		} else {
			$this->setTipoGastoId($v->getId());
		}


		$this->aTipoGasto = $v;
	}


	/**
	 * Get the associated TipoGasto object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     TipoGasto The associated TipoGasto object.
	 * @throws     PropelException
	 */
	public function getTipoGasto($con = null)
	{
		if ($this->aTipoGasto === null && ($this->tipo_gasto_id !== null)) {
			// include the related Peer class
			$this->aTipoGasto = TipoGastoPeer::retrieveByPK($this->tipo_gasto_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = TipoGastoPeer::retrieveByPK($this->tipo_gasto_id, $con);
			   $obj->addTipoGastos($this);
			 */
		}
		return $this->aTipoGasto;
	}

	/**
	 * Declares an association between this object and a Diligencia object.
	 *
	 * @param      Diligencia $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setDiligencia($v)
	{


		if ($v === null) {
			$this->setDiligenciaId(NULL);
		} else {
			$this->setDiligenciaId($v->getId());
		}


		$this->aDiligencia = $v;
	}


	/**
	 * Get the associated Diligencia object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     Diligencia The associated Diligencia object.
	 * @throws     PropelException
	 */
	public function getDiligencia($con = null)
	{
		if ($this->aDiligencia === null && ($this->diligencia_id !== null)) {
			// include the related Peer class
			$this->aDiligencia = DiligenciaPeer::retrieveByPK($this->diligencia_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = DiligenciaPeer::retrieveByPK($this->diligencia_id, $con);
			   $obj->addDiligencias($this);
			 */
		}
		return $this->aDiligencia;
	}

} // BaseGasto
