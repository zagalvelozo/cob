<?php

/**
 * Base class that represents a row from the 'perfil' table.
 *
 * 
 *
 * @package    lib.model.om
 */
abstract class BasePerfil extends BaseObject  implements Persistent {


	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        PerfilPeer
	 */
	protected static $peer;


	/**
	 * The value for the id field.
	 * @var        int
	 */
	protected $id;


	/**
	 * The value for the sf_guard_user_id field.
	 * @var        int
	 */
	protected $sf_guard_user_id;


	/**
	 * The value for the password1 field.
	 * @var        string
	 */
	protected $password1;


	/**
	 * The value for the password2 field.
	 * @var        string
	 */
	protected $password2;


	/**
	 * The value for the password3 field.
	 * @var        string
	 */
	protected $password3;


	/**
	 * The value for the pregunta field.
	 * @var        string
	 */
	protected $pregunta;


	/**
	 * The value for the respuesta field.
	 * @var        string
	 */
	protected $respuesta;


	/**
	 * The value for the ultimo_cambio field.
	 * @var        int
	 */
	protected $ultimo_cambio;


	/**
	 * The value for the nombres field.
	 * @var        string
	 */
	protected $nombres;


	/**
	 * The value for the apellido_paterno field.
	 * @var        string
	 */
	protected $apellido_paterno;


	/**
	 * The value for the apellido_materno field.
	 * @var        string
	 */
	protected $apellido_materno;


	/**
	 * The value for the comuna_id field.
	 * @var        int
	 */
	protected $comuna_id;


	/**
	 * The value for the direccion field.
	 * @var        string
	 */
	protected $direccion;


	/**
	 * The value for the telefono field.
	 * @var        string
	 */
	protected $telefono;


	/**
	 * The value for the fax field.
	 * @var        string
	 */
	protected $fax;


	/**
	 * The value for the email field.
	 * @var        string
	 */
	protected $email;


	/**
	 * The value for the envio_mail field.
	 * @var        boolean
	 */
	protected $envio_mail;

	/**
	 * @var        sfGuardUser
	 */
	protected $asfGuardUser;

	/**
	 * @var        Comuna
	 */
	protected $aComuna;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Get the [id] column value.
	 * 
	 * @return     int
	 */
	public function getId()
	{

		return $this->id;
	}

	/**
	 * Get the [sf_guard_user_id] column value.
	 * 
	 * @return     int
	 */
	public function getSfGuardUserId()
	{

		return $this->sf_guard_user_id;
	}

	/**
	 * Get the [password1] column value.
	 * 
	 * @return     string
	 */
	public function getPassword1()
	{

		return $this->password1;
	}

	/**
	 * Get the [password2] column value.
	 * 
	 * @return     string
	 */
	public function getPassword2()
	{

		return $this->password2;
	}

	/**
	 * Get the [password3] column value.
	 * 
	 * @return     string
	 */
	public function getPassword3()
	{

		return $this->password3;
	}

	/**
	 * Get the [pregunta] column value.
	 * 
	 * @return     string
	 */
	public function getPregunta()
	{

		return $this->pregunta;
	}

	/**
	 * Get the [respuesta] column value.
	 * 
	 * @return     string
	 */
	public function getRespuesta()
	{

		return $this->respuesta;
	}

	/**
	 * Get the [optionally formatted] [ultimo_cambio] column value.
	 * 
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the integer unix timestamp will be returned.
	 * @return     mixed Formatted date/time value as string or integer unix timestamp (if format is NULL).
	 * @throws     PropelException - if unable to convert the date/time to timestamp.
	 */
	public function getUltimoCambio($format = 'Y-m-d')
	{

		if ($this->ultimo_cambio === null || $this->ultimo_cambio === '') {
			return null;
		} elseif (!is_int($this->ultimo_cambio)) {
			// a non-timestamp value was set externally, so we convert it
			$ts = strtotime($this->ultimo_cambio);
			if ($ts === -1 || $ts === false) { // in PHP 5.1 return value changes to FALSE
				throw new PropelException("Unable to parse value of [ultimo_cambio] as date/time value: " . var_export($this->ultimo_cambio, true));
			}
		} else {
			$ts = $this->ultimo_cambio;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	/**
	 * Get the [nombres] column value.
	 * 
	 * @return     string
	 */
	public function getNombres()
	{

		return $this->nombres;
	}

	/**
	 * Get the [apellido_paterno] column value.
	 * 
	 * @return     string
	 */
	public function getApellidoPaterno()
	{

		return $this->apellido_paterno;
	}

	/**
	 * Get the [apellido_materno] column value.
	 * 
	 * @return     string
	 */
	public function getApellidoMaterno()
	{

		return $this->apellido_materno;
	}

	/**
	 * Get the [comuna_id] column value.
	 * 
	 * @return     int
	 */
	public function getComunaId()
	{

		return $this->comuna_id;
	}

	/**
	 * Get the [direccion] column value.
	 * 
	 * @return     string
	 */
	public function getDireccion()
	{

		return $this->direccion;
	}

	/**
	 * Get the [telefono] column value.
	 * 
	 * @return     string
	 */
	public function getTelefono()
	{

		return $this->telefono;
	}

	/**
	 * Get the [fax] column value.
	 * 
	 * @return     string
	 */
	public function getFax()
	{

		return $this->fax;
	}

	/**
	 * Get the [email] column value.
	 * 
	 * @return     string
	 */
	public function getEmail()
	{

		return $this->email;
	}

	/**
	 * Get the [envio_mail] column value.
	 * 
	 * @return     boolean
	 */
	public function getEnvioMail()
	{

		return $this->envio_mail;
	}

	/**
	 * Set the value of [id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = PerfilPeer::ID;
		}

	} // setId()

	/**
	 * Set the value of [sf_guard_user_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setSfGuardUserId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->sf_guard_user_id !== $v) {
			$this->sf_guard_user_id = $v;
			$this->modifiedColumns[] = PerfilPeer::SF_GUARD_USER_ID;
		}

		if ($this->asfGuardUser !== null && $this->asfGuardUser->getId() !== $v) {
			$this->asfGuardUser = null;
		}

	} // setSfGuardUserId()

	/**
	 * Set the value of [password1] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setPassword1($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->password1 !== $v) {
			$this->password1 = $v;
			$this->modifiedColumns[] = PerfilPeer::PASSWORD1;
		}

	} // setPassword1()

	/**
	 * Set the value of [password2] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setPassword2($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->password2 !== $v) {
			$this->password2 = $v;
			$this->modifiedColumns[] = PerfilPeer::PASSWORD2;
		}

	} // setPassword2()

	/**
	 * Set the value of [password3] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setPassword3($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->password3 !== $v) {
			$this->password3 = $v;
			$this->modifiedColumns[] = PerfilPeer::PASSWORD3;
		}

	} // setPassword3()

	/**
	 * Set the value of [pregunta] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setPregunta($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->pregunta !== $v) {
			$this->pregunta = $v;
			$this->modifiedColumns[] = PerfilPeer::PREGUNTA;
		}

	} // setPregunta()

	/**
	 * Set the value of [respuesta] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setRespuesta($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->respuesta !== $v) {
			$this->respuesta = $v;
			$this->modifiedColumns[] = PerfilPeer::RESPUESTA;
		}

	} // setRespuesta()

	/**
	 * Set the value of [ultimo_cambio] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setUltimoCambio($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { // in PHP 5.1 return value changes to FALSE
				throw new PropelException("Unable to parse date/time value for [ultimo_cambio] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->ultimo_cambio !== $ts) {
			$this->ultimo_cambio = $ts;
			$this->modifiedColumns[] = PerfilPeer::ULTIMO_CAMBIO;
		}

	} // setUltimoCambio()

	/**
	 * Set the value of [nombres] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setNombres($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nombres !== $v) {
			$this->nombres = $v;
			$this->modifiedColumns[] = PerfilPeer::NOMBRES;
		}

	} // setNombres()

	/**
	 * Set the value of [apellido_paterno] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setApellidoPaterno($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->apellido_paterno !== $v) {
			$this->apellido_paterno = $v;
			$this->modifiedColumns[] = PerfilPeer::APELLIDO_PATERNO;
		}

	} // setApellidoPaterno()

	/**
	 * Set the value of [apellido_materno] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setApellidoMaterno($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->apellido_materno !== $v) {
			$this->apellido_materno = $v;
			$this->modifiedColumns[] = PerfilPeer::APELLIDO_MATERNO;
		}

	} // setApellidoMaterno()

	/**
	 * Set the value of [comuna_id] column.
	 * 
	 * @param      int $v new value
	 * @return     void
	 */
	public function setComunaId($v)
	{

		// Since the native PHP type for this column is integer,
		// we will cast the input value to an int (if it is not).
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->comuna_id !== $v) {
			$this->comuna_id = $v;
			$this->modifiedColumns[] = PerfilPeer::COMUNA_ID;
		}

		if ($this->aComuna !== null && $this->aComuna->getId() !== $v) {
			$this->aComuna = null;
		}

	} // setComunaId()

	/**
	 * Set the value of [direccion] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setDireccion($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->direccion !== $v) {
			$this->direccion = $v;
			$this->modifiedColumns[] = PerfilPeer::DIRECCION;
		}

	} // setDireccion()

	/**
	 * Set the value of [telefono] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setTelefono($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->telefono !== $v) {
			$this->telefono = $v;
			$this->modifiedColumns[] = PerfilPeer::TELEFONO;
		}

	} // setTelefono()

	/**
	 * Set the value of [fax] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setFax($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->fax !== $v) {
			$this->fax = $v;
			$this->modifiedColumns[] = PerfilPeer::FAX;
		}

	} // setFax()

	/**
	 * Set the value of [email] column.
	 * 
	 * @param      string $v new value
	 * @return     void
	 */
	public function setEmail($v)
	{

		// Since the native PHP type for this column is string,
		// we will cast the input to a string (if it is not).
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->email !== $v) {
			$this->email = $v;
			$this->modifiedColumns[] = PerfilPeer::EMAIL;
		}

	} // setEmail()

	/**
	 * Set the value of [envio_mail] column.
	 * 
	 * @param      boolean $v new value
	 * @return     void
	 */
	public function setEnvioMail($v)
	{

		if ($this->envio_mail !== $v) {
			$this->envio_mail = $v;
			$this->modifiedColumns[] = PerfilPeer::ENVIO_MAIL;
		}

	} // setEnvioMail()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (1-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      ResultSet $rs The ResultSet class with cursor advanced to desired record pos.
	 * @param      int $startcol 1-based offset column which indicates which restultset column to start with.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->sf_guard_user_id = $rs->getInt($startcol + 1);

			$this->password1 = $rs->getString($startcol + 2);

			$this->password2 = $rs->getString($startcol + 3);

			$this->password3 = $rs->getString($startcol + 4);

			$this->pregunta = $rs->getString($startcol + 5);

			$this->respuesta = $rs->getString($startcol + 6);

			$this->ultimo_cambio = $rs->getDate($startcol + 7, null);

			$this->nombres = $rs->getString($startcol + 8);

			$this->apellido_paterno = $rs->getString($startcol + 9);

			$this->apellido_materno = $rs->getString($startcol + 10);

			$this->comuna_id = $rs->getInt($startcol + 11);

			$this->direccion = $rs->getString($startcol + 12);

			$this->telefono = $rs->getString($startcol + 13);

			$this->fax = $rs->getString($startcol + 14);

			$this->email = $rs->getString($startcol + 15);

			$this->envio_mail = $rs->getBoolean($startcol + 16);

			$this->resetModified();

			$this->setNew(false);

			// FIXME - using NUM_COLUMNS may be clearer.
			return $startcol + 17; // 17 = PerfilPeer::NUM_COLUMNS - PerfilPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating Perfil object", $e);
		}
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      Connection $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PerfilPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PerfilPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.  If the object is new,
	 * it inserts it; otherwise an update is performed.  This method
	 * wraps the doSave() worker method in a transaction.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PerfilPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	/**
	 * Stores the object in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      Connection $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave($con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->asfGuardUser !== null) {
				if ($this->asfGuardUser->isModified()) {
					$affectedRows += $this->asfGuardUser->save($con);
				}
				$this->setsfGuardUser($this->asfGuardUser);
			}

			if ($this->aComuna !== null) {
				if ($this->aComuna->isModified()) {
					$affectedRows += $this->aComuna->save($con);
				}
				$this->setComuna($this->aComuna);
			}


			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PerfilPeer::doInsert($this, $con);
					$affectedRows += 1; // we are assuming that there is only 1 row per doInsert() which
										 // should always be true here (even though technically
										 // BasePeer::doInsert() can insert multiple rows).

					$this->setId($pk);  //[IMV] update autoincrement primary key

					$this->setNew(false);
				} else {
					$affectedRows += PerfilPeer::doUpdate($this, $con);
				}
				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->asfGuardUser !== null) {
				if (!$this->asfGuardUser->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->asfGuardUser->getValidationFailures());
				}
			}

			if ($this->aComuna !== null) {
				if (!$this->aComuna->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aComuna->getValidationFailures());
				}
			}


			if (($retval = PerfilPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PerfilPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getSfGuardUserId();
				break;
			case 2:
				return $this->getPassword1();
				break;
			case 3:
				return $this->getPassword2();
				break;
			case 4:
				return $this->getPassword3();
				break;
			case 5:
				return $this->getPregunta();
				break;
			case 6:
				return $this->getRespuesta();
				break;
			case 7:
				return $this->getUltimoCambio();
				break;
			case 8:
				return $this->getNombres();
				break;
			case 9:
				return $this->getApellidoPaterno();
				break;
			case 10:
				return $this->getApellidoMaterno();
				break;
			case 11:
				return $this->getComunaId();
				break;
			case 12:
				return $this->getDireccion();
				break;
			case 13:
				return $this->getTelefono();
				break;
			case 14:
				return $this->getFax();
				break;
			case 15:
				return $this->getEmail();
				break;
			case 16:
				return $this->getEnvioMail();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param      string $keyType One of the class type constants TYPE_PHPNAME,
	 *                        TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PerfilPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getSfGuardUserId(),
			$keys[2] => $this->getPassword1(),
			$keys[3] => $this->getPassword2(),
			$keys[4] => $this->getPassword3(),
			$keys[5] => $this->getPregunta(),
			$keys[6] => $this->getRespuesta(),
			$keys[7] => $this->getUltimoCambio(),
			$keys[8] => $this->getNombres(),
			$keys[9] => $this->getApellidoPaterno(),
			$keys[10] => $this->getApellidoMaterno(),
			$keys[11] => $this->getComunaId(),
			$keys[12] => $this->getDireccion(),
			$keys[13] => $this->getTelefono(),
			$keys[14] => $this->getFax(),
			$keys[15] => $this->getEmail(),
			$keys[16] => $this->getEnvioMail(),
		);
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants TYPE_PHPNAME,
	 *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PerfilPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setSfGuardUserId($value);
				break;
			case 2:
				$this->setPassword1($value);
				break;
			case 3:
				$this->setPassword2($value);
				break;
			case 4:
				$this->setPassword3($value);
				break;
			case 5:
				$this->setPregunta($value);
				break;
			case 6:
				$this->setRespuesta($value);
				break;
			case 7:
				$this->setUltimoCambio($value);
				break;
			case 8:
				$this->setNombres($value);
				break;
			case 9:
				$this->setApellidoPaterno($value);
				break;
			case 10:
				$this->setApellidoMaterno($value);
				break;
			case 11:
				$this->setComunaId($value);
				break;
			case 12:
				$this->setDireccion($value);
				break;
			case 13:
				$this->setTelefono($value);
				break;
			case 14:
				$this->setFax($value);
				break;
			case 15:
				$this->setEmail($value);
				break;
			case 16:
				$this->setEnvioMail($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME,
	 * TYPE_NUM. The default key type is the column's phpname (e.g. 'authorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PerfilPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setSfGuardUserId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setPassword1($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setPassword2($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setPassword3($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setPregunta($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setRespuesta($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setUltimoCambio($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setNombres($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setApellidoPaterno($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setApellidoMaterno($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setComunaId($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setDireccion($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setTelefono($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setFax($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setEmail($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setEnvioMail($arr[$keys[16]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(PerfilPeer::DATABASE_NAME);

		if ($this->isColumnModified(PerfilPeer::ID)) $criteria->add(PerfilPeer::ID, $this->id);
		if ($this->isColumnModified(PerfilPeer::SF_GUARD_USER_ID)) $criteria->add(PerfilPeer::SF_GUARD_USER_ID, $this->sf_guard_user_id);
		if ($this->isColumnModified(PerfilPeer::PASSWORD1)) $criteria->add(PerfilPeer::PASSWORD1, $this->password1);
		if ($this->isColumnModified(PerfilPeer::PASSWORD2)) $criteria->add(PerfilPeer::PASSWORD2, $this->password2);
		if ($this->isColumnModified(PerfilPeer::PASSWORD3)) $criteria->add(PerfilPeer::PASSWORD3, $this->password3);
		if ($this->isColumnModified(PerfilPeer::PREGUNTA)) $criteria->add(PerfilPeer::PREGUNTA, $this->pregunta);
		if ($this->isColumnModified(PerfilPeer::RESPUESTA)) $criteria->add(PerfilPeer::RESPUESTA, $this->respuesta);
		if ($this->isColumnModified(PerfilPeer::ULTIMO_CAMBIO)) $criteria->add(PerfilPeer::ULTIMO_CAMBIO, $this->ultimo_cambio);
		if ($this->isColumnModified(PerfilPeer::NOMBRES)) $criteria->add(PerfilPeer::NOMBRES, $this->nombres);
		if ($this->isColumnModified(PerfilPeer::APELLIDO_PATERNO)) $criteria->add(PerfilPeer::APELLIDO_PATERNO, $this->apellido_paterno);
		if ($this->isColumnModified(PerfilPeer::APELLIDO_MATERNO)) $criteria->add(PerfilPeer::APELLIDO_MATERNO, $this->apellido_materno);
		if ($this->isColumnModified(PerfilPeer::COMUNA_ID)) $criteria->add(PerfilPeer::COMUNA_ID, $this->comuna_id);
		if ($this->isColumnModified(PerfilPeer::DIRECCION)) $criteria->add(PerfilPeer::DIRECCION, $this->direccion);
		if ($this->isColumnModified(PerfilPeer::TELEFONO)) $criteria->add(PerfilPeer::TELEFONO, $this->telefono);
		if ($this->isColumnModified(PerfilPeer::FAX)) $criteria->add(PerfilPeer::FAX, $this->fax);
		if ($this->isColumnModified(PerfilPeer::EMAIL)) $criteria->add(PerfilPeer::EMAIL, $this->email);
		if ($this->isColumnModified(PerfilPeer::ENVIO_MAIL)) $criteria->add(PerfilPeer::ENVIO_MAIL, $this->envio_mail);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PerfilPeer::DATABASE_NAME);

		$criteria->add(PerfilPeer::ID, $this->id);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	/**
	 * Generic method to set the primary key (id column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Perfil (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setSfGuardUserId($this->sf_guard_user_id);

		$copyObj->setPassword1($this->password1);

		$copyObj->setPassword2($this->password2);

		$copyObj->setPassword3($this->password3);

		$copyObj->setPregunta($this->pregunta);

		$copyObj->setRespuesta($this->respuesta);

		$copyObj->setUltimoCambio($this->ultimo_cambio);

		$copyObj->setNombres($this->nombres);

		$copyObj->setApellidoPaterno($this->apellido_paterno);

		$copyObj->setApellidoMaterno($this->apellido_materno);

		$copyObj->setComunaId($this->comuna_id);

		$copyObj->setDireccion($this->direccion);

		$copyObj->setTelefono($this->telefono);

		$copyObj->setFax($this->fax);

		$copyObj->setEmail($this->email);

		$copyObj->setEnvioMail($this->envio_mail);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); // this is a pkey column, so set to default value

	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Perfil Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     PerfilPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PerfilPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a sfGuardUser object.
	 *
	 * @param      sfGuardUser $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setsfGuardUser($v)
	{


		if ($v === null) {
			$this->setSfGuardUserId(NULL);
		} else {
			$this->setSfGuardUserId($v->getId());
		}


		$this->asfGuardUser = $v;
	}


	/**
	 * Get the associated sfGuardUser object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     sfGuardUser The associated sfGuardUser object.
	 * @throws     PropelException
	 */
	public function getsfGuardUser($con = null)
	{
		if ($this->asfGuardUser === null && ($this->sf_guard_user_id !== null)) {
			// include the related Peer class
			$this->asfGuardUser = sfGuardUserPeer::retrieveByPK($this->sf_guard_user_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = sfGuardUserPeer::retrieveByPK($this->sf_guard_user_id, $con);
			   $obj->addsfGuardUsers($this);
			 */
		}
		return $this->asfGuardUser;
	}

	/**
	 * Declares an association between this object and a Comuna object.
	 *
	 * @param      Comuna $v
	 * @return     void
	 * @throws     PropelException
	 */
	public function setComuna($v)
	{


		if ($v === null) {
			$this->setComunaId(NULL);
		} else {
			$this->setComunaId($v->getId());
		}


		$this->aComuna = $v;
	}


	/**
	 * Get the associated Comuna object
	 *
	 * @param      Connection Optional Connection object.
	 * @return     Comuna The associated Comuna object.
	 * @throws     PropelException
	 */
	public function getComuna($con = null)
	{
		if ($this->aComuna === null && ($this->comuna_id !== null)) {
			// include the related Peer class
			$this->aComuna = ComunaPeer::retrieveByPK($this->comuna_id, $con);

			/* The following can be used instead of the line above to
			   guarantee the related object contains a reference
			   to this object, but this level of coupling
			   may be undesirable in many circumstances.
			   As it can lead to a db query with many results that may
			   never be used.
			   $obj = ComunaPeer::retrieveByPK($this->comuna_id, $con);
			   $obj->addComunas($this);
			 */
		}
		return $this->aComuna;
	}

} // BasePerfil
