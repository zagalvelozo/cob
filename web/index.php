<?php

require_once(__DIR__.'/../config/ProjectConfiguration.class.php');
$configuration = ProjectConfiguration::getApplicationConfiguration('site', 'dev', true);
sfContext::createInstance($configuration)->dispatch();
